/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indotraindisp_cuk;

import java.awt.Button;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author Tech Connexion Indonesia
 */
public class HeadPanel {
    ControlPanelCUK self = null;
    
    public HeadPanel(ControlPanelCUK parent){
        self = parent;
    }
    
    /**
     * Switch head panel on or off
     * @param enabled enable panel | use true or false
     */
    public void toggleEnablePanel(boolean enabled){
        new Thread(() -> {
            try {
                int time_between_switching_panel = 500; //ms
                if (enabled) Thread.sleep(1000);
                if (enabled) {self.audio.masterOn();} else {self.audio.masterOff();}
                Thread.sleep(1000);
                // Switch indicator
                self.audio.panelSwitching();
                self.indicator_arrival_jng.setEnabled(enabled);
                self.indicator_arrival_bks.setEnabled(enabled);
                self.counter_arrival.setEnabled(enabled);
                self.indicator_failure_signal.setEnabled(enabled);
                self.indicator_failure_junction.setEnabled(enabled);
                self.counter_failure.setEnabled(enabled);
                self.btn_reset_indicator.setEnabled(enabled);
                Thread.sleep(time_between_switching_panel);
                // Notification
                self.audio.panelSwitching();
                self.btn_notification_bks.setEnabled(enabled);
                self.btn_notification_jng.setEnabled(enabled);
                self.indicator_notification_bks.setEnabled(enabled);
                self.indicator_notification_jng.setEnabled(enabled);
                self.indicator_notification_arrival_bks.setEnabled(enabled);
                self.indicator_notification_arrival_jng.setEnabled(enabled);
                Thread.sleep(time_between_switching_panel);
                // Junction
                self.audio.panelSwitching();
                self.btn_lock_all_junction.setEnabled(enabled);
                self.btn_unlock_all_junction.setEnabled(enabled);
                self.btn_lock_junction.setEnabled(enabled);
                self.btn_unlock_junction.setEnabled(enabled);
                self.btn_remove_obstacle.setEnabled(enabled);
                self.btn_reposition_junction.setEnabled(enabled);
                self.counter_lock_all_junction.setEnabled(enabled);
                self.counter_unlock_all_junction.setEnabled(enabled);
                self.counter_lock_junction.setEnabled(enabled);
                self.counter_unlock_junction.setEnabled(enabled);
                self.counter_remove_obstacle.setEnabled(enabled);
                self.counter_reposition_junction.setEnabled(enabled);
                self.counter_junction_changed.setEnabled(enabled);
                Thread.sleep(time_between_switching_panel);
                // Route
                self.audio.panelSwitching();
                self.counter_created_route.setEnabled(enabled);
                self.counter_removed_route.setEnabled(enabled);
                String text = enabled ? self.lang.text("panel_unlocked") : self.lang.text("panel_locked");
                self.information.notifyAndLog(text);
            } catch (InterruptedException ex) {
                Logger.getLogger(HeadPanel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }
    
    /**
     * Unlock master panel to make all components are functional
     * @param button 
     */
    public void unlockMaster(Button button){
        if (!self.is_master_on){
            if (button.equals(self.btn_unlock_1)){
                self.is_btn_unlock_1_pressed = self.is_btn_unlock_1_pressed ? false : true;
                String color = self.is_btn_unlock_1_pressed ? "green" : "gray";
                self.switcher.changeBgColor(button, color);
            } else if (button.equals(self.btn_unlock_2)){
                self.is_btn_unlock_2_pressed = self.is_btn_unlock_2_pressed ? false : true;
                String color = self.is_btn_unlock_2_pressed ? "green" : "gray";
                self.switcher.changeBgColor(button, color);
            }
            // Check if both button pressed, so do unlock master
            if (self.is_btn_unlock_1_pressed && self.is_btn_unlock_2_pressed){
                self.switcher.blinkBgColor(self.indicator_master, "red", "green", 3);
                new Thread(()->{
                    try {
                        toggleEnablePanel(true);
                        Thread.sleep(3000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(HeadPanel.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    self.switcher.changeBgColor(self.btn_unlock_1, "gray");
                    self.switcher.changeBgColor(self.btn_unlock_2, "gray");
                    self.is_btn_unlock_1_pressed = false;
                    self.is_btn_unlock_1_pressed = false;
                    self.is_master_on = true;
                }).start();
            }
        }
    }
    
    /**
     * Lock master panel to make all components are non functional
     */
    public void lockMaster(){
        if (self.is_master_on){
            self.switcher.quickBgColor(self.btn_lock, "red");
            self.switcher.changeBgColor(self.indicator_master, "red");
            self.switcher.changeBgColor(self.btn_unlock_1, "gray");
            self.switcher.changeBgColor(self.btn_unlock_2, "gray");
            self.headPanel.toggleEnablePanel(false);
            self.is_master_on = false;
            self.is_btn_unlock_1_pressed = false;
            self.is_btn_unlock_2_pressed = false;
        }
    }
    
    /**
     * Reset all button in Junction section panel
     */
    public void resetJunctionPanel(Button button){
        // Reset lock junction
        if (button != self.btn_lock_junction){
            self.is_btn_lock_junction_pressed = false;
            self.switcher.changeBgColor(self.btn_lock_junction, "gray");
        }
        if (button != self.btn_unlock_junction){
            self.is_btn_unlock_junction_pressed = false;
            self.switcher.changeBgColor(self.btn_unlock_junction, "gray");
        }
        if (button != self.btn_remove_obstacle){
            self.is_btn_remove_obstacle_pressed = false;
            self.switcher.changeBgColor(self.btn_remove_obstacle, "gray");
        }
        if (button != self.btn_reposition_junction){
            self.is_btn_reposition_junction_pressed = false;
            self.switcher.changeBgColor(self.btn_reposition_junction, "gray");
        }
    }
    
    /**
     * Lock single junction toggle button
     */
    public void lockJunction(){
        if (self.is_master_on){
            resetJunctionPanel(self.btn_lock_junction);
            self.is_btn_lock_junction_pressed = self.is_btn_lock_junction_pressed ? false : true;
            String color = self.is_btn_lock_junction_pressed ? "green" : "gray";
            self.switcher.changeBgColor(self.btn_lock_junction, color);
        }
    }
    
    /**
     * Unlock single junction toggle button
     */
    public void unlockJunction(){
        if (self.is_master_on){
            resetJunctionPanel(self.btn_unlock_junction);
            self.is_btn_unlock_junction_pressed = self.is_btn_unlock_junction_pressed ? false : true;
            String color = self.is_btn_unlock_junction_pressed ? "green" : "gray";
            self.switcher.changeBgColor(self.btn_unlock_junction, color);
        }
    }
    
    /**
     * Lock all junction
     */
    public void lockAllJunction(){
        if (self.is_master_on){
            resetJunctionPanel(self.btn_lock_all_junction);
            self.audio.alertGeneral();
            self.is_btn_lock_all_junction_pressed = true;
            self.switcher.quickBgColor(self.btn_lock_all_junction, "green");
            for (Map.Entry<JLabel, String> junction : self.junction_type.entrySet()) {
                self.mainControl.switchJunction(junction.getKey());
            }
            self.is_btn_lock_all_junction_pressed = false;
            increaseCounter(self.counter_lock_all_junction);
            self.information.notifyAndLog(self.lang.text("all_junction_locked"));
        }
    }
    
    /**
     * Lock all junction
     */
    public void unlockAllJunction(){
        if (self.is_master_on){
            resetJunctionPanel(self.btn_unlock_all_junction);
            self.audio.alertGeneral();
            self.is_btn_unlock_all_junction_pressed = true;
            self.switcher.quickBgColor(self.btn_unlock_all_junction, "green");
            for (Map.Entry<JLabel, String> junction : self.junction_type.entrySet()) {
                self.mainControl.switchJunction(junction.getKey());
            }
            self.is_btn_unlock_all_junction_pressed = false;
            increaseCounter(self.counter_unlock_all_junction);
            self.information.log(self.lang.text("all_junction_unlocked"));
            self.information.notification(self.lang.text("all_junction_unlocked"));
        }
    }
    
    /**
     * Remove obstacle of junction
     */
    public void removeObstacle(){
        if (self.is_master_on){
            resetJunctionPanel(self.btn_remove_obstacle);
            self.is_btn_remove_obstacle_pressed = self.is_btn_remove_obstacle_pressed ? false : true;
            String color = self.is_btn_remove_obstacle_pressed ? "green" : "gray";
            self.switcher.changeBgColor(self.btn_remove_obstacle, color);
        }
    }
    
    /**
     * Junction repositioning
     */
    public void repositionJunction(){
        if (self.is_master_on){
            resetJunctionPanel(self.btn_reposition_junction);
            self.is_btn_reposition_junction_pressed = self.is_btn_reposition_junction_pressed ? false : true;
            String color = self.is_btn_reposition_junction_pressed ? "green" : "gray";
            self.switcher.changeBgColor(self.btn_reposition_junction, color);
        }
    }
    
    /**
     * Increase counter on specified panel
     */
    public void increaseCounter(JTextField counter){
        int count = Integer.parseInt(counter.getText());
        count++;
        String count_string = String.valueOf(count);
        counter.setText(count_string);
        self.switcher.quickBgColor(counter, "green");
    }
    
    public void alertFailureJunction(){
        self.switcher.blinkBgColor(self.indicator_failure_junction, "red", "gray");
        increaseCounter(self.counter_failure);
    }
    
    public void alertFailureSignal(){
        self.switcher.blinkBgColor(self.indicator_failure_signal, "red", "gray");
        increaseCounter(self.counter_failure);
    }
    
    public void alertTrainEnter(String from_station){
        JTextField station_column = from_station.equals("JNG") ? self.indicator_arrival_jng : self.indicator_arrival_bks;
        self.switcher.blinkBgColor(station_column, "green", "gray",5);
        increaseCounter(self.counter_arrival);
    }
    
    public void resetFailure(){
        self.switcher.quickBgColor(self.btn_reset_indicator, "green");
        self.audio.stopFailure();
        self.switcher.stopBlinkBgColor(self.indicator_failure_junction);
        self.switcher.stopBlinkBgColor(self.indicator_failure_signal);
    }
    
    public void notificationArrival(String station){
        if (station.equals("JNG")){
            self.switcher.blinkBgColor(self.indicator_notification_arrival_jng, "green", "gray",5);
            self.audio.alertNotificationJNG();
        } else {
            self.switcher.blinkBgColor(self.indicator_notification_arrival_bks, "green", "gray",5);
            self.audio.alertNotificationBKS();
        }
    }
    
    public void makeNotifyAvailable(String station){
        if (station.equals("JNG")){
            self.switcher.changeBgColor(self.indicator_notification_jng, "green");
        } else {
            self.switcher.changeBgColor(self.indicator_notification_bks, "green");
        }
    }
    
    public void makeNotifyUnavailable(String station){
        if (station.equals("JNG")){
            self.switcher.changeBgColor(self.indicator_notification_jng, "red");
        } else {
            self.switcher.changeBgColor(self.indicator_notification_bks, "red");
        }
    }
    
    public void notifyAdjacentStation(String station){
        new Thread(()->{
            if (station.equals("JNG")){
                self.switcher.quickBgColor(self.btn_notification_jng, "green");
                self.switcher.blinkBgColor(self.indicator_notification_jng, "green", "gray", 5);
                if (!self.block_202.getText().equals("")){
                    String train_number = self.block_202.getText();
                    self.train_notification_next_station_sent.put(train_number, true);
                }
                if (!self.block_401.getText().equals("")){
                    String train_number = self.block_401.getText();
                    self.train_notification_next_station_sent.put(train_number, true);
                }
                self.audio.notifyStation();
                try {
                    Thread.sleep(5*1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(HeadPanel.class.getName()).log(Level.SEVERE, null, ex);
                }
                self.switcher.changeBgColor(self.indicator_notification_jng, "red");
            } else {
                self.switcher.quickBgColor(self.btn_notification_bks, "green");
                self.switcher.blinkBgColor(self.indicator_notification_bks, "green", "gray", 5);
                if (!self.block_103.getText().equals("")){
                    String train_number = self.block_103.getText();
                    self.train_notification_next_station_sent.put(train_number, true);
                }
                self.audio.notifyStation();
                try {
                    Thread.sleep(5*1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(HeadPanel.class.getName()).log(Level.SEVERE, null, ex);
                }
                self.switcher.changeBgColor(self.indicator_notification_bks, "red");
            }
        }).start();
    }
}
