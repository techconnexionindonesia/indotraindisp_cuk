package indotraindisp_cuk;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author Tech Connexion Indonesia
 */
public class Time {
    ControlPanelCUK self = null;
    
    public Time(ControlPanelCUK parent){
        self = parent;
    }
    
    public void startTime(){
        new Thread(()->{
            while (self.is_simulation_running){
                self.time_play = self.time_play.plusSeconds(1);
                int hour = self.time_play.getHourOfDay();
                int minute = self.time_play.getMinuteOfHour();
                int second = self.time_play.getSecondOfMinute();
                String toPrint = self.utility.cvt2Digit(hour)+":"+self.utility.cvt2Digit(minute)+":"+self.utility.cvt2Digit(second);
                self.label_time.setText(toPrint);
                updateElapsed();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ControlPanelCUK.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }).start();
    }
    
    private void updateElapsed(){
        DateTimeFormatter formatter = DateTimeFormat.forPattern("HH:mm:ss");
        DateTime time = formatter.parseDateTime(self.label_elapsed.getText());
        time = time.plusSeconds(1);
        int hour = time.getHourOfDay();
        int minute = time.getMinuteOfHour();
        int second = time.getSecondOfMinute();
        String toPrint = self.utility.cvt2Digit(hour)+":"+self.utility.cvt2Digit(minute)+":"+self.utility.cvt2Digit(second);
        self.label_elapsed.setText(toPrint);
        if ((minute == 30 || minute == 0 ) && second == 0){
            self.scoring.giveBonusPlayTime();
        }
        checkTargetEnd(time);
    }
    
    private void checkTargetEnd(DateTime time){
        if (self.time_play.isBefore(self.time_end.plusSeconds(+1)) && self.time_play.isAfter(self.time_end.plusSeconds(-1))){
            if (JOptionPane.showConfirmDialog(null, self.lang.text("time_up_text"), self.lang.text("time_up"), 0) == 1){
                self.closeSimulation();
            }
        }
    }
    
    public void addMinutes(){
        new Thread(()->{
            try {
                while(self.is_simulation_running){
                    ResultSet rs = self.utility.dbGet("main");
                    rs.next();
                    String total = rs.getString("total_hours");
                    String add = self.utility.getAddOneMinute(total);
                    Map<String,String> criteria = new HashMap<String,String>();
                    criteria.put("total_hours", add);
                    self.utility.dbUpdate(criteria, "main");
                    Thread.sleep(60000);
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Aplikasi Error , Silahkan reinstall.", "Error", 0);
            } catch (InterruptedException ex) {
                Logger.getLogger(ControlPanelCUK.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }
}
