package indotraindisp_cuk;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Data Parser
 *
 * @author TCIPRO
 */
public class DataParser {
    ControlPanelCUK self = null;
    String base_path = "";
    String unit_path = "";
    String timetable_path = "";
    Map<String,String> file_path = new HashMap<String,String>();
    FileReader file_reader = null;
    BufferedReader buffered_reader = null;
    
    
    public DataParser(ControlPanelCUK parent){
        self = parent;
        base_path = self.utility.getPath() + "/data/";
        unit_path = base_path + "unit/";
        timetable_path = base_path + "timetable/";
    }
    
    public void loadAll(){
        new Thread(()->{
            loadFilePath();
            loadDriver();
            loadStation();
            loadUnit();
            loadTimetable();
            self.afterParseData();
            self.closeSetSimulation();
        }).start();
    }
    
    public void loadFilePath(){
        file_path.put("driver", base_path + "masinis.itd");
        file_path.put("station", base_path + "station.itd");
        file_path.put("unit_kajj", unit_path + "kajj.itd");
        file_path.put("unit_krl", unit_path + "krl.itd");
        file_path.put("unit_barang", unit_path + "barang.itd");
        file_path.put("unit_klb", unit_path + "klb.itd");
        file_path.put("timetable", timetable_path + "CUKtimetable.itd");
    }
    
    public void loadDriver(){
        try {
            System.out.println("Parsing driver data");
            file_reader = new FileReader(file_path.get("driver"));
            buffered_reader = new BufferedReader(file_reader);
            int total_line = Integer.valueOf(String.valueOf(buffered_reader.lines().count()));
            buffered_reader.close();
            file_reader = new FileReader(file_path.get("driver"));
            buffered_reader = new BufferedReader(file_reader);
            int on_line = 0;
            String read_line;
            while (on_line <= total_line){
                read_line = buffered_reader.readLine();
                int reading_line_log = on_line + 1;
                System.out.println("read line : "+(reading_line_log)+"/"+total_line);
                if (read_line != null && !read_line.isEmpty() && !read_line.contains("#")){
                    String selection[] = read_line.split("/");
                    if (selection.length != 2){
                        System.out.println("------------------------");
                        System.out.println("[!] FORMAT INCORRECT [!]");
                        System.out.println("Line : "+read_line);
                        System.out.println("------------------------");
                    } else {
                        if (selection[1].equals("1")){
                            self.driver_count++;
                            self.driver[self.driver_count-1] = selection[0];
                        } else {
                            self.driver_assistant_count++;
                            self.driver_assistant[self.driver_assistant_count-1] = selection[0];
                        }
                        System.out.println(selection[0] + " : " + (selection[1].equals("1") ? "Driver" : "Assistant"));
                    }
                }
                on_line++;
            }
            System.out.println("Finish parsing station data");
            System.out.println();
            buffered_reader.close();
            file_reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DataParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DataParser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void loadStation(){
        try {
            System.out.println("Parsing station data");
            file_reader = new FileReader(file_path.get("station"));
            buffered_reader = new BufferedReader(file_reader);
            int total_line = Integer.valueOf(String.valueOf(buffered_reader.lines().count()));
            buffered_reader.close();
            file_reader = new FileReader(file_path.get("station"));
            buffered_reader = new BufferedReader(file_reader);
            int on_line = 0;
            String read_line;
            while (on_line <= total_line){
                read_line = buffered_reader.readLine();
                int reading_line_log = on_line + 1;
                System.out.println("read line : "+(reading_line_log)+"/"+total_line);
                if (read_line != null && !read_line.isEmpty() && !read_line.contains("#")){
                    String selection[] = read_line.split("/");
                    if (selection.length != 2){
                        System.out.println("------------------------");
                        System.out.println("[!] FORMAT INCORRECT [!]");
                        System.out.println("Line : "+read_line);
                        System.out.println("------------------------");
                    } else {
                        System.out.println(selection[0] + " : " + selection[1]);
                        self.station.put(selection[0], selection[1]);
                    }
                }
                on_line++;
            }
            System.out.println("Finish parsing station data");
            System.out.println();
            buffered_reader.close();
            file_reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DataParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DataParser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void loadUnit(){
        parseUnit("unit_kajj");
        parseUnit("unit_krl");
        parseUnit("unit_barang");
        parseUnit("unit_klb");
    }
    
    public void parseUnit(String unit){
        try {
            System.out.println("Parsing "+unit+" data");
            file_reader = new FileReader(file_path.get(unit));
            buffered_reader = new BufferedReader(file_reader);
            int total_line = Integer.valueOf(String.valueOf(buffered_reader.lines().count()));
            buffered_reader.close();
            file_reader = new FileReader(file_path.get(unit));
            buffered_reader = new BufferedReader(file_reader);
            int on_line = 0;
            String read_line;
            String entering_unit_name = null;
            ArrayList<String> entering_unit_line = new ArrayList<String>();
            int count_sf = 0;
            while (on_line <= total_line){
                read_line = buffered_reader.readLine();
                int reading_line_log = on_line + 1;
                System.out.println("read line : "+(reading_line_log)+"/"+total_line);
                if (read_line != null && !read_line.isEmpty() && !read_line.contains("#")){
                    String selection[] = read_line.split("/");
                    if (selection.length == 3 && selection[0].equals("entry")){
                        entering_unit_name = selection[2];
                        self.unit_type.put(selection[2],selection[1]);
                        System.out.println("Entry unit : "+entering_unit_name+" | Type : "+selection[1]);
                    } else if (selection.length == 2 && entering_unit_name != null){
                        entering_unit_line.add(read_line);
                        count_sf++;
                        System.out.println("Enter unit : "+read_line);
                    } else if (read_line.equals(";")){
                        self.unit.put(entering_unit_name, entering_unit_line);
                        self.unit_sf.put(entering_unit_name, (count_sf+1));
                        System.out.println("Submit unit : "+entering_unit_name+" SF : "+count_sf);
                        System.out.println();
                        entering_unit_name = null;
                        entering_unit_line = new ArrayList<String>();
                        count_sf = 0;
                    } else if (selection.length == 3 && selection[0].equals("unit")){
                        self.unit_count.put(selection[1], Integer.valueOf(selection[2]));
                        System.out.println("Unit count for : "+selection[1]+" is : "+selection[2]);
                    } else {
                        System.out.println("------------------------");
                        System.out.println("[!] FORMAT INCORRECT [!]");
                        System.out.println("Line : "+read_line);
                        System.out.println("------------------------");
                    }
                }
                on_line++;
            }
            System.out.println("Finish parsing "+unit+" data");
            System.out.println();
            file_reader.close();
            buffered_reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DataParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DataParser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void loadTimetable(){
        try {
            System.out.println("Parsing timetable data");
            file_reader = new FileReader(file_path.get("timetable"));
            buffered_reader = new BufferedReader(file_reader);
            int total_line = Integer.valueOf(String.valueOf(buffered_reader.lines().count()));
            buffered_reader.close();
            file_reader = new FileReader(file_path.get("timetable"));
            buffered_reader = new BufferedReader(file_reader);
            int on_line = 0;
            boolean timetable_valid = true;
            String read_line;
            String entering_unit_name = null;
            String[] entering_unit_line = new String[50];
            int count_sf = 0;
            while (on_line <= total_line && timetable_valid){
                System.out.println();
                read_line = buffered_reader.readLine();
                int reading_line_log = on_line + 1;
                System.out.println("read line : "+(reading_line_log)+"/"+total_line);
                if (read_line != null && !read_line.isEmpty() && !read_line.contains("#")){
                    String selection[] = read_line.split("/");
                    System.out.println(read_line);
                    if (validateTimetable(selection)){
                        String type = selection[7];
                        boolean insert_train = true;
                        /* --- Check if train is Facultative --- */
                        if ((type.equals("jjf") || type.equals("krlf") || type.equals("barangf")) && self.activate_facultative == false){
                            insert_train = false;
                            System.out.println("This train will not be inserted");
                        }
                        /* --- Check if train is additional train --- */
                        if (type.equals("klb")){
                            int random_number = self.utility.getRandomNumber(1, 100);
                            if (random_number > self.additional_percentage){
                                insert_train = false;
                                System.out.println("This train will not be inserted");
                            }
                        }
                        if (insert_train){
                            ArrayList<String> train_unit = getUnit(selection[0],selection[1],selection[8]);
                            int train_length = train_unit.size();
                            self.train_name.put(selection[0],selection[1]);
                            self.train_from.put(selection[0],selection[2]);
                            self.train_destination.put(selection[0],selection[3]);
                            self.train_eta.put(selection[0],selection[4]);
                            self.train_etd.put(selection[0],selection[5]);
                            self.train_platform.put(selection[0],selection[6]);
                            self.train_selected_platform.put(selection[0],selection[6]);
                            self.train_type.put(selection[0],selection[7]);
                            self.train_length.put(selection[0],train_length);
                            self.train_unit.put(selection[0],train_unit);
                            self.train_info.put(selection[0],selection[9]);
                            self.train_arrival_block.put(selection[0],Integer.valueOf(selection[10]));
                            self.train_driver.put(selection[0], getDriver("driver"));
                            self.train_driver_assistant.put(selection[0], getDriver("assistant"));
                            self.train_delay.put(selection[0], getDelay());
                            self.is_train_entered_zone.put(selection[0], false);
                            self.train_position.put(selection[0], getArrivalBlockPositionName(selection[0]));
                            self.train_movement.put(selection[0], self.lang.text("running"));
                            self.train_notification_previous_station_sent.put(selection[0], false);
                            self.train_notification_next_station_sent.put(selection[0], false);
                            
                            /* --- Insert train into active train list ---*/
                            insertTrainIntoActiveTrainList(selection[0]);
                        }
                    } else timetable_valid = false;
                }
                on_line++;
            }
            sortActiveTrain();
            self.information.timetableUpdate();
            self.information.trainStatusUpdate();
            self.train.checkEntryTrain();
            System.out.println("Finish parsing timetable data");
            System.out.println();
            file_reader.close();
            buffered_reader.close();
        } catch (IOException ex) {
            Logger.getLogger(DataParser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private boolean validateTimetable(String line[]){
        System.out.println("Validate Timetable");
        boolean valid = true;
        String[] available_type = new String[]{"krl","krlf","jj","jjf","barang","barangf","klb jj","klb krl","klb barang","klb krd","klb lokomotif"};
        if (line.length != 11){
            System.out.println("Format incorrect");
            valid = false;
        }
        if (!self.station.containsKey(line[2])){
            System.out.println("Departure station "+line[2]+" not exist");
            valid = false;
        }
        if (!self.station.containsKey(line[3])){
            System.out.println("Arrival station "+line[3]+" not exist");
            valid = false;
        }
        if (line[4].equals("Ls") == false && line[4].equals("-") == false && (line[4].contains(":") == false || line[4].length() != 5)){
            System.out.println("ETD not valid");
            valid = false;
        }
        if (line[5].equals("Ls") == false && line[5].equals("-") == false && (line[5].contains(":") == false || line[5].length() != 5)){
            System.out.println("ETA not valid");
            valid = false;
        }
        if (Integer.valueOf(line[6]) < 1 || Integer.valueOf(line[6]) > 6){
            System.out.println("Platform not valid");
            valid = false;
        }
        if (!Arrays.asList(available_type).contains(line[7])){
            System.out.println("Train type not available");
            valid = false;
        }
        if (Integer.valueOf(line[8]) < 1){
            System.out.println("Standformation is not number");
            valid = false;
        }
        if (Integer.valueOf(line[10]) < 0 || Integer.valueOf(line[10]) > 3){
            System.out.println("Arrival block not valid");
            valid = false;
        }
        return valid;
    }
    
    private ArrayList<String> getUnit(String number,String name,String sf_string){
        System.out.println("Get unit for : "+name);
        ArrayList<String> unit = new ArrayList<String>();
        int sf = Integer.valueOf(sf_string);
        if (self.unit_count.containsKey(name)){
            System.out.println("Unit count exist");
            int unit_count = self.unit_count.get(name);
            if (checkIfSfAvailable(name,sf)){
                System.out.println("sf available");
                boolean got_unit = false;
                while (got_unit == false){
                    int random_number = self.utility.getRandomNumber(1, unit_count);
                    String get_unit_name = name+" "+String.valueOf(random_number);
                    System.out.println("Get unit name : "+get_unit_name);
                    if (self.unit_sf.get(get_unit_name) == sf){
                        unit = self.unit.get(get_unit_name);
                        got_unit = true;
                        System.out.println("Unit is : "+get_unit_name);
                    }
                }
            } else {
                int random_number = self.utility.getRandomNumber(1, unit_count);
                String get_unit_name = name+" "+String.valueOf(random_number);
                unit = self.unit.get(get_unit_name);
                System.out.println("Unit is : "+get_unit_name);
            }
        } else if (self.unit.containsKey(name)) {
            unit = self.unit.get(name);
            System.out.println("Unit is : "+name);
        } else if (self.unit.containsKey(number)) {
            unit = self.unit.get(number);
            System.out.println("Unit is : "+number);
        }else {
            unit.add(0,self.lang.text("formation_not_available")+"/"+self.lang.text("formation_not_available"));
            System.out.println("Unit not exist for : "+name);
        }
        return unit;
    }
    
    private String getDriver(String type){
        String name = "";
        if (type.equals("driver")){
            int number = self.utility.getRandomNumber(0, (self.driver_count-1));
            name = self.driver[number];
        } else {
            int number = self.utility.getRandomNumber(0, (self.driver_assistant_count-1));
            name = self.driver_assistant[number];
        }
        System.out.println("Get "+type+" : "+name);
        return name;
    }
    
    private int getDelay(){
        int random_number = self.utility.getRandomNumber(1, 100);
        int delay = 0;
        if (random_number >= 1 && random_number <= 50){
            delay = 0;
        }else if (random_number >= 51 && random_number <= 65){
            delay = -1;
        }else if (random_number >= 66 && random_number <= 70){
            delay = -2;
        }else if (random_number >= 71 && random_number <= 85){
            delay = 1;
        }else if (random_number >= 86 && random_number <= 95){
            delay = 2;
        }else if (random_number >= 96 && random_number <= 100){
            delay = 3;
        }
        System.out.println("Get delay : "+String.valueOf(delay));
        return delay;
    }
    
    private boolean checkIfSfAvailable(String name,int sf){
        boolean available = false;
        int unit_count = self.unit_count.get(name);
        int count = 1;
        while (count <= unit_count && !available){
            String unit_name = name+" "+String.valueOf(count);
            if (self.unit_sf.containsKey(unit_name) && self.unit_sf.get(unit_name) == sf) available = true;
            count++;
        }
        return available;
    }
    
    private void insertTrainIntoActiveTrainList(String train_number){
        boolean terminus_departure = false;
        String train_eta_text = self.train_eta.get(train_number);
        if (train_eta_text.equals("-")){
            terminus_departure = true;
            train_eta_text = self.train_etd.get(train_number);
        }
        int delay = self.train_delay.get(train_number);
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy/MM/dd HH:mm:ss");
        DateTime time_eta = formatter.parseDateTime(self.date_play+" "+train_eta_text+":00");
        time_eta = terminus_departure ? time_eta : time_eta.plusMinutes(Integer.valueOf(delay));
        if (time_eta.isAfter(self.time_play.plusSeconds(273))){
            String date_time_string = formatter.print(time_eta);
            self.active_trains.add(date_time_string+";"+train_number);
        } else {
            time_eta = time_eta.plusDays(1);
            String date_time_string = formatter.print(time_eta);
            self.active_trains.add(date_time_string+";"+train_number);
        }
    }
    
    private void sortActiveTrain(){
        Collections.sort(self.active_trains);
        
        ListIterator<String> iterator = self.active_trains.listIterator();
        while (iterator.hasNext()) {
             String value = iterator.next();
             String new_value = value.split(";")[1];
             iterator.set(new_value);
             System.out.println("Enter train : "+new_value+" into active train list");
         }
    }
    
    private String getArrivalBlockPositionName(String train_number){
        int arrival_block = self.train_arrival_block.get(train_number);
        String position = "=>";
        if (arrival_block == 2) position = "<=";
        else if (arrival_block == 0){
            position = "=";
        }
        return position;
    }
}
