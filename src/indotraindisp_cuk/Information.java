/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indotraindisp_cuk;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultCellEditor;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author 62896
 */
public class Information {
    ControlPanelCUK self = null;
    
    public Information(ControlPanelCUK parent){
        self = parent;
    }
    
    public void log(String text){
        DefaultTableModel model = (DefaultTableModel) self.table_log.getModel();
        String hour = self.utility.cvt2Digit(self.time_play.getHourOfDay())+":"+self.utility.cvt2Digit(self.time_play.getMinuteOfHour())+":"+self.utility.cvt2Digit(self.time_play.getSecondOfMinute());
        Object[] row = {hour,text};
        model.insertRow(0, row);
    }
    
    public void notification(String text){
        self.txt_notification.setText(text);
    }
    
    public void notifyAndLog(String text){
        log(text);
        notification(text);
    }
    
    public void notification(String text, int second){
        new Thread(()->{
            self.txt_notification.setText(text);
            try {
                Thread.sleep(second*1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Information.class.getName()).log(Level.SEVERE, null, ex);
            }
            self.txt_notification.setText("");
        });
    }
    
    public void alertUnavailable(String text){
        text = self.lang.text(text);
        self.information.notification(text);
        self.audio.unavailable();
    }
    
    public void alertFailure(String text){
        self.information.notification(text);
        self.information.log(text);
        self.audio.alertFailure();
    }
    
    public void timetableUpdate(){
        new Thread(()->{
            while(self.is_simulation_running){
                try {
                    DefaultTableModel table = (DefaultTableModel) self.table_timetable.getModel();
                    table.setRowCount(0);
                    ListIterator<String> iterator = self.active_trains.listIterator();
                    int row_count = 0;
                    while (iterator.hasNext() && row_count <= 50) {
                        String number = iterator.next();
                        String name = self.train_name.get(number);
                        String from = self.train_from.get(number);
                        String destination = self.train_destination.get(number);
                        String eta = self.train_eta.get(number);
                        String etd = self.train_etd.get(number);
                        String info = self.train_info.get(number);
                        int delay = self.train_delay.get(number);
                        String delay_text = self.lang.text("delay_"+String.valueOf(delay));
                        Object[] row = { number,name,from,destination,eta,etd,info,delay_text };
                        table.addRow(row);
                        row_count++;
                    }
                    Thread.sleep(60000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Information.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }).start();
    }
    
    public void updateTrainSelectedPlatform(){
        DefaultCellEditor cell_editor = (DefaultCellEditor)self.table_train_status.getDefaultEditor(Object.class);
        JTextField textField = (JTextField)cell_editor.getComponent();
        textField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_ENTER){
                    int row = self.table_train_status.getSelectedRow();

                    String platform = textField.getText();
                    String train_number = self.table_train_status.getValueAt(row, 0).toString();

                    self.train_selected_platform.put(train_number, platform);
                    System.out.println("Updated platform : "+platform+" on train : "+train_number);
                }
            }

        });
    }
    
    public void trainStatusUpdate(){
        new Thread(()->{
            updateTrainSelectedPlatform();
            while (self.is_simulation_running){
                try {
                    if (!self.table_train_status.isEditing()){
                        DefaultTableModel table = (DefaultTableModel) self.table_train_status.getModel();
                        table.setRowCount(0);
                        ListIterator<String> iterator = self.running_train.listIterator();
                        int row_count = 0;
                        while (iterator.hasNext() && row_count <= 50) {
                            String number = iterator.next();
                            String name = self.train_name.get(number);
                            String destination = self.train_destination.get(number);
                            String platform = self.train_selected_platform.get(number);
                            String length = String.valueOf(self.train_length.get(number));
                            String position = self.train_position.get(number);
                            String movement = self.train_movement.get(number);
                            Object[] row = { number,name,destination,platform,length,position,movement };
                            table.addRow(row);
                            row_count++;
                        }
                    }
                    Thread.sleep(5000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Information.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }).start();
    }
    
    public void getTrainDetail(){
        new Thread(()->{
            try {
                Thread.sleep(100);
                String train_number = self.info_input_number.getText();
                self.info_number.setText(self.train_name.get(train_number) != null ? train_number : "");
                self.info_name.setText(self.train_name.get(train_number));
                self.info_type.setText(self.lang.text("type_"+self.train_type.get(train_number)));
                self.info_driver.setText(self.train_driver.get(train_number));
                self.info_driver_assistant.setText(self.train_driver_assistant.get(train_number));
                self.info_sf.setText(String.valueOf(self.train_length.get(train_number) != null ? self.train_length.get(train_number) : ""));
                self.info_from.setText(self.station.get(self.train_from.get(train_number)));
                self.info_destination.setText(self.station.get(self.train_destination.get(train_number)));
                self.info_eta.setText(self.train_eta.get(train_number));
                self.info_etd.setText(self.train_etd.get(train_number));
                self.info_platform.setText(self.train_selected_platform.get(train_number));
                self.info_delay.setText(self.lang.text("delay_"+self.train_delay.get(train_number)));
                self.info_info.setText(self.train_info.get(train_number));
                parseUnitToInformation(self.train_unit.get(train_number));
            } catch (InterruptedException ex) {
                Logger.getLogger(Information.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }
    
    private void parseUnitToInformation(ArrayList<String> unit){
        DefaultTableModel table = (DefaultTableModel) self.table_sf.getModel();
        table.setRowCount(0);
        if (unit != null){
            int length = unit.size();
            ListIterator<String> train = unit.listIterator();
            int sf_count = 1;
            while (train.hasNext()) {
                String[] selection = train.next().split("/");
                String train_id = selection[0];
                String train_type = selection[1];
                Object[] row = { sf_count,train_id,train_type };
                table.addRow(row);
                sf_count++;
            }
        }
    }
    
    public void updatePosition(){
        Map<String,String> criteria = new HashMap<String,String>();
        criteria.put("last_play", "Cakung");
        self.utility.dbUpdate(criteria, "main");
    }
    
    public void submitNote(String catatan){
        DefaultTableModel model = (DefaultTableModel) self.table_note.getModel();
        String jam = self.utility.cvt2Digit(self.time_play.getHourOfDay())+":"+self.utility.cvt2Digit(self.time_play.getMinuteOfHour())+":"+self.utility.cvt2Digit(self.time_play.getSecondOfMinute());
        Object[] row = {jam,catatan};
        model.insertRow(0, row);
    }
    public void clearNote(){
        DefaultTableModel model = (DefaultTableModel) self.table_note.getModel();
        model.setRowCount(0);
    }
}
