/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indotraindisp_cuk;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JLabel;

/**
 *
 * @author Tech Connexion Indonesia
 */
public class Initiator {
    ControlPanelCUK self = null;
    
    public Initiator (ControlPanelCUK parent){
        self = parent;
    }
    
    public void initiate_all(){
        initiateJunction();
        initiateTrack();
        initiateSignal();
        initiateBlock();
        initiateTravelTime();
    }
    
    private void initiateJunction(){
        /* --- Segment Type ---*/
        self.segment_type.put(self.junction_11a, "junction");
        self.segment_type.put(self.junction_11b, "junction");
        self.segment_type.put(self.junction_21a, "junction");
        self.segment_type.put(self.junction_21b, "junction");
        self.segment_type.put(self.junction_13a, "junction");
        self.segment_type.put(self.junction_13b, "junction");
        self.segment_type.put(self.junction_23a, "junction");
        self.segment_type.put(self.junction_23b, "junction");
        self.segment_type.put(self.junction_24a1, "junction");
        self.segment_type.put(self.junction_24a2, "junction");
        self.segment_type.put(self.junction_24, "junction");
        self.segment_type.put(self.junction_14, "junction");
        /* --- Is Segment Reserved --- */
        self.is_segment_reserved.put(self.junction_11a, false);
        self.is_segment_reserved.put(self.junction_11b, false);
        self.is_segment_reserved.put(self.junction_21a, false);
        self.is_segment_reserved.put(self.junction_21b, false);
        self.is_segment_reserved.put(self.junction_13a, false);
        self.is_segment_reserved.put(self.junction_13b, false);
        self.is_segment_reserved.put(self.junction_23a, false);
        self.is_segment_reserved.put(self.junction_23b, false);
        self.is_segment_reserved.put(self.junction_24a1, false);
        self.is_segment_reserved.put(self.junction_24a2, false);
        self.is_segment_reserved.put(self.junction_24, false);
        self.is_segment_reserved.put(self.junction_14, false);
        /* --- Is Train on Segment --- */
        self.is_train_on_segment.put(self.junction_11a, false);
        self.is_train_on_segment.put(self.junction_11b, false);
        self.is_train_on_segment.put(self.junction_21a, false);
        self.is_train_on_segment.put(self.junction_21b, false);
        self.is_train_on_segment.put(self.junction_13a, false);
        self.is_train_on_segment.put(self.junction_13b, false);
        self.is_train_on_segment.put(self.junction_23a, false);
        self.is_train_on_segment.put(self.junction_23b, false);
        self.is_train_on_segment.put(self.junction_24a1, false);
        self.is_train_on_segment.put(self.junction_24a2, false);
        self.is_train_on_segment.put(self.junction_24, false);
        self.is_train_on_segment.put(self.junction_14, false);
        /* --- Segment Next Left --- */
        self.segment_next_left.put(self.junction_11a, self.a5);
        self.segment_next_left.put(self.junction_11b, self.junction_11a);
        self.segment_next_left.put(self.junction_21a, self.b4);
        self.segment_next_left.put(self.junction_21b, self.b5);
        self.segment_next_left.put(self.junction_13a, self.a11);
        self.segment_next_left.put(self.junction_13b, self.junction_13a);
        self.segment_next_left.put(self.junction_23a, self.b11);
        self.segment_next_left.put(self.junction_23b, self.b12);
        self.segment_next_left.put(self.junction_24a1, self.b13);
        self.segment_next_left.put(self.junction_24a2, self.b14);
        self.segment_next_left.put(self.junction_24, self.b15);
        self.segment_next_left.put(self.junction_14, self.a15);
        /* --- Segment Next Right --- */
        self.segment_next_right.put(self.junction_11a, self.junction_11b);
        self.segment_next_right.put(self.junction_11b, self.a6);
        self.segment_next_right.put(self.junction_21a, self.b5);
        self.segment_next_right.put(self.junction_21b, self.b6);
        self.segment_next_right.put(self.junction_13a, self.junction_13b);
        self.segment_next_right.put(self.junction_13b, self.a12);
        self.segment_next_right.put(self.junction_23a, self.b12);
        self.segment_next_right.put(self.junction_23b, self.b13);
        self.segment_next_right.put(self.junction_24a1, self.b14);
        self.segment_next_right.put(self.junction_24a2, self.b15);
        self.segment_next_right.put(self.junction_24, self.b16);
        self.segment_next_right.put(self.junction_14, self.a16);
        /* --- Segment Next Turn --- */
        self.segment_next_turn.put(self.junction_11a, self.x_21a_11a);
        self.segment_next_turn.put(self.junction_11b, self.ab1);
        self.segment_next_turn.put(self.junction_21a, self.x_21a_11a);
        self.segment_next_turn.put(self.junction_21b, self.bb1);
        self.segment_next_turn.put(self.junction_13a, self.ab8);
        self.segment_next_turn.put(self.junction_13b, self.x_13b_23b);
        self.segment_next_turn.put(self.junction_23a, self.bb8);
        self.segment_next_turn.put(self.junction_23b, self.x_13b_23b);
        self.segment_next_turn.put(self.junction_24a1, self.c18);
        self.segment_next_turn.put(self.junction_24a2, self.x_24a2_14);
        self.segment_next_turn.put(self.junction_24, self.d22);
        self.segment_next_turn.put(self.junction_14, self.x_24a2_14);
        /* --- Junction Type --- */
        self.junction_type.put(self.junction_11a, "three_point");
        self.junction_type.put(self.junction_11b, "three_point");
        self.junction_type.put(self.junction_21a, "three_point");
        self.junction_type.put(self.junction_21b, "three_point");
        self.junction_type.put(self.junction_13a, "three_point");
        self.junction_type.put(self.junction_13b, "three_point");
        self.junction_type.put(self.junction_23a, "three_point");
        self.junction_type.put(self.junction_23b, "three_point");
        self.junction_type.put(self.junction_24a1, "three_point");
        self.junction_type.put(self.junction_24a2, "three_point");
        self.junction_type.put(self.junction_24, "three_point");
        self.junction_type.put(self.junction_14, "three_point");
        /* --- Junction Vertical Heading --- */
        self.junction_vertical_heading.put(self.junction_11a, "up");
        self.junction_vertical_heading.put(self.junction_11b, "down");
        self.junction_vertical_heading.put(self.junction_21a, "down");
        self.junction_vertical_heading.put(self.junction_21b, "up");
        self.junction_vertical_heading.put(self.junction_13a, "down");
        self.junction_vertical_heading.put(self.junction_13b, "up");
        self.junction_vertical_heading.put(self.junction_23a, "up");
        self.junction_vertical_heading.put(self.junction_23b, "down");
        self.junction_vertical_heading.put(self.junction_24a1, "up");
        self.junction_vertical_heading.put(self.junction_24a2, "down");
        self.junction_vertical_heading.put(self.junction_24, "up");
        self.junction_vertical_heading.put(self.junction_14, "up");
        /* --- Junction Horizontal Heading --- */
        self.junction_horizontal_heading.put(self.junction_11a, "left");
        self.junction_horizontal_heading.put(self.junction_11b, "right");
        self.junction_horizontal_heading.put(self.junction_21a, "right");
        self.junction_horizontal_heading.put(self.junction_21b, "right");
        self.junction_horizontal_heading.put(self.junction_13a, "left");
        self.junction_horizontal_heading.put(self.junction_13b, "right");
        self.junction_horizontal_heading.put(self.junction_23a, "left");
        self.junction_horizontal_heading.put(self.junction_23b, "left");
        self.junction_horizontal_heading.put(self.junction_24a1, "left");
        self.junction_horizontal_heading.put(self.junction_24a2, "right");
        self.junction_horizontal_heading.put(self.junction_24, "left");
        self.junction_horizontal_heading.put(self.junction_14, "left");
        /* --- Segment clickable --- */
        self.is_segment_clickable.put(self.junction_11a, true);
        self.is_segment_clickable.put(self.junction_11b, true);
        self.is_segment_clickable.put(self.junction_21a, true);
        self.is_segment_clickable.put(self.junction_21b, true);
        self.is_segment_clickable.put(self.junction_13a, true);
        self.is_segment_clickable.put(self.junction_13b, true);
        self.is_segment_clickable.put(self.junction_23a, true);
        self.is_segment_clickable.put(self.junction_23b, true);
        self.is_segment_clickable.put(self.junction_24a1, true);
        self.is_segment_clickable.put(self.junction_24a2, true);
        self.is_segment_clickable.put(self.junction_24, true);
        self.is_segment_clickable.put(self.junction_14, true);
        /* --- Is junction turned --- */
        self.is_junction_turned.put(self.junction_11a, false);
        self.is_junction_turned.put(self.junction_11b, false);
        self.is_junction_turned.put(self.junction_21a, false);
        self.is_junction_turned.put(self.junction_21b, false);
        self.is_junction_turned.put(self.junction_13a, false);
        self.is_junction_turned.put(self.junction_13b, false);
        self.is_junction_turned.put(self.junction_23a, false);
        self.is_junction_turned.put(self.junction_23b, false);
        self.is_junction_turned.put(self.junction_24a1, false);
        self.is_junction_turned.put(self.junction_24a2, false);
        self.is_junction_turned.put(self.junction_24, false);
        self.is_junction_turned.put(self.junction_14, false);
        /* --- Is junction locked --- */
        self.is_junction_locked.put(self.junction_11a, false);
        self.is_junction_locked.put(self.junction_11b, false);
        self.is_junction_locked.put(self.junction_21a, false);
        self.is_junction_locked.put(self.junction_21b, false);
        self.is_junction_locked.put(self.junction_13a, false);
        self.is_junction_locked.put(self.junction_13b, false);
        self.is_junction_locked.put(self.junction_23a, false);
        self.is_junction_locked.put(self.junction_23b, false);
        self.is_junction_locked.put(self.junction_24a1, false);
        self.is_junction_locked.put(self.junction_24a2, false);
        self.is_junction_locked.put(self.junction_24, false);
        self.is_junction_locked.put(self.junction_14, false);
        /* --- Is junction has obstacle --- */
        self.is_junction_has_obstacle.put(self.junction_11a, false);
        self.is_junction_has_obstacle.put(self.junction_11b, false);
        self.is_junction_has_obstacle.put(self.junction_21a, false);
        self.is_junction_has_obstacle.put(self.junction_21b, false);
        self.is_junction_has_obstacle.put(self.junction_13a, false);
        self.is_junction_has_obstacle.put(self.junction_13b, false);
        self.is_junction_has_obstacle.put(self.junction_23a, false);
        self.is_junction_has_obstacle.put(self.junction_23b, false);
        self.is_junction_has_obstacle.put(self.junction_24a1, false);
        self.is_junction_has_obstacle.put(self.junction_24a2, false);
        self.is_junction_has_obstacle.put(self.junction_24, false);
        self.is_junction_has_obstacle.put(self.junction_14, false);
        /* --- Is junction need reposition --- */
        self.is_junction_need_reposition.put(self.junction_11a, false);
        self.is_junction_need_reposition.put(self.junction_11b, false);
        self.is_junction_need_reposition.put(self.junction_21a, false);
        self.is_junction_need_reposition.put(self.junction_21b, false);
        self.is_junction_need_reposition.put(self.junction_13a, false);
        self.is_junction_need_reposition.put(self.junction_13b, false);
        self.is_junction_need_reposition.put(self.junction_23a, false);
        self.is_junction_need_reposition.put(self.junction_23b, false);
        self.is_junction_need_reposition.put(self.junction_24a1, false);
        self.is_junction_need_reposition.put(self.junction_24a2, false);
        self.is_junction_need_reposition.put(self.junction_24, false);
        self.is_junction_need_reposition.put(self.junction_14, false);
        /* --- Is junction need fixing --- */
        self.is_junction_need_fixing.put(self.junction_11a, false);
        self.is_junction_need_fixing.put(self.junction_11b, false);
        self.is_junction_need_fixing.put(self.junction_21a, false);
        self.is_junction_need_fixing.put(self.junction_21b, false);
        self.is_junction_need_fixing.put(self.junction_13a, false);
        self.is_junction_need_fixing.put(self.junction_13b, false);
        self.is_junction_need_fixing.put(self.junction_23a, false);
        self.is_junction_need_fixing.put(self.junction_23b, false);
        self.is_junction_need_fixing.put(self.junction_24a1, false);
        self.is_junction_need_fixing.put(self.junction_24a2, false);
        self.is_junction_need_fixing.put(self.junction_24, false);
        self.is_junction_need_fixing.put(self.junction_14, false);
        /* ---- Junction Number --- */
        self.number_of_segment.put(self.junction_11a, "11A");
        self.number_of_segment.put(self.junction_11b, "11B");
        self.number_of_segment.put(self.junction_21a, "21A");
        self.number_of_segment.put(self.junction_21b, "21B");
        self.number_of_segment.put(self.junction_13a, "13A");
        self.number_of_segment.put(self.junction_13b, "13B");
        self.number_of_segment.put(self.junction_23a, "23A");
        self.number_of_segment.put(self.junction_23b, "23B");
        self.number_of_segment.put(self.junction_24a1, "24A1");
        self.number_of_segment.put(self.junction_24a2, "24A2");
        self.number_of_segment.put(self.junction_24, "24");
        self.number_of_segment.put(self.junction_14, "14");
        /* --- Label of Junction --- */
        self.label_of_junction.put(self.junction_11a, self.label_junction_11a);
        self.label_of_junction.put(self.junction_11b, self.label_junction_11b);
        self.label_of_junction.put(self.junction_21a, self.label_junction_21a);
        self.label_of_junction.put(self.junction_21b, self.label_junction_21b);
        self.label_of_junction.put(self.junction_13a, self.label_junction_13a);
        self.label_of_junction.put(self.junction_13b, self.label_junction_13b);
        self.label_of_junction.put(self.junction_23a, self.label_junction_23a);
        self.label_of_junction.put(self.junction_23b, self.label_junction_23b);
        self.label_of_junction.put(self.junction_24a1, self.label_junction_24a1);
        self.label_of_junction.put(self.junction_24a2, self.label_junction_24a2);
        self.label_of_junction.put(self.junction_24, self.label_junction_24);
        self.label_of_junction.put(self.junction_14, self.label_junction_14);
        /* --- Segment on Failure --- */
        self.is_segment_on_failure.put(self.junction_11a, false);
        self.is_segment_on_failure.put(self.junction_11b, false);
        self.is_segment_on_failure.put(self.junction_21a, false);
        self.is_segment_on_failure.put(self.junction_21b, false);
        self.is_segment_on_failure.put(self.junction_13a, false);
        self.is_segment_on_failure.put(self.junction_13b, false);
        self.is_segment_on_failure.put(self.junction_23a, false);
        self.is_segment_on_failure.put(self.junction_23b, false);
        self.is_segment_on_failure.put(self.junction_24a1, false);
        self.is_segment_on_failure.put(self.junction_24a2, false);
        self.is_segment_on_failure.put(self.junction_24, false);
        self.is_segment_on_failure.put(self.junction_14, false);
    }
    
    private void initiateTrack(){
        /* --- Segment Type --- */
        self.segment_type.put(self.a1, "track");
        self.segment_type.put(self.a2, "track");
        self.segment_type.put(self.a3, "track");
        self.segment_type.put(self.a4, "track");
        self.segment_type.put(self.a5, "track");
        self.segment_type.put(self.a6, "track");
        self.segment_type.put(self.a7, "track");
        self.segment_type.put(self.a8, "track");
        self.segment_type.put(self.a9, "track");
        self.segment_type.put(self.a10, "track");
        self.segment_type.put(self.a11, "track");
        self.segment_type.put(self.a12, "track");
        self.segment_type.put(self.a13, "track");
        self.segment_type.put(self.a14, "track");
        self.segment_type.put(self.a15, "track");
        self.segment_type.put(self.a16, "track");
        self.segment_type.put(self.a17, "track");
        self.segment_type.put(self.a18, "track");
        self.segment_type.put(self.a19, "track");
        self.segment_type.put(self.a20, "track");
        
        self.segment_type.put(self.b1, "track");
        self.segment_type.put(self.b2, "track");
        self.segment_type.put(self.b3, "track");
        self.segment_type.put(self.b4, "track");
        self.segment_type.put(self.b5, "track");
        self.segment_type.put(self.b6, "track");
        self.segment_type.put(self.b7, "track");
        self.segment_type.put(self.b8, "track");
        self.segment_type.put(self.b9, "track");
        self.segment_type.put(self.b10, "track");
        self.segment_type.put(self.b11, "track");
        self.segment_type.put(self.b12, "track");
        self.segment_type.put(self.b13, "track");
        self.segment_type.put(self.b14, "track");
        self.segment_type.put(self.b15, "track");
        self.segment_type.put(self.b16, "track");
        self.segment_type.put(self.b17, "track");
        self.segment_type.put(self.b18, "track");
        self.segment_type.put(self.b19, "track");
        self.segment_type.put(self.ab1, "track");
        self.segment_type.put(self.ab2, "track");
        self.segment_type.put(self.ab3, "track");
        self.segment_type.put(self.ab4, "track");
        self.segment_type.put(self.ab5, "track");
        self.segment_type.put(self.ab6, "track");
        self.segment_type.put(self.ab7, "track");
        self.segment_type.put(self.ab8, "track");
        
        self.segment_type.put(self.bb1, "track");
        self.segment_type.put(self.bb2, "track");
        self.segment_type.put(self.bb3, "track");
        self.segment_type.put(self.bb4, "track");
        self.segment_type.put(self.bb5, "track");
        self.segment_type.put(self.bb6, "track");
        self.segment_type.put(self.bb7, "track");
        self.segment_type.put(self.bb8, "track");
        
        self.segment_type.put(self.c1, "track");
        self.segment_type.put(self.c2, "track");
        self.segment_type.put(self.c3, "track");
        self.segment_type.put(self.c4, "track");
        self.segment_type.put(self.c5, "track");
        self.segment_type.put(self.c6, "track");
        self.segment_type.put(self.c7, "track");
        self.segment_type.put(self.c8, "track");
        self.segment_type.put(self.c9, "track");
        self.segment_type.put(self.c10, "track");
        self.segment_type.put(self.c11, "track");
        self.segment_type.put(self.c12, "track");
        self.segment_type.put(self.c13, "track");
        self.segment_type.put(self.c14, "track");
        self.segment_type.put(self.c15, "track");
        self.segment_type.put(self.c16, "track");
        self.segment_type.put(self.c17, "track");
        self.segment_type.put(self.c18, "track");
        
        self.segment_type.put(self.d1, "track");
        self.segment_type.put(self.d2, "track");
        self.segment_type.put(self.d3, "track");
        self.segment_type.put(self.d4, "track");
        self.segment_type.put(self.d5, "track");
        self.segment_type.put(self.d6, "track");
        self.segment_type.put(self.d7, "track");
        self.segment_type.put(self.d8, "track");
        self.segment_type.put(self.d9, "track");
        self.segment_type.put(self.d10, "track");
        self.segment_type.put(self.d11, "track");
        self.segment_type.put(self.d12, "track");
        self.segment_type.put(self.d13, "track");
        self.segment_type.put(self.d14, "track");
        self.segment_type.put(self.d15, "track");
        self.segment_type.put(self.d16, "track");
        self.segment_type.put(self.d17, "track");
        self.segment_type.put(self.d18, "track");
        self.segment_type.put(self.d19, "track");
        self.segment_type.put(self.d20, "track");
        self.segment_type.put(self.d21, "track");
        self.segment_type.put(self.d22, "track");
        
        self.segment_type.put(self.x_21a_11a, "track");
        self.segment_type.put(self.x_13b_23b, "track");
        self.segment_type.put(self.x_24a2_14, "track");
        
        /* --- Is Segment Reserved --- */
        self.is_segment_reserved.put(self.a1, false);
        self.is_segment_reserved.put(self.a2, false);
        self.is_segment_reserved.put(self.a3, false);
        self.is_segment_reserved.put(self.a4, false);
        self.is_segment_reserved.put(self.a5, false);
        self.is_segment_reserved.put(self.a6, false);
        self.is_segment_reserved.put(self.a7, false);
        self.is_segment_reserved.put(self.a8, false);
        self.is_segment_reserved.put(self.a9, false);
        self.is_segment_reserved.put(self.a10, false);
        self.is_segment_reserved.put(self.a11, false);
        self.is_segment_reserved.put(self.a12, false);
        self.is_segment_reserved.put(self.a13, false);
        self.is_segment_reserved.put(self.a14, false);
        self.is_segment_reserved.put(self.a15, false);
        self.is_segment_reserved.put(self.a16, false);
        self.is_segment_reserved.put(self.a17, false);
        self.is_segment_reserved.put(self.a18, false);
        self.is_segment_reserved.put(self.a19, false);
        self.is_segment_reserved.put(self.a20, false);
        
        self.is_segment_reserved.put(self.b1, false);
        self.is_segment_reserved.put(self.b2, false);
        self.is_segment_reserved.put(self.b3, false);
        self.is_segment_reserved.put(self.b4, false);
        self.is_segment_reserved.put(self.b5, false);
        self.is_segment_reserved.put(self.b6, false);
        self.is_segment_reserved.put(self.b7, false);
        self.is_segment_reserved.put(self.b8, false);
        self.is_segment_reserved.put(self.b9, false);
        self.is_segment_reserved.put(self.b10, false);
        self.is_segment_reserved.put(self.b11, false);
        self.is_segment_reserved.put(self.b12, false);
        self.is_segment_reserved.put(self.b13, false);
        self.is_segment_reserved.put(self.b14, false);
        self.is_segment_reserved.put(self.b15, false);
        self.is_segment_reserved.put(self.b16, false);
        self.is_segment_reserved.put(self.b17, false);
        self.is_segment_reserved.put(self.b18, false);
        self.is_segment_reserved.put(self.b19, false);
        self.is_segment_reserved.put(self.ab1, false);
        self.is_segment_reserved.put(self.ab2, false);
        self.is_segment_reserved.put(self.ab3, false);
        self.is_segment_reserved.put(self.ab4, false);
        self.is_segment_reserved.put(self.ab5, false);
        self.is_segment_reserved.put(self.ab6, false);
        self.is_segment_reserved.put(self.ab7, false);
        self.is_segment_reserved.put(self.ab8, false);
        
        self.is_segment_reserved.put(self.bb1, false);
        self.is_segment_reserved.put(self.bb2, false);
        self.is_segment_reserved.put(self.bb3, false);
        self.is_segment_reserved.put(self.bb4, false);
        self.is_segment_reserved.put(self.bb5, false);
        self.is_segment_reserved.put(self.bb6, false);
        self.is_segment_reserved.put(self.bb7, false);
        self.is_segment_reserved.put(self.bb8, false);
        
        self.is_segment_reserved.put(self.c1, false);
        self.is_segment_reserved.put(self.c2, false);
        self.is_segment_reserved.put(self.c3, false);
        self.is_segment_reserved.put(self.c4, false);
        self.is_segment_reserved.put(self.c5, false);
        self.is_segment_reserved.put(self.c6, false);
        self.is_segment_reserved.put(self.c7, false);
        self.is_segment_reserved.put(self.c8, false);
        self.is_segment_reserved.put(self.c9, false);
        self.is_segment_reserved.put(self.c10, false);
        self.is_segment_reserved.put(self.c11, false);
        self.is_segment_reserved.put(self.c12, false);
        self.is_segment_reserved.put(self.c13, false);
        self.is_segment_reserved.put(self.c14, false);
        self.is_segment_reserved.put(self.c15, false);
        self.is_segment_reserved.put(self.c16, false);
        self.is_segment_reserved.put(self.c17, false);
        self.is_segment_reserved.put(self.c18, false);
        
        self.is_segment_reserved.put(self.d1, false);
        self.is_segment_reserved.put(self.d2, false);
        self.is_segment_reserved.put(self.d3, false);
        self.is_segment_reserved.put(self.d4, false);
        self.is_segment_reserved.put(self.d5, false);
        self.is_segment_reserved.put(self.d6, false);
        self.is_segment_reserved.put(self.d7, false);
        self.is_segment_reserved.put(self.d8, false);
        self.is_segment_reserved.put(self.d9, false);
        self.is_segment_reserved.put(self.d10, false);
        self.is_segment_reserved.put(self.d11, false);
        self.is_segment_reserved.put(self.d12, false);
        self.is_segment_reserved.put(self.d13, false);
        self.is_segment_reserved.put(self.d14, false);
        self.is_segment_reserved.put(self.d15, false);
        self.is_segment_reserved.put(self.d16, false);
        self.is_segment_reserved.put(self.d17, false);
        self.is_segment_reserved.put(self.d18, false);
        self.is_segment_reserved.put(self.d19, false);
        self.is_segment_reserved.put(self.d20, false);
        self.is_segment_reserved.put(self.d21, false);
        self.is_segment_reserved.put(self.d22, false);
        
        
        self.is_segment_reserved.put(self.x_21a_11a, false);
        self.is_segment_reserved.put(self.x_13b_23b, false);
        self.is_segment_reserved.put(self.x_24a2_14, false);
        
        /* --- Is Train On Segment --- */
        self.is_train_on_segment.put(self.a1, false);
        self.is_train_on_segment.put(self.a2, false);
        self.is_train_on_segment.put(self.a3, false);
        self.is_train_on_segment.put(self.a4, false);
        self.is_train_on_segment.put(self.a5, false);
        self.is_train_on_segment.put(self.a6, false);
        self.is_train_on_segment.put(self.a7, false);
        self.is_train_on_segment.put(self.a8, false);
        self.is_train_on_segment.put(self.a9, false);
        self.is_train_on_segment.put(self.a10, false);
        self.is_train_on_segment.put(self.a11, false);
        self.is_train_on_segment.put(self.a12, false);
        self.is_train_on_segment.put(self.a13, false);
        self.is_train_on_segment.put(self.a14, false);
        self.is_train_on_segment.put(self.a15, false);
        self.is_train_on_segment.put(self.a16, false);
        self.is_train_on_segment.put(self.a17, false);
        self.is_train_on_segment.put(self.a18, false);
        self.is_train_on_segment.put(self.a19, false);
        self.is_train_on_segment.put(self.a20, false);
        
        self.is_train_on_segment.put(self.b1, false);
        self.is_train_on_segment.put(self.b2, false);
        self.is_train_on_segment.put(self.b3, false);
        self.is_train_on_segment.put(self.b4, false);
        self.is_train_on_segment.put(self.b5, false);
        self.is_train_on_segment.put(self.b6, false);
        self.is_train_on_segment.put(self.b7, false);
        self.is_train_on_segment.put(self.b8, false);
        self.is_train_on_segment.put(self.b9, false);
        self.is_train_on_segment.put(self.b10, false);
        self.is_train_on_segment.put(self.b11, false);
        self.is_train_on_segment.put(self.b12, false);
        self.is_train_on_segment.put(self.b13, false);
        self.is_train_on_segment.put(self.b14, false);
        self.is_train_on_segment.put(self.b15, false);
        self.is_train_on_segment.put(self.b16, false);
        self.is_train_on_segment.put(self.b17, false);
        self.is_train_on_segment.put(self.b18, false);
        self.is_train_on_segment.put(self.b19, false);
        self.is_train_on_segment.put(self.ab1, false);
        self.is_train_on_segment.put(self.ab2, false);
        self.is_train_on_segment.put(self.ab3, false);
        self.is_train_on_segment.put(self.ab4, false);
        self.is_train_on_segment.put(self.ab5, false);
        self.is_train_on_segment.put(self.ab6, false);
        self.is_train_on_segment.put(self.ab7, false);
        self.is_train_on_segment.put(self.ab8, false);
        
        self.is_train_on_segment.put(self.bb1, false);
        self.is_train_on_segment.put(self.bb2, false);
        self.is_train_on_segment.put(self.bb3, false);
        self.is_train_on_segment.put(self.bb4, false);
        self.is_train_on_segment.put(self.bb5, false);
        self.is_train_on_segment.put(self.bb6, false);
        self.is_train_on_segment.put(self.bb7, false);
        self.is_train_on_segment.put(self.bb8, false);
        
        self.is_train_on_segment.put(self.c1, false);
        self.is_train_on_segment.put(self.c2, false);
        self.is_train_on_segment.put(self.c3, false);
        self.is_train_on_segment.put(self.c4, false);
        self.is_train_on_segment.put(self.c5, false);
        self.is_train_on_segment.put(self.c6, false);
        self.is_train_on_segment.put(self.c7, false);
        self.is_train_on_segment.put(self.c8, false);
        self.is_train_on_segment.put(self.c9, false);
        self.is_train_on_segment.put(self.c10, false);
        self.is_train_on_segment.put(self.c11, false);
        self.is_train_on_segment.put(self.c12, false);
        self.is_train_on_segment.put(self.c13, false);
        self.is_train_on_segment.put(self.c14, false);
        self.is_train_on_segment.put(self.c15, false);
        self.is_train_on_segment.put(self.c16, false);
        self.is_train_on_segment.put(self.c17, false);
        self.is_train_on_segment.put(self.c18, false);
        
        self.is_train_on_segment.put(self.d1, false);
        self.is_train_on_segment.put(self.d2, false);
        self.is_train_on_segment.put(self.d3, false);
        self.is_train_on_segment.put(self.d4, false);
        self.is_train_on_segment.put(self.d5, false);
        self.is_train_on_segment.put(self.d6, false);
        self.is_train_on_segment.put(self.d7, false);
        self.is_train_on_segment.put(self.d8, false);
        self.is_train_on_segment.put(self.d9, false);
        self.is_train_on_segment.put(self.d10, false);
        self.is_train_on_segment.put(self.d11, false);
        self.is_train_on_segment.put(self.d12, false);
        self.is_train_on_segment.put(self.d13, false);
        self.is_train_on_segment.put(self.d14, false);
        self.is_train_on_segment.put(self.d15, false);
        self.is_train_on_segment.put(self.d16, false);
        self.is_train_on_segment.put(self.d17, false);
        self.is_train_on_segment.put(self.d18, false);
        self.is_train_on_segment.put(self.d19, false);
        self.is_train_on_segment.put(self.d20, false);
        self.is_train_on_segment.put(self.d21, false);
        self.is_train_on_segment.put(self.d22, false);
        
        
        self.is_train_on_segment.put(self.x_21a_11a, false);
        self.is_train_on_segment.put(self.x_13b_23b, false);
        self.is_train_on_segment.put(self.x_24a2_14, false);
        
        /* --- Segment next left --- */
        self.segment_next_left.put(self.a1,null);
        self.segment_next_left.put(self.a2,self.block_101);
        self.segment_next_left.put(self.a3,self.a2);
        self.segment_next_left.put(self.a4,self.block_102);
        self.segment_next_left.put(self.a5,self.a4);
        self.segment_next_left.put(self.a6,self.junction_11b);
        self.segment_next_left.put(self.a7,self.a6);
        self.segment_next_left.put(self.a8,self.a7);
        self.segment_next_left.put(self.a9,self.block_020);
        self.segment_next_left.put(self.a10,self.a9);
        self.segment_next_left.put(self.a11,self.a10);
        self.segment_next_left.put(self.a12,self.junction_13b);
        self.segment_next_left.put(self.a13,self.a12);
        self.segment_next_left.put(self.a14,self.a13);
        self.segment_next_left.put(self.a15,self.a14);
        self.segment_next_left.put(self.a16,self.junction_14);
        self.segment_next_left.put(self.a17,self.a16);
        self.segment_next_left.put(self.a18,self.a17);
        self.segment_next_left.put(self.a19,self.a18);
        self.segment_next_left.put(self.a20,self.block_103);
        
        self.segment_next_left.put(self.b1,null);
        self.segment_next_left.put(self.b2,self.block_201);
        self.segment_next_left.put(self.b3,self.b2);
        self.segment_next_left.put(self.b4,self.block_202);
        self.segment_next_left.put(self.b5,self.junction_21a);
        self.segment_next_left.put(self.b6,self.junction_21b);
        self.segment_next_left.put(self.b7,self.b6);
        self.segment_next_left.put(self.b8,self.b7);
        self.segment_next_left.put(self.b9,self.block_030);
        self.segment_next_left.put(self.b10,self.b9);
        self.segment_next_left.put(self.b11,self.b10);
        self.segment_next_left.put(self.b12,self.junction_23a);
        self.segment_next_left.put(self.b13,self.junction_23b);
        self.segment_next_left.put(self.b14,self.junction_24a1);
        self.segment_next_left.put(self.b15,self.junction_24a2);
        self.segment_next_left.put(self.b16,self.junction_24);
        self.segment_next_left.put(self.b17,self.block_202b);
        self.segment_next_left.put(self.b18,self.b17);
        self.segment_next_left.put(self.b19,self.block_203);
        
        self.segment_next_left.put(self.c1,null);
        self.segment_next_left.put(self.c2,self.block_301);
        self.segment_next_left.put(self.c3,self.c2);
        self.segment_next_left.put(self.c4,self.block_302);
        self.segment_next_left.put(self.c5,self.c4);
        self.segment_next_left.put(self.c6,self.c5);
        self.segment_next_left.put(self.c7,self.c6);
        self.segment_next_left.put(self.c8,self.c7);
        self.segment_next_left.put(self.c9,self.c8);
        self.segment_next_left.put(self.c10,self.block_050);
        self.segment_next_left.put(self.c11,self.c10);
        self.segment_next_left.put(self.c12,self.c11);
        self.segment_next_left.put(self.c13,self.c12);
        self.segment_next_left.put(self.c14,self.c13);
        self.segment_next_left.put(self.c15,self.c14);
        self.segment_next_left.put(self.c16,self.c15);
        self.segment_next_left.put(self.c17,self.c16);
        self.segment_next_left.put(self.c18,self.c17);
        
        self.segment_next_left.put(self.d1,null);
        self.segment_next_left.put(self.d2,self.block_401);
        self.segment_next_left.put(self.d3,self.d2);
        self.segment_next_left.put(self.d4,self.d3);
        self.segment_next_left.put(self.d5,self.d4);
        self.segment_next_left.put(self.d6,self.d5);
        self.segment_next_left.put(self.d7,self.d6);
        self.segment_next_left.put(self.d8,self.d7);
        self.segment_next_left.put(self.d9,self.d8);
        self.segment_next_left.put(self.d10,self.d9);
        self.segment_next_left.put(self.d11,self.block_060);
        self.segment_next_left.put(self.d12,self.d11);
        self.segment_next_left.put(self.d13,self.d12);
        self.segment_next_left.put(self.d14,self.d13);
        self.segment_next_left.put(self.d15,self.d14);
        self.segment_next_left.put(self.d16,self.d15);
        self.segment_next_left.put(self.d17,self.d16);
        self.segment_next_left.put(self.d18,self.d17);
        self.segment_next_left.put(self.d19,self.d18);
        self.segment_next_left.put(self.d20,self.d19);
        self.segment_next_left.put(self.d21,self.d20);
        self.segment_next_left.put(self.d22,self.d21);
        
        self.segment_next_left.put(self.ab1, self.junction_11b);
        self.segment_next_left.put(self.ab2, self.ab1);
        self.segment_next_left.put(self.ab3, self.ab2);
        self.segment_next_left.put(self.ab4, self.ab3);
        self.segment_next_left.put(self.ab5, self.block_010);
        self.segment_next_left.put(self.ab6, self.ab5);
        self.segment_next_left.put(self.ab7, self.ab6);
        self.segment_next_left.put(self.ab8, self.ab7);
        
        self.segment_next_left.put(self.bb1, self.junction_21b);
        self.segment_next_left.put(self.bb2, self.bb1);
        self.segment_next_left.put(self.bb3, self.bb2);
        self.segment_next_left.put(self.bb4, self.bb3);
        self.segment_next_left.put(self.bb5, self.block_040);
        self.segment_next_left.put(self.bb6, self.bb5);
        self.segment_next_left.put(self.bb7, self.bb6);
        self.segment_next_left.put(self.bb8, self.bb7);
        
        self.segment_next_left.put(self.x_21a_11a, self.junction_21a);
        self.segment_next_left.put(self.x_13b_23b, self.junction_13b);
        self.segment_next_left.put(self.x_24a2_14, self.junction_24a2);
        
        /* --- Segment Next Right --- */
        self.segment_next_right.put(self.a1, self.block_101);
        self.segment_next_right.put(self.a2, self.a3);
        self.segment_next_right.put(self.a3, self.block_102);
        self.segment_next_right.put(self.a4, self.a5);
        self.segment_next_right.put(self.a5, self.junction_11a);
        self.segment_next_right.put(self.a6, self.a7);
        self.segment_next_right.put(self.a7, self.a8);
        self.segment_next_right.put(self.a8, self.block_020);
        self.segment_next_right.put(self.a9, self.a10);
        self.segment_next_right.put(self.a10, self.a11);
        self.segment_next_right.put(self.a11, self.junction_13a);
        self.segment_next_right.put(self.a12, self.a13);
        self.segment_next_right.put(self.a13, self.a14);
        self.segment_next_right.put(self.a14, self.a15);
        self.segment_next_right.put(self.a15, self.junction_14);
        self.segment_next_right.put(self.a16, self.a17);
        self.segment_next_right.put(self.a17, self.a18);
        self.segment_next_right.put(self.a18, self.a19);
        self.segment_next_right.put(self.a19, self.block_103);
        self.segment_next_right.put(self.a20, null);
        
        self.segment_next_right.put(self.b1, self.block_201);
        self.segment_next_right.put(self.b2, self.b3);
        self.segment_next_right.put(self.b3, self.block_202);
        self.segment_next_right.put(self.b4, self.junction_21a);
        self.segment_next_right.put(self.b5, self.junction_21b);
        self.segment_next_right.put(self.b6, self.b7);
        self.segment_next_right.put(self.b7, self.b8);
        self.segment_next_right.put(self.b8, self.block_030);
        self.segment_next_right.put(self.b9, self.b10);
        self.segment_next_right.put(self.b10, self.b11);
        self.segment_next_right.put(self.b11, self.junction_23a);
        self.segment_next_right.put(self.b12, self.junction_23b);
        self.segment_next_right.put(self.b13, self.junction_24a1);
        self.segment_next_right.put(self.b14, self.junction_24a2);
        self.segment_next_right.put(self.b15, self.junction_24);
        self.segment_next_right.put(self.b16, self.block_202b);
        self.segment_next_right.put(self.b17, self.b18);
        self.segment_next_right.put(self.b18, self.block_203);
        self.segment_next_right.put(self.b19, null);
        
        self.segment_next_right.put(self.c1,self.block_301);
        self.segment_next_right.put(self.c2,self.c3);
        self.segment_next_right.put(self.c3,self.block_302);
        self.segment_next_right.put(self.c4,self.c5);
        self.segment_next_right.put(self.c5,self.c6);
        self.segment_next_right.put(self.c6,self.c7);
        self.segment_next_right.put(self.c7,self.c8);
        self.segment_next_right.put(self.c8,self.c9);
        self.segment_next_right.put(self.c9,self.block_050);
        self.segment_next_right.put(self.c10,self.c11);
        self.segment_next_right.put(self.c11,self.c12);
        self.segment_next_right.put(self.c12,self.c13);
        self.segment_next_right.put(self.c13,self.c14);
        self.segment_next_right.put(self.c14,self.c15);
        self.segment_next_right.put(self.c15,self.c16);
        self.segment_next_right.put(self.c16,self.c17);
        self.segment_next_right.put(self.c17,self.c18);
        self.segment_next_right.put(self.c18,self.junction_24a1);
        
        self.segment_next_right.put(self.d1,self.block_401);
        self.segment_next_right.put(self.d2,self.d3);
        self.segment_next_right.put(self.d3,self.d4);
        self.segment_next_right.put(self.d4,self.d5);
        self.segment_next_right.put(self.d5,self.d6);
        self.segment_next_right.put(self.d6,self.d7);
        self.segment_next_right.put(self.d7,self.d8);
        self.segment_next_right.put(self.d8,self.d9);
        self.segment_next_right.put(self.d9,self.d10);
        self.segment_next_right.put(self.d10,self.block_060);
        self.segment_next_right.put(self.d11,self.d12);
        self.segment_next_right.put(self.d12,self.d13);
        self.segment_next_right.put(self.d13,self.d14);
        self.segment_next_right.put(self.d14,self.d15);
        self.segment_next_right.put(self.d15,self.d16);
        self.segment_next_right.put(self.d16,self.d17);
        self.segment_next_right.put(self.d17,self.d18);
        self.segment_next_right.put(self.d18,self.d19);
        self.segment_next_right.put(self.d19,self.d20);
        self.segment_next_right.put(self.d20,self.d21);
        self.segment_next_right.put(self.d21,self.d22);
        self.segment_next_right.put(self.d22,self.junction_24);
        
        self.segment_next_right.put(self.ab1, self.ab2);
        self.segment_next_right.put(self.ab2, self.ab3);
        self.segment_next_right.put(self.ab3, self.ab4);
        self.segment_next_right.put(self.ab4, self.block_010);
        self.segment_next_right.put(self.ab5, self.ab6);
        self.segment_next_right.put(self.ab6, self.ab7);
        self.segment_next_right.put(self.ab7, self.ab8);
        self.segment_next_right.put(self.ab8, self.junction_13a);
        
        self.segment_next_right.put(self.bb1, self.bb2);
        self.segment_next_right.put(self.bb2, self.bb3);
        self.segment_next_right.put(self.bb3, self.bb4);
        self.segment_next_right.put(self.bb4, self.block_040);
        self.segment_next_right.put(self.bb5, self.bb6);
        self.segment_next_right.put(self.bb6, self.bb7);
        self.segment_next_right.put(self.bb7, self.bb8);
        self.segment_next_right.put(self.bb8, self.junction_23a);
        
        self.segment_next_right.put(self.x_21a_11a, self.junction_11a);
        self.segment_next_right.put(self.x_13b_23b, self.junction_23b);
        self.segment_next_right.put(self.x_24a2_14, self.junction_14);
        
        /* --- Track Type --- */
        self.track_type.put(self.a1, "straight");
        self.track_type.put(self.a2, "straight");
        self.track_type.put(self.a3, "straight");
        self.track_type.put(self.a4, "straight");
        self.track_type.put(self.a5, "straight");
        self.track_type.put(self.a6, "straight");
        self.track_type.put(self.a7, "straight");
        self.track_type.put(self.a8, "straight");
        self.track_type.put(self.a9, "straight");
        self.track_type.put(self.a10, "straight");
        self.track_type.put(self.a11, "straight");
        self.track_type.put(self.a12, "straight");
        self.track_type.put(self.a13, "straight");
        self.track_type.put(self.a14, "straight");
        self.track_type.put(self.a15, "straight");
        self.track_type.put(self.a16, "straight");
        self.track_type.put(self.a17, "straight");
        self.track_type.put(self.a18, "straight");
        self.track_type.put(self.a19, "straight");
        self.track_type.put(self.a20, "straight");
        
        self.track_type.put(self.b1, "straight");
        self.track_type.put(self.b2, "straight");
        self.track_type.put(self.b3, "straight");
        self.track_type.put(self.b4, "straight");
        self.track_type.put(self.b5, "straight");
        self.track_type.put(self.b6, "straight");
        self.track_type.put(self.b7, "straight");
        self.track_type.put(self.b8, "straight");
        self.track_type.put(self.b9, "straight");
        self.track_type.put(self.b10, "straight");
        self.track_type.put(self.b11, "straight");
        self.track_type.put(self.b12, "straight");
        self.track_type.put(self.b13, "straight");
        self.track_type.put(self.b14, "straight");
        self.track_type.put(self.b15, "straight");
        self.track_type.put(self.b16, "straight");
        self.track_type.put(self.b17, "straight");
        self.track_type.put(self.b18, "straight");
        self.track_type.put(self.b19, "straight");
        
        self.track_type.put(self.ab1, "diagonal");
        self.track_type.put(self.ab2, "diagonal");
        self.track_type.put(self.ab3, "diagonal");
        self.track_type.put(self.ab4, "straight");
        self.track_type.put(self.ab5, "straight");
        self.track_type.put(self.ab6, "diagonal");
        self.track_type.put(self.ab7, "diagonal");
        self.track_type.put(self.ab8, "diagonal");
        
        self.track_type.put(self.bb1, "diagonal");
        self.track_type.put(self.bb2, "diagonal");
        self.track_type.put(self.bb3, "diagonal");
        self.track_type.put(self.bb4, "straight");
        self.track_type.put(self.bb5, "straight");
        self.track_type.put(self.bb6, "diagonal");
        self.track_type.put(self.bb7, "diagonal");
        self.track_type.put(self.bb8, "diagonal");
        
        self.track_type.put(self.c1, "straight");
        self.track_type.put(self.c2, "straight");
        self.track_type.put(self.c3, "straight");
        self.track_type.put(self.c4, "straight");
        self.track_type.put(self.c5, "straight");
        self.track_type.put(self.c6, "straight");
        self.track_type.put(self.c7, "straight");
        self.track_type.put(self.c8, "straight");
        self.track_type.put(self.c9, "straight");
        self.track_type.put(self.c10, "straight");
        self.track_type.put(self.c11, "diagonal");
        self.track_type.put(self.c12, "diagonal");
        self.track_type.put(self.c13, "diagonal");
        self.track_type.put(self.c14, "diagonal");
        self.track_type.put(self.c15, "diagonal");
        self.track_type.put(self.c16, "diagonal");
        self.track_type.put(self.c17, "diagonal");
        self.track_type.put(self.c18, "diagonal");
        
        self.track_type.put(self.d1, "straight");
        self.track_type.put(self.d2, "straight");
        self.track_type.put(self.d3, "straight");
        self.track_type.put(self.d4, "straight");
        self.track_type.put(self.d5, "straight");
        self.track_type.put(self.d6, "straight");
        self.track_type.put(self.d7, "straight");
        self.track_type.put(self.d8, "straight");
        self.track_type.put(self.d9, "straight");
        self.track_type.put(self.d10, "straight");
        self.track_type.put(self.d11, "straight");
        self.track_type.put(self.d12, "diagonal");
        self.track_type.put(self.d13, "diagonal");
        self.track_type.put(self.d14, "diagonal");
        self.track_type.put(self.d15, "diagonal");
        self.track_type.put(self.d16, "diagonal");
        self.track_type.put(self.d17, "diagonal");
        self.track_type.put(self.d18, "diagonal");
        self.track_type.put(self.d19, "diagonal");
        self.track_type.put(self.d20, "diagonal");
        self.track_type.put(self.d21, "diagonal");
        self.track_type.put(self.d22, "diagonal");
        
        self.track_type.put(self.x_21a_11a, "diagonal");
        self.track_type.put(self.x_13b_23b, "diagonal");
        self.track_type.put(self.x_24a2_14, "diagonal");
        
        /* --- Track Direction --- */
        self.track_direction.put(self.ab1, "left");
        self.track_direction.put(self.ab2, "left");
        self.track_direction.put(self.ab3, "left");
        self.track_direction.put(self.ab6, "right");
        self.track_direction.put(self.ab7, "right");
        self.track_direction.put(self.ab8, "right");
        
        self.track_direction.put(self.bb1, "right");
        self.track_direction.put(self.bb2, "right");
        self.track_direction.put(self.bb3, "right");
        self.track_direction.put(self.bb6, "left");
        self.track_direction.put(self.bb7, "left");
        self.track_direction.put(self.bb8, "left");
        
        self.track_direction.put(self.c11, "left");
        self.track_direction.put(self.c12, "left");
        self.track_direction.put(self.c13, "left");
        self.track_direction.put(self.c14, "left");
        self.track_direction.put(self.c15, "left");
        self.track_direction.put(self.c16, "left");
        self.track_direction.put(self.c17, "left");
        self.track_direction.put(self.c18, "left");
        
        self.track_direction.put(self.d12, "left");
        self.track_direction.put(self.d13, "left");
        self.track_direction.put(self.d14, "left");
        self.track_direction.put(self.d15, "left");
        self.track_direction.put(self.d16, "left");
        self.track_direction.put(self.d17, "left");
        self.track_direction.put(self.d18, "left");
        self.track_direction.put(self.d19, "left");
        self.track_direction.put(self.d20, "left");
        self.track_direction.put(self.d21, "left");
        self.track_direction.put(self.d22, "left");
        
        self.track_direction.put(self.x_21a_11a, "left");
        self.track_direction.put(self.x_13b_23b, "right");
        self.track_direction.put(self.x_24a2_14, "left");
        
        self.arrival_block_segment.put(1,self.a1);
        self.arrival_block_segment.put(2,self.b19);
        self.arrival_block_segment.put(3,self.c1);
        
        self.arrival_block_direction.put(1,"right");
        self.arrival_block_direction.put(2,"left");
        self.arrival_block_direction.put(3,"right");
        
        self.arrival_block_station.put(1,"JNG");
        self.arrival_block_station.put(2,"BKS");
        self.arrival_block_station.put(3,"JNG");
        
        self.arrival_block_name.put(2,"entry_block_2");
        self.arrival_block_name.put(1,"entry_block_1");
        self.arrival_block_name.put(3,"entry_block_3");
        
        self.exit_block_segment.put(self.b1,"exit_block_2");
        self.exit_block_segment.put(self.a20,"exit_block_1");
        self.exit_block_segment.put(self.d1,"exit_block_3");
    }
    
    public void initiateSignal(){
        /* --- Segment Type --- */
        self.segment_type.put(self.signal_b101, "signal");
        self.segment_type.put(self.signal_b105, "signal");
        self.segment_type.put(self.signal_b201, "signal");
        self.segment_type.put(self.signal_b209, "signal");
        self.segment_type.put(self.signal_b208, "signal");
        self.segment_type.put(self.signal_b301, "signal");
        self.segment_type.put(self.signal_b406, "signal");
        self.segment_type.put(self.signal_j10, "signal");
        self.segment_type.put(self.signal_j12, "signal");
        self.segment_type.put(self.signal_j22, "signal");
        self.segment_type.put(self.signal_j32, "signal");
        self.segment_type.put(self.signal_j42, "signal");
        self.segment_type.put(self.signal_j24, "signal");
        self.segment_type.put(self.signal_j60, "signal");
        self.segment_type.put(self.signal_j62, "signal");
        self.segment_type.put(self.signal_j82, "signal");
        self.segment_type.put(self.signal_uj10, "signal");
        /* --- Is Segment Clickable --- */
        self.is_segment_clickable.put(self.signal_j10, true);
        self.is_segment_clickable.put(self.signal_j12, true);
        self.is_segment_clickable.put(self.signal_j22, true);
        self.is_segment_clickable.put(self.signal_j32, true);
        self.is_segment_clickable.put(self.signal_j42, true);
        self.is_segment_clickable.put(self.signal_j24, true);
        self.is_segment_clickable.put(self.signal_j60, true);
        self.is_segment_clickable.put(self.signal_j62, true);
        self.is_segment_clickable.put(self.signal_j82, true);
        /* --- Signal State --- */
        self.signal_state.put(self.signal_b101, "yellow");
        self.signal_state.put(self.signal_b105, "green");
        self.signal_state.put(self.signal_b201, "yellow");
        self.signal_state.put(self.signal_b209, "green");
        self.signal_state.put(self.signal_b208, "green");
        self.signal_state.put(self.signal_b301, "yellow");
        self.signal_state.put(self.signal_b406, "green");
        self.signal_state.put(self.signal_j10, "red");
        self.signal_state.put(self.signal_j12, "red");
        self.signal_state.put(self.signal_j22, "red");
        self.signal_state.put(self.signal_j32, "red");
        self.signal_state.put(self.signal_j42, "red");
        self.signal_state.put(self.signal_j24, "red");
        self.signal_state.put(self.signal_j60, "red");
        self.signal_state.put(self.signal_j62, "red");
        self.signal_state.put(self.signal_j82, "red");
        self.signal_state.put(self.signal_uj10, "red");
        
        /* --- Signal Direction --- */
        self.signal_direction.put(self.signal_b101, "right");
        self.signal_direction.put(self.signal_b105, "right");
        self.signal_direction.put(self.signal_b201, "left");
        self.signal_direction.put(self.signal_b209, "left");
        self.signal_direction.put(self.signal_b208, "left");
        self.signal_direction.put(self.signal_b301, "right");
        self.signal_direction.put(self.signal_b406, "left");
        self.signal_direction.put(self.signal_j10, "right");
        self.signal_direction.put(self.signal_j12, "right");
        self.signal_direction.put(self.signal_j22, "left");
        self.signal_direction.put(self.signal_j32, "right");
        self.signal_direction.put(self.signal_j42, "left");
        self.signal_direction.put(self.signal_j24, "left");
        self.signal_direction.put(self.signal_j60, "right");
        self.signal_direction.put(self.signal_j62, "right");
        self.signal_direction.put(self.signal_j82, "left");
        self.signal_direction.put(self.signal_uj10, "right");
        
        /* --- Signal Type --- */
        self.signal_type.put(self.signal_b101, "blok");
        self.signal_type.put(self.signal_b105, "blok");
        self.signal_type.put(self.signal_b201, "blok");
        self.signal_type.put(self.signal_b209, "blok");
        self.signal_type.put(self.signal_b208, "blok");
        self.signal_type.put(self.signal_b301, "blok");
        self.signal_type.put(self.signal_b406, "blok");
        self.signal_type.put(self.signal_j10, "blok");
        self.signal_type.put(self.signal_j12, "blok");
        self.signal_type.put(self.signal_j22, "blok");
        self.signal_type.put(self.signal_j32, "blok");
        self.signal_type.put(self.signal_j42, "blok");
        self.signal_type.put(self.signal_j24, "blok");
        self.signal_type.put(self.signal_j60, "masuk2");
        self.signal_type.put(self.signal_j62, "blok");
        self.signal_type.put(self.signal_j82, "blok");
        self.signal_type.put(self.signal_uj10, "repeater");
        
        /* --- Block Of Signal --- */
        self.block_of_signal.put(self.signal_b101, self.block_101);
        self.block_of_signal.put(self.signal_b105, self.block_103);
        self.block_of_signal.put(self.signal_b201, self.block_203);
        self.block_of_signal.put(self.signal_b209, self.block_202);
        self.block_of_signal.put(self.signal_b208, self.block_201);
        self.block_of_signal.put(self.signal_b301, self.block_301);
        self.block_of_signal.put(self.signal_b406, self.block_401);
        self.block_of_signal.put(self.signal_j10, self.block_102);
        self.block_of_signal.put(self.signal_j12, self.block_020);
        self.block_of_signal.put(self.signal_j22, self.block_030);
        self.block_of_signal.put(self.signal_j42, self.block_040);
        self.block_of_signal.put(self.signal_j32, self.block_010);
        self.block_of_signal.put(self.signal_j24, self.block_202b);
        self.block_of_signal.put(self.signal_j60, self.block_302);
        self.block_of_signal.put(self.signal_j62, self.block_050);
        self.block_of_signal.put(self.signal_j82, self.block_060);
        
        /* --- Signal Of Block --- */
        self.signal_of_block.put(self.block_101, self.signal_b101);
        self.signal_of_block.put(self.block_103, self.signal_b105);
        self.signal_of_block.put(self.block_203, self.signal_b201);
        self.signal_of_block.put(self.block_202, self.signal_b209);
        self.signal_of_block.put(self.block_201, self.signal_b208);
        self.signal_of_block.put(self.block_301, self.signal_b301);
        self.signal_of_block.put(self.block_401, self.signal_b406);
        self.signal_of_block.put(self.block_102, self.signal_j10);
        self.signal_of_block.put(self.block_020, self.signal_j12);
        self.signal_of_block.put(self.block_030, self.signal_j22);
        self.signal_of_block.put(self.block_040, self.signal_j42);
        self.signal_of_block.put(self.block_010, self.signal_j32);
        self.signal_of_block.put(self.block_202b, self.signal_j24);
        self.signal_of_block.put(self.block_302, self.signal_j60);
        self.signal_of_block.put(self.block_050, self.signal_j62);
        self.signal_of_block.put(self.block_060, self.signal_j82);
        
        /* --- Segment Name --- */
        self.number_of_segment.put(self.signal_b101, "B101");
        self.number_of_segment.put(self.signal_b105, "B105");
        self.number_of_segment.put(self.signal_b201, "B201");
        self.number_of_segment.put(self.signal_b209, "B209");
        self.number_of_segment.put(self.signal_b208, "B208");
        self.number_of_segment.put(self.signal_b301, "B301");
        self.number_of_segment.put(self.signal_b406, "B406");
        self.number_of_segment.put(self.signal_j10, "J10");
        self.number_of_segment.put(self.signal_j12, "J12");
        self.number_of_segment.put(self.signal_j22, "J22");
        self.number_of_segment.put(self.signal_j32, "J32");
        self.number_of_segment.put(self.signal_j42, "J42");
        self.number_of_segment.put(self.signal_j24, "J24");
        self.number_of_segment.put(self.signal_j60, "J60");
        self.number_of_segment.put(self.signal_j62, "J62");
        self.number_of_segment.put(self.signal_j82, "J82");
        self.number_of_segment.put(self.signal_uj10, "UJ10");
        
        /* --- Controlable Signal --- */
        self.controlable_signal = new JLabel[]{self.signal_j10,self.signal_j12,self.signal_j22,self.signal_j32,self.signal_j42,self.signal_j24,self.signal_j60,self.signal_j62,self.signal_j82};
        
        /* --- Relative Signal --- */
        self.relative_signal.put(self.signal_j10, self.signal_b101);
        self.relative_signal.put(self.signal_j24, self.signal_b201);
        self.relative_signal.put(self.signal_b208, self.signal_b209);
        self.relative_signal.put(self.signal_j60, self.signal_b301);
        
        /* --- Repeater Of Signal --- */
        self.repeater_of_signal.put(self.signal_j10, self.signal_uj10);
        
        /* --- Speed Signal --- */
        self.speed_signal.put(self.signal_j10, self.speed_signal_j10);
        self.speed_signal.put(self.signal_j24, self.speed_signal_j24);
        
        self.is_speed_signal_on.put(self.signal_j10, false);
        self.is_speed_signal_on.put(self.signal_j24, false);
    }
    
    private void initiateBlock(){
        /* --- Segment Type */
        self.segment_type.put(self.block_101, "block");
        self.segment_type.put(self.block_103, "block");
        self.segment_type.put(self.block_203, "block");
        self.segment_type.put(self.block_202, "block");
        self.segment_type.put(self.block_201, "block");
        self.segment_type.put(self.block_301, "block");
        self.segment_type.put(self.block_401, "block");
        self.segment_type.put(self.block_102, "block");
        self.segment_type.put(self.block_020, "block");
        self.segment_type.put(self.block_030, "block");
        self.segment_type.put(self.block_040, "block");
        self.segment_type.put(self.block_010, "block");
        self.segment_type.put(self.block_202b, "block");
        self.segment_type.put(self.block_302, "block");
        self.segment_type.put(self.block_050, "block");
        self.segment_type.put(self.block_060, "block");
        
        /* --- Is Segment Reserved ---*/
        self.is_segment_reserved.put(self.block_101, false);
        self.is_segment_reserved.put(self.block_103, false);
        self.is_segment_reserved.put(self.block_203, false);
        self.is_segment_reserved.put(self.block_202, false);
        self.is_segment_reserved.put(self.block_201, false);
        self.is_segment_reserved.put(self.block_301, false);
        self.is_segment_reserved.put(self.block_401, false);
        self.is_segment_reserved.put(self.block_102, false);
        self.is_segment_reserved.put(self.block_020, false);
        self.is_segment_reserved.put(self.block_030, false);
        self.is_segment_reserved.put(self.block_040, false);
        self.is_segment_reserved.put(self.block_010, false);
        self.is_segment_reserved.put(self.block_202b, false);
        self.is_segment_reserved.put(self.block_302, false);
        self.is_segment_reserved.put(self.block_050, false);
        self.is_segment_reserved.put(self.block_060, false);
        
        /* --- Segment Next Left ---*/
        self.segment_next_left.put(self.block_101, self.a1);
        self.segment_next_left.put(self.block_103, self.a19);
        self.segment_next_left.put(self.block_203, self.b18);
        self.segment_next_left.put(self.block_202, self.b3);
        self.segment_next_left.put(self.block_201, self.b1);
        self.segment_next_left.put(self.block_301, self.c1);
        self.segment_next_left.put(self.block_401, self.d1);
        self.segment_next_left.put(self.block_102, self.a3);
        self.segment_next_left.put(self.block_020, self.a8);
        self.segment_next_left.put(self.block_030, self.b8);
        self.segment_next_left.put(self.block_040, self.bb4);
        self.segment_next_left.put(self.block_010, self.ab4);
        self.segment_next_left.put(self.block_202b, self.b16);
        self.segment_next_left.put(self.block_302, self.c3);
        self.segment_next_left.put(self.block_050, self.c9);
        self.segment_next_left.put(self.block_060, self.d10);
        
        /* --- Segment Next Right ---*/
        self.segment_next_right.put(self.block_101, self.a2);
        self.segment_next_right.put(self.block_103, self.a20);
        self.segment_next_right.put(self.block_203, self.b19);
        self.segment_next_right.put(self.block_202, self.b4);
        self.segment_next_right.put(self.block_201, self.b2);
        self.segment_next_right.put(self.block_301, self.c2);
        self.segment_next_right.put(self.block_401, self.d2);
        self.segment_next_right.put(self.block_102, self.a4);
        self.segment_next_right.put(self.block_020, self.a9);
        self.segment_next_right.put(self.block_030, self.b9);
        self.segment_next_right.put(self.block_040, self.bb5);
        self.segment_next_right.put(self.block_010, self.ab5);
        self.segment_next_right.put(self.block_202b, self.b17);
        self.segment_next_right.put(self.block_302, self.c4);
        self.segment_next_right.put(self.block_050, self.c10);
        self.segment_next_right.put(self.block_060, self.d11);
        
        /* --- Number Of Segment --- */
        self.number_of_segment.put(self.block_101, "101");
        self.number_of_segment.put(self.block_103, "103");
        self.number_of_segment.put(self.block_203, "203");
        self.number_of_segment.put(self.block_202, "202");
        self.number_of_segment.put(self.block_201, "201");
        self.number_of_segment.put(self.block_301, "301");
        self.number_of_segment.put(self.block_401, "401");
        self.number_of_segment.put(self.block_102, "102");
        self.number_of_segment.put(self.block_020, "020");
        self.number_of_segment.put(self.block_030, "030");
        self.number_of_segment.put(self.block_040, "040");
        self.number_of_segment.put(self.block_010, "010");
        self.number_of_segment.put(self.block_202b, "202B");
        self.number_of_segment.put(self.block_302, "302");
        self.number_of_segment.put(self.block_050, "050");
        self.number_of_segment.put(self.block_060, "060");
        
        /* --- Is Train On Segment ---*/
        self.is_train_on_segment.put(self.block_101, false);
        self.is_train_on_segment.put(self.block_103, false);
        self.is_train_on_segment.put(self.block_203, false);
        self.is_train_on_segment.put(self.block_202, false);
        self.is_train_on_segment.put(self.block_201, false);
        self.is_train_on_segment.put(self.block_301, false);
        self.is_train_on_segment.put(self.block_401, false);
        self.is_train_on_segment.put(self.block_102, false);
        self.is_train_on_segment.put(self.block_020, false);
        self.is_train_on_segment.put(self.block_030, false);
        self.is_train_on_segment.put(self.block_040, false);
        self.is_train_on_segment.put(self.block_010, false);
        self.is_train_on_segment.put(self.block_202b, false);
        self.is_train_on_segment.put(self.block_302, false);
        self.is_train_on_segment.put(self.block_050, false);
        self.is_train_on_segment.put(self.block_060, false);
        
        self.is_entry_signal_block.put(self.block_102, true);
        self.is_entry_signal_block.put(self.block_202b, true);
        
        self.is_adjacent_small_station.put(self.block_101, true);
        self.is_adjacent_small_station.put(self.block_201, true);
        
        self.platform_number.put(self.block_010, "1");
        self.platform_number.put(self.block_020, "2");
        self.platform_number.put(self.block_030, "3");
        self.platform_number.put(self.block_040, "4");
        self.platform_number.put(self.block_050, "5");
        self.platform_number.put(self.block_060, "6");
        
        self.platform_departure_approved.put(self.block_010, false);
        self.platform_departure_approved.put(self.block_020, false);
        self.platform_departure_approved.put(self.block_030, false);
        self.platform_departure_approved.put(self.block_040, false);
        self.platform_departure_approved.put(self.block_050, false);
        self.platform_departure_approved.put(self.block_060, false);
        
        Object[] block_102_turn_condition = new Object[]{self.junction_11b};
        Object[] block_202b_turn_condition = new Object[]{self.junction_24,self.junction_23b,self.junction_23a,self.junction_24a1};
        self.turn_condition.put(self.block_102, block_102_turn_condition);
        self.turn_condition.put(self.block_202b, block_202b_turn_condition);
        
        /* --- Block Notification --- */
        self.block_notification_trigger.put(self.block_103, "BKS");
        self.block_notification_trigger.put(self.block_202, "JNG");
        self.block_notification_trigger.put(self.block_401, "JNG");
    }
    
    private void initiateTravelTime(){
        /* distance in meter */
        
        /* --- Initialize distance --- */
        Object[][] distances = {
            {self.block_101,self.block_102,670},
            {self.block_102,self.block_010,610},
            {self.block_102,self.block_020,610},
            {self.block_203,self.block_202b,815},
            {self.block_202b,self.block_010,850},
            {self.block_202b,self.block_020,850},
            {self.block_202b,self.block_030,850},
            {self.block_202b,self.block_040,850},
            {self.block_202b,self.block_050,850},
            {self.block_202b,self.block_060,1150},
            {self.block_202,self.block_201,500},
            {self.block_301,self.block_302,870},
            {self.block_302,self.block_050,800},
            {self.block_010,self.block_103,1020},
            {self.block_020,self.block_103,1020},
            {self.block_030,self.block_202,820},
            {self.block_040,self.block_202,820},
            {self.block_050,self.block_103,1020},
            {self.block_060,self.block_401,1172},
            {self.block_102,self.junction_11a,250},
            {self.junction_11a,self.junction_11b,30},
            {self.junction_11b,self.block_030,365},
            {self.block_010,self.junction_13a,170},
            {self.block_020,self.junction_13a,170},
            {self.junction_13a,self.junction_13b,30},
            {self.junction_13b,self.junction_23b,30},
            {self.junction_13b,self.junction_14,185},
            {self.junction_14,self.block_103,560},
            {self.block_202b,self.junction_24,80},
            {self.junction_24,self.junction_24a2,65},
            {self.junction_24a2,self.junction_24a1,22},
            {self.junction_24a1,self.junction_23b,90},
            {self.junction_23b,self.junction_23a,60},
            {self.junction_23a,self.block_030,465},
            {self.junction_23a,self.block_040,465},
            {self.block_030,self.junction_21b,120},
            {self.block_040,self.junction_21b,120},
            {self.junction_21b,self.junction_21a,68},
            {self.block_050,self.junction_24a1,201},
            {self.junction_24a2,self.junction_14,60},
            {self.junction_24,self.block_060,705}
        };
        
        /* --- Parsing variable --- */
        for (int i = 0; i < distances.length; i++) {
            Map<Object,Integer> distance_collection = null;
            if (self.segment_distance.containsKey(distances[i][0])){
                /* --- distance --- */
                distance_collection = self.segment_distance.get(distances[i][0]);
            } else {
                distance_collection = new HashMap<Object,Integer>();
            }
            distance_collection.put(distances[i][1], (int) distances[i][2]);
            self.segment_distance.put(distances[i][0], distance_collection);
            
            if (self.segment_distance.containsKey(distances[i][1])){
                /* --- distance --- */
                distance_collection = self.segment_distance.get(distances[i][1]);
            } else {
                distance_collection = new HashMap<Object,Integer>();
            }
            distance_collection.put(distances[i][0], (int) distances[i][2]);
            self.segment_distance.put(distances[i][1], distance_collection);
        }
        
        self.entry_exit_distance.put("exit_block_1",490);
        self.entry_exit_distance.put("exit_block_2",760);
        self.entry_exit_distance.put("exit_block_3",1005);
        self.entry_exit_distance.put("entry_block_1",635);
        self.entry_exit_distance.put("entry_block_2",679);
        self.entry_exit_distance.put("entry_block_3", 902);
    }
}
