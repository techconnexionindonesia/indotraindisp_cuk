package indotraindisp_cuk;

import java.util.HashMap;
import java.util.Map;
import indotraindisp_cuk.language.En;
import indotraindisp_cuk.language.Id;

/**
 *
 * @author TCIPRO
 */
public class Lang {
    String language = "en";
    Map<String,String> texts = new HashMap<String,String>();
    ControlPanelCUK self = null;
    
    public Lang(ControlPanelCUK parent){
        self = parent;
        En en = new En();
        Id id = new Id();
        if (self.utility.lang != null){
            language = self.utility.lang;
        }
        System.out.println("Language : "+self.utility.lang);
        switch (language){
            case "en":
                texts = en.text;
                break;
                
            case "id":
                texts = id.text;
                break;
        }
    }
    
    public String text(String lang_text){
        return texts.get(lang_text);
    }
    
    public String text(String lang_text, String parameter_1){
        String original_text = texts.get(lang_text);
        String result_text = original_text.replace("%1", parameter_1);
        return result_text;
    }
    
    public String text(String lang_text, String parameter_1, String parameter_2){
        String original_text = texts.get(lang_text);
        String result_text = original_text.replace("%1", parameter_1);
        result_text = result_text.replace("%2", parameter_2);
        return result_text;
    }
}
