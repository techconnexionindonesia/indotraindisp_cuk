/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indotraindisp_cuk;

import java.awt.Button;
import java.awt.Color;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author Tech Connexion Indonesia
 */
public class Switcher {
    ControlPanelCUK self = null;
    Map<String,Color> color = new HashMap<String,Color>();
    Map<Object,Boolean> on_blink_looping = new HashMap<Object,Boolean>();
    Map<Object,Boolean> on_blink_stopping = new HashMap<Object,Boolean>();
    
    public Switcher(ControlPanelCUK parent){
        self = parent;
        initializeColor();
    }
    
    void initializeColor(){
        color.put("red", new java.awt.Color(255, 0, 0));
        color.put("green", new java.awt.Color(0, 255, 0));
        color.put("gray", new java.awt.Color(204, 204, 204));
        color.put("dark_gray", new java.awt.Color(102, 102, 102));
    }
    
    /**
     * Get full filename for set icon
     */
    public javax.swing.ImageIcon getFileName(String filename){
        String img_path = self.utility.getPath() + "/data/img/";
        return new javax.swing.ImageIcon(img_path+filename);
    }
    
    /**
     * Change Background Color of an element
     */
    public void changeBgColor(Button element, String selected_color){
        element.setBackground(color.get(selected_color));
    }
    
    public void changeBgColor(JButton element, String selected_color){
        element.setBackground(color.get(selected_color));
    }
    
    public void changeBgColor(JTextField element, String selected_color){
        element.setBackground(color.get(selected_color));
    }
    
    public void changeBgColor(JLabel element, String selected_color){
        element.setBackground(color.get(selected_color));
    }
    
    public void quickBgColor(Button element, String selected_color){
        new Thread(()->{
            try {
                Color original_color = element.getBackground();
                changeBgColor(element, selected_color);
                Thread.sleep(300);
                element.setBackground(original_color);
            } catch (InterruptedException ex) {
                Logger.getLogger(Switcher.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }
    
    public void quickBgColor(JButton element, String selected_color){
        new Thread(()->{
            try {
                Color original_color = element.getBackground();
                changeBgColor(element, selected_color);
                Thread.sleep(300);
                element.setBackground(original_color);
            } catch (InterruptedException ex) {
                Logger.getLogger(Switcher.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }
    
    public void quickBgColor(JTextField element, String selected_color){
        new Thread(()->{
            try {
                Color original_color = element.getBackground();
                changeBgColor(element, selected_color);
                Thread.sleep(300);
                element.setBackground(original_color);
            } catch (InterruptedException ex) {
                Logger.getLogger(Switcher.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }
    
    public void quickBgColor(JLabel element, String selected_color){
        new Thread(()->{
            try {
                Color original_color = element.getBackground();
                changeBgColor(element, selected_color);
                Thread.sleep(300);
                element.setBackground(original_color);
            } catch (InterruptedException ex) {
                Logger.getLogger(Switcher.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }
    
    /**
     * Blink Background Color for loop
     */
    public void blinkBgColor(Button element, String color_from, String color_to){
        new Thread(()->{
            try {
                if (on_blink_looping.containsKey(element)){
                    on_blink_stopping.put(element, true);
                    while (on_blink_looping.get(element)){
                        Thread.sleep(500);
                    }
                }
                on_blink_looping.put(element, true);
                on_blink_stopping.put(element,false);
                while(on_blink_looping.get(element) && on_blink_stopping.get(element) == false){
                    changeBgColor(element, color_from);
                    Thread.sleep(500);
                    changeBgColor(element, color_to);
                    Thread.sleep(500);  
                }
                on_blink_looping.put(element, false);
                on_blink_stopping.put(element, false);
            } catch (InterruptedException ex) {
                Logger.getLogger(Switcher.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }
    
    public void blinkBgColor(JButton element, String color_from, String color_to){
        new Thread(()->{
            try {
                if (on_blink_looping.containsKey(element)){
                    on_blink_stopping.put(element, true);
                    while (on_blink_looping.get(element)){
                        Thread.sleep(500);
                    }
                }
                on_blink_looping.put(element, true);
                on_blink_stopping.put(element,false);
                while(on_blink_looping.get(element) && on_blink_stopping.get(element) == false){
                    changeBgColor(element, color_from);
                    Thread.sleep(500);
                    changeBgColor(element, color_to);
                    Thread.sleep(500);  
                }
                on_blink_looping.put(element, false);
                on_blink_stopping.put(element, false);
            } catch (InterruptedException ex) {
                Logger.getLogger(Switcher.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }
    
    public void blinkBgColor(JTextField element, String color_from, String color_to){
        new Thread(()->{
            try {
                if (on_blink_looping.containsKey(element)){
                    on_blink_stopping.put(element, true);
                    while (on_blink_looping.get(element)){
                        Thread.sleep(500);
                    }
                }
                on_blink_looping.put(element, true);
                on_blink_stopping.put(element,false);
                while(on_blink_looping.get(element) && on_blink_stopping.get(element) == false){
                    changeBgColor(element, color_from);
                    Thread.sleep(500);
                    changeBgColor(element, color_to);
                    Thread.sleep(500);  
                }
                on_blink_looping.put(element, false);
                on_blink_stopping.put(element, false);
            } catch (InterruptedException ex) {
                Logger.getLogger(Switcher.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }
    
    public void blinkBgColor(JLabel element, String color_from, String color_to){
        new Thread(()->{
            try {
                if (on_blink_looping.containsKey(element)){
                    on_blink_stopping.put(element, true);
                    while (on_blink_looping.get(element)){
                        Thread.sleep(500);
                    }
                }
                on_blink_looping.put(element, true);
                on_blink_stopping.put(element,false);
                while(on_blink_looping.get(element) && on_blink_stopping.get(element) == false){
                    changeBgColor(element, color_from);
                    Thread.sleep(500);
                    changeBgColor(element, color_to);
                    Thread.sleep(500);  
                }
                on_blink_looping.put(element, false);
                on_blink_stopping.put(element, false);
            } catch (InterruptedException ex) {
                Logger.getLogger(Switcher.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }
    
    /**
     * Stop blinking element
     */
    public void stopBlinkBgColor(Object element){
        on_blink_stopping.put(element, true);
    }
    
    /**
     * Blink Background Color of an element for specified time
     */
    public void blinkBgColor(Button element, String color_from, String color_to, int second){
        new Thread(()->{
            for (int i = 0; i < second; i++) {
                try {
                    changeBgColor(element, color_from);
                    Thread.sleep(500);
                    changeBgColor(element, color_to);
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Switcher.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }).start();
    }
    
    public void blinkBgColor(JButton element, String color_from, String color_to, int second){
        new Thread(()->{
            for (int i = 0; i < second; i++) {
                try {
                    changeBgColor(element, color_from);
                    Thread.sleep(500);
                    changeBgColor(element, color_to);
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Switcher.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }).start();
    }
    
    public void blinkBgColor(JTextField element, String color_from, String color_to, int second){
        new Thread(()->{
            for (int i = 0; i < second; i++) {
                try {
                    changeBgColor(element, color_from);
                    Thread.sleep(500);
                    changeBgColor(element, color_to);
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Switcher.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }).start();
    }
    
    public void blinkBgColor(JLabel element, String color_from, String color_to, int second){
        new Thread(()->{
            for (int i = 0; i < second; i++) {
                try {
                    changeBgColor(element, color_from);
                    Thread.sleep(500);
                    changeBgColor(element, color_to);
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Switcher.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }).start();
    }
    
    /**
     * Blink invisible of an element
     * @param element 
     */
    public void blinkBgInvisible(JLabel element){
        new Thread(()->{
            try {
                if (on_blink_looping.containsKey(element)){
                    on_blink_stopping.put(element, true);
                    while (on_blink_looping.get(element)){
                        Thread.sleep(500);
                    }
                }
                on_blink_looping.put(element, true);
                on_blink_stopping.put(element,false);
                while(on_blink_looping.get(element) && on_blink_stopping.get(element) == false){
                    element.setVisible(false);
                    Thread.sleep(500);
                    element.setVisible(true);
                    Thread.sleep(500);
                }
                on_blink_looping.put(element, false);
                on_blink_stopping.put(element, false);
            } catch (InterruptedException ex) {
                Logger.getLogger(Switcher.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }
    
    public void blinkBgInvisible(Button element){
        new Thread(()->{
            try {
                if (on_blink_looping.containsKey(element)){
                    on_blink_stopping.put(element, true);
                    while (on_blink_looping.get(element)){
                        Thread.sleep(500);
                    }
                }
                on_blink_looping.put(element, true);
                on_blink_stopping.put(element,false);
                while(on_blink_looping.get(element) && on_blink_stopping.get(element) == false){
                    element.setVisible(false);
                    Thread.sleep(500);
                    element.setVisible(true);
                    Thread.sleep(500);
                }
                on_blink_looping.put(element, false);
                on_blink_stopping.put(element, false);
            } catch (InterruptedException ex) {
                Logger.getLogger(Switcher.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }
    
    public void blinkBgInvisible(JButton element){
        new Thread(()->{
            try {
                if (on_blink_looping.containsKey(element)){
                    on_blink_stopping.put(element, true);
                    while (on_blink_looping.get(element)){
                        Thread.sleep(500);
                    }
                }
                on_blink_looping.put(element, true);
                on_blink_stopping.put(element,false);
                while(on_blink_looping.get(element) && on_blink_stopping.get(element) == false){
                    element.setVisible(false);
                    Thread.sleep(500);
                    element.setVisible(true);
                    Thread.sleep(500);
                }
                on_blink_looping.put(element, false);
                on_blink_stopping.put(element, false);
            } catch (InterruptedException ex) {
                Logger.getLogger(Switcher.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }
    
    public void blinkBgInvisible(JTextField element){
        new Thread(()->{
            try {
                if (on_blink_looping.containsKey(element)){
                    on_blink_stopping.put(element, true);
                    while (on_blink_looping.get(element)){
                        Thread.sleep(500);
                    }
                }
                on_blink_looping.put(element, true);
                on_blink_stopping.put(element,false);
                while(on_blink_looping.get(element) && on_blink_stopping.get(element) == false){
                    element.setVisible(false);
                    Thread.sleep(500);
                    element.setVisible(true);
                    Thread.sleep(500);
                }
                on_blink_looping.put(element, false);
                on_blink_stopping.put(element, false);
            } catch (InterruptedException ex) {
                Logger.getLogger(Switcher.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }
    
    /**
     * Junction Switching
     */
    public void switchJunction(JLabel element){
        new Thread(()->{
            String horizontal_heading = self.junction_horizontal_heading.get(element);
            String vertical_heading = self.junction_vertical_heading.get(element) == "up" ? "Upside" : "";
            String is_turned = self.is_junction_turned.get(element) ? "Straight" : "Turn";
            String file_format = ".png";
            String filename = "switch"+horizontal_heading+is_turned+"Black"+vertical_heading+file_format;
            element.setIcon(getFileName(filename));
            Boolean is_turned_now = self.is_junction_turned.get(element) ? false : true;
            self.is_junction_turned.put(element, is_turned_now);
        }).start();
    }
    
    /**
     * Track color switching
     */
    public void switchTrack(Object object,String color){
        new Thread(()->{
            String file_format = ".png";
            String filename = "";
            JLabel element = (JLabel) object;
            if (self.segment_type.get(element).equals("track")){
                String track_type = self.track_type.get(element);
                if (track_type.equals("straight")){
                    filename = "track_"+color+file_format;
                } else {
                    String direction = self.track_direction.get(element);
                    filename = "trackDiagonal"+direction+color+file_format;
                }
            } else {
                String horizontal_heading = self.junction_horizontal_heading.get(element);
                String vertical_heading = self.junction_vertical_heading.get(element) == "up" ? "Upside" : "";
                String is_turned = self.is_junction_turned.get(element) ? "Turn" : "Straight";
                filename = "switch"+horizontal_heading+is_turned+color+vertical_heading+file_format;
                element.setIcon(getFileName(filename));
            }
            element.setIcon(getFileName(filename));
        }).start();
    }
    
    /**
     * Signal switching
     */
    public void switchSignal(JLabel signal,String color_parameter){
        new Thread(()->{
            String color = color_parameter;
            if (self.signal_type.get(signal).equals("masuk2") && color.equals("green")) color = "yellow";
            String start_state = self.signal_state.get(signal);
            self.signal_state.put(signal,color);
            String file_format = ".png";
            String filename = "";
            String signal_type = self.signal_type.get(signal);
            String direction = self.signal_direction.get(signal) == "left" ? "kiri" : "kanan";
            String color_filename = "red";
            switch (color){
                case "red":
                    color_filename = "merah";
                    break;

                case "yellow":
                    color_filename = "kuning";
                    break;

                case "green":
                    color_filename = "hijau";
                    break;
            }
            filename = "Sinyal"+signal_type+color_filename+direction+file_format;
            signal.setIcon(getFileName(filename));
            if (!start_state.equals(color)) self.audio.signalSwitch();
            if (self.repeater_of_signal.containsKey(signal)){
                JLabel repeater = self.repeater_of_signal.get(signal);
                switchSignal(repeater,color);
            }
            if (color.equals("red")){
                switchSpeedSignal(signal,"off");
            }
        }).start();
    }
    
    /**
     * Speed Signal switching
     */
    public void switchSpeedSignal(JLabel signal,String state){
        if (self.speed_signal.containsKey(signal)){
            JLabel speed_signal = self.speed_signal.get(signal);
            String file_format = ".png";
            String filename = "";
            String direction = self.signal_direction.get(signal) == "left" ? "kiri" : "kanan";
            filename = "taspat30"+state+direction+file_format;
            speed_signal.setIcon(getFileName(filename));
            self.is_speed_signal_on.put(signal,state.equals("on") ? true : false);
        }
    }
    
    /**
     * Telephone Button switching
     */
    public void switchButtonTelephone(JLabel element, boolean is_on){
        new Thread(()->{
            String file_format = ".png";
            String filename = "iconTelepon"+(is_on ? "On":"Off")+file_format;
            element.setIcon(getFileName(filename));
        }).start();
    }
}
