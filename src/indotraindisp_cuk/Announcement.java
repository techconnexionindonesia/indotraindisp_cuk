package indotraindisp_cuk;

import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tech Connexion Indonesia
 */
public class Announcement {
    ControlPanelCUK self = null;
    boolean audioPlaying = false;
    
    public Announcement(ControlPanelCUK parent){
        self = parent;
    }
    
    public void stationaryAnnouncement(){
        new Thread(()->{
            while (self.is_simulation_running){
                try {
                    Thread.sleep(300*1000);
                    String basename = "announcement_station";
                    String filename = self.audio.getRandomAudioFilenameAnnouncement(basename);
                    self.audio.prepareAudio(filename, "announcement_station", false);
                    self.announcement_on_going.add("announcement_station");
                    runAudioRunner("announcement_station");
                } catch (InterruptedException ex) {
                    Logger.getLogger(Announcement.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }).start();
    }
    
    public void trainDepAdjacentStation(String train_number,String station){
        new Thread(()->{
            try {
                String destination = self.train_destination.get(train_number);
                String type = self.train_type.get(train_number);
                if (type.equals("krl") || type.equals("krlf")){
                    String basename = "announcement_krl_"+destination+"_dep_"+station;
                    String filename = self.audio.getRandomAudioFilenameAnnouncement(basename);
                    self.audio.prepareAudio(filename, train_number+"_dep_adjacent", false);
                    self.announcement_on_going.add(train_number+"_dep_adjacent");
                    int random_wait_time = self.utility.getRandomNumber(1, 10);
                    Thread.sleep(random_wait_time*1000);
                    runAudioRunner(train_number+"_dep_adjacent");
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(Announcement.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }
    
    public void trainEnterPlatform(String train_number){
        new Thread(()->{
            try {
                String etd = self.train_etd.get(train_number);
                String destination = self.train_destination.get(train_number);
                String platform = self.train_selected_platform.get(train_number);
                String basename = "announcement_platform_"+platform+"_"+(etd.equals("Ls")? etd:destination);
                String filename = self.audio.getRandomAudioFilenameAnnouncement(basename);
                self.audio.prepareAudio(filename, train_number+"_arrival", false);
                self.announcement_on_going.add(train_number+"_arrival");
                int random_wait_time = self.utility.getRandomNumber(1, 10);
                Thread.sleep(random_wait_time*1000);
                runAudioRunner(train_number+"_arrival");
            } catch (InterruptedException ex) {
                Logger.getLogger(Announcement.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }
    
    public void welcomingPassanger(){
        new Thread(()->{
            try {
                int random_wait_time = self.utility.getRandomNumber(1, 5);
                Thread.sleep(random_wait_time*1000);
                String basename = "announcement_welcome";
                String filename = self.audio.getRandomAudioFilenameAnnouncement(basename);
                self.audio.prepareAudio(filename, "announcement_welcome", false);
                self.announcement_on_going.add("announcement_welcome");
                runAudioRunner("announcement_welcome");
            } catch (InterruptedException ex) {
                Logger.getLogger(Announcement.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }
    
    private void runAudioRunner(String media_name){
        new Thread(()->{
            while (audioPlaying){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Announcement.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (!audioPlaying){
                try {
                    audioPlaying = true;
                    int length = self.audio.getLength(media_name);
                    self.audio.media_player.get(media_name).play();
                    Thread.sleep(length*1000);
                    Thread.sleep(3000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Announcement.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            audioPlaying = false;
        }).start();
    }
    
}
