package indotraindisp_cuk;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.embed.swing.JFXPanel;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

/**
 *
 * @author TCIPRO
 */
public class Audio {
    ControlPanelCUK self = null;
    String path_main = "";
    String path_system = "";
    String path_background = "";
    String path_train = "";
    String path_announcement = "";
    Map<String,String> file_path = new HashMap<String,String>();
    Map<String,String> once_file_path = new HashMap<String,String>(); // Path file variable for one time use audio
    Map<String,Boolean> is_loop = new HashMap<String,Boolean>();
    Map<String,Media> media = new HashMap<String,Media>();
    Map<String,MediaPlayer> media_player = new HashMap<String,MediaPlayer>();
    
    final JFXPanel fxPanel = new JFXPanel();
    
    public Audio(ControlPanelCUK parent){
        self = parent;
        path_main = self.utility.getPath() + "/data/sound/cuk/";
        path_system = path_main + "cuk_system/";
        path_background = path_main + "cuk_background/";
        path_train = path_main + "cuk_train/";
        path_announcement = path_main + "cuk_announcement/"+self.announcer_voice+"/";
        initializeFilepath();
        initializeIsLoop();
        initializeMedia();
    }
    
    private void initializeFilepath(){
        // Utility sound
        file_path.put("unavailable", path_system + "cuk_unavailable.mp3");
        file_path.put("master_off", path_system + "cuk_master_off.mp3");
        file_path.put("master_on", path_system + "cuk_master_on.mp3");
        file_path.put("panel_switching", path_system + "cuk_panel_switching.mp3");
        file_path.put("headpanel_click", path_system + "cuk_headpanel_click.mp3");
        file_path.put("junction_click", path_system + "cuk_junction_click.mp3");
        file_path.put("alert_general", path_system + "cuk_alert_general.mp3");
        file_path.put("failure", path_system + "cuk_failure.mp3");
        file_path.put("route_pattern", path_system + "cuk_route_pattern.mp3");
        file_path.put("signal_switch", path_system + "cuk_signal_switch.mp3");
        file_path.put("alert_arrival", path_system + "cuk_arrival.mp3");
        file_path.put("alert_notification_jng", path_system + "cuk_notification_jng.mp3");
        file_path.put("alert_notification_bks", path_system + "cuk_notification_bks.mp3");
        file_path.put("notify_station", path_system + "cuk_notify_station.mp3");
        file_path.put("bell", path_system + "cuk_bell.mp3");
        once_file_path.put("bg-idle_1", path_background + "bg-idle_1.mp3");
        once_file_path.put("bg-idle_2", path_background + "bg-idle_2.mp3");
        once_file_path.put("bg-idle_3", path_background + "bg-idle_3.mp3");
        once_file_path.put("emu_dep_1", path_train + "cuk_emu_dep_1.mp3");
        once_file_path.put("emu_dep_2", path_train + "cuk_emu_dep_2.mp3");
        once_file_path.put("emu_dep_3", path_train + "cuk_emu_dep_3.mp3");
        once_file_path.put("emu_arr_1", path_train + "cuk_emu_arr_1.mp3");
        once_file_path.put("emu_arr_2", path_train + "cuk_emu_arr_2.mp3");
        once_file_path.put("emu_arr_3", path_train + "cuk_emu_arr_3.mp3");
        once_file_path.put("emu_pass_highspeed_1", path_train + "cuk_emu_pass_highspeed_1.mp3");
        once_file_path.put("emu_pass_lowspeed_1", path_train + "cuk_emu_pass_lowspeed_1.mp3");
        once_file_path.put("emu_idle_1", path_train + "cuk_emu_idle_1.mp3");
        once_file_path.put("emu_idle_2", path_train + "cuk_emu_idle_2.mp3");
        once_file_path.put("emu_idle_3", path_train + "cuk_emu_idle_3.mp3");
        once_file_path.put("emu_open_door_1", path_train + "cuk_emu_open_door_1.mp3");
        once_file_path.put("emu_close_door_1", path_train + "cuk_emu_close_door_1.mp3");
        once_file_path.put("dmu_pass_highspeed_1", path_train + "cuk_dmu_pass_highspeed_1.mp3");
        once_file_path.put("dmu_pass_lowspeed_1", path_train + "cuk_dmu_pass_lowspeed_1.mp3");
        once_file_path.put("train_pass_highspeed_1", path_train + "cuk_train_pass_highspeed_1.mp3");
        once_file_path.put("train_pass_highspeed_2", path_train + "cuk_train_pass_highspeed_2.mp3");
        once_file_path.put("train_pass_lowspeed_1", path_train + "cuk_train_pass_lowspeed_1.mp3");
        once_file_path.put("freight_pass_lowspeed_1", path_train + "cuk_freight_pass_lowspeed_1.mp3");
        once_file_path.put("freight_pass_highspeed_1", path_train + "cuk_freight_pass_lowspeed_1.mp3");
        once_file_path.put("announcement_krl_BKS_dep_JNG_1", path_announcement + "announcement_krl_BKS_dep_JNG_1.mp3");
        once_file_path.put("announcement_krl_CKR_dep_JNG_1", path_announcement + "announcement_krl_CKR_dep_JNG_1.mp3");
        once_file_path.put("announcement_krl_CKR_dep_JNG_2", path_announcement + "announcement_krl_CKR_dep_JNG_2.mp3");
        once_file_path.put("announcement_krl_JAKK_dep_BKS_1", path_announcement + "announcement_krl_JAKK_dep_BKS_1.mp3");
        once_file_path.put("announcement_krl_JAKK_dep_BKS_2", path_announcement + "announcement_krl_JAKK_dep_BKS_2.mp3");
        once_file_path.put("announcement_krl_MRI_dep_BKS_1", path_announcement + "announcement_krl_MRI_dep_BKS_1.mp3");
        once_file_path.put("announcement_krl_MRI_dep_BKS_2", path_announcement + "announcement_krl_MRI_dep_BKS_2.mp3");
        once_file_path.put("announcement_platform_1_BKS_1", path_announcement + "announcement_platform_1_BKS_1.mp3");
        once_file_path.put("announcement_platform_1_BKS_2", path_announcement + "announcement_platform_1_BKS_2.mp3");
        once_file_path.put("announcement_platform_1_CKR_1", path_announcement + "announcement_platform_1_CKR_1.mp3");
        once_file_path.put("announcement_platform_1_CKR_2", path_announcement + "announcement_platform_1_CKR_2.mp3");
        once_file_path.put("announcement_platform_2_BKS_1", path_announcement + "announcement_platform_2_BKS_1.mp3");
        once_file_path.put("announcement_platform_2_BKS_2", path_announcement + "announcement_platform_2_BKS_2.mp3");
        once_file_path.put("announcement_platform_2_CKR_1", path_announcement + "announcement_platform_2_CKR_1.mp3");
        once_file_path.put("announcement_platform_2_CKR_2", path_announcement + "announcement_platform_2_CKR_2.mp3");
        once_file_path.put("announcement_platform_3_JAKK_1", path_announcement + "announcement_platform_3_JAKK_1.mp3");
        once_file_path.put("announcement_platform_3_JAKK_2", path_announcement + "announcement_platform_3_JAKK_2.mp3");
        once_file_path.put("announcement_platform_3_MRI_1", path_announcement + "announcement_platform_3_MRI_1.mp3");
        once_file_path.put("announcement_platform_3_MRI_2", path_announcement + "announcement_platform_3_MRI_2.mp3");
        once_file_path.put("announcement_platform_4_JAKK_1", path_announcement + "announcement_platform_4_JAKK_1.mp3");
        once_file_path.put("announcement_platform_4_JAKK_2", path_announcement + "announcement_platform_4_JAKK_2.mp3");
        once_file_path.put("announcement_platform_4_MRI_1", path_announcement + "announcement_platform_4_MRI_1.mp3");
        once_file_path.put("announcement_platform_4_MRI_2", path_announcement + "announcement_platform_4_MRI_2.mp3");
        once_file_path.put("announcement_platform_1_Ls_1", path_announcement + "announcement_platform_1_Ls_1.mp3");
        once_file_path.put("announcement_platform_1_Ls_2", path_announcement + "announcement_platform_1_Ls_2.mp3");
        once_file_path.put("announcement_platform_2_Ls_1", path_announcement + "announcement_platform_2_Ls_1.mp3");
        once_file_path.put("announcement_platform_2_Ls_2", path_announcement + "announcement_platform_2_Ls_2.mp3");
        once_file_path.put("announcement_platform_3_Ls_1", path_announcement + "announcement_platform_3_Ls_1.mp3");
        once_file_path.put("announcement_platform_3_Ls_2", path_announcement + "announcement_platform_3_Ls_2.mp3");
        once_file_path.put("announcement_platform_4_Ls_1", path_announcement + "announcement_platform_4_Ls_1.mp3");
        once_file_path.put("announcement_platform_4_Ls_2", path_announcement + "announcement_platform_4_Ls_2.mp3");
        once_file_path.put("announcement_station_1", path_announcement + "announcement_station_1.mp3");
        once_file_path.put("announcement_station_2", path_announcement + "announcement_station_2.mp3");
        once_file_path.put("announcement_welcome_1", path_announcement + "announcement_welcome_1.mp3");
        once_file_path.put("announcement_welcome_2", path_announcement + "announcement_welcome_2.mp3");
        once_file_path.put("announcement_welcome_3", path_announcement + "announcement_welcome_3.mp3");
    }
    
    private void initializeIsLoop(){
        is_loop.put("failure", true);
        is_loop.put("bg-idle_1", true);
        is_loop.put("bg-idle_2", true);
        is_loop.put("bg-idle_3", true);
        is_loop.put("bell", true);
    }
    
    private void initializeMedia(){
        for (Map.Entry<String, String> entry : file_path.entrySet()) {
            media.put(entry.getKey(), new Media(new File(entry.getValue()).toURI().toString()));
            media_player.put(entry.getKey(), new MediaPlayer(media.get(entry.getKey())));
            media_player.get(entry.getKey()).setOnEndOfMedia(()->{
                if (!is_loop.containsKey(entry.getKey())){media_player.get(entry.getKey()).stop();};
                media_player.get(entry.getKey()).seek(Duration.ZERO);
            });
        }
    }
    
    public void unavailable(){
        media_player.get("unavailable").play();
    }
    
    public void headpanelClick(){
        media_player.get("headpanel_click").play();
    }
    
    public void masterOn(){
        media_player.get("master_on").play();
    }
    
    public void masterOff(){
        media_player.get("master_off").play();
    }
    
    public void panelSwitching(){
        media_player.get("panel_switching").play();
    }
    
    public void junctionClick(){
        media_player.get("junction_click").play();
    }
    
    public void alertGeneral(){
        media_player.get("alert_general").play();
    }
    
    public void alertFailure(){
        media_player.get("failure").play();
    }
    
    public void stopFailure(){
        media_player.get("failure").stop();
    }
    
    public void routePattern(){
        media_player.get("route_pattern").play();
    }
    
    public void signalSwitch(){
        media_player.get("signal_switch").play();
    }
    
    public void alertNotificationJNG(){
        media_player.get("alert_notification_jng").play();
    }
    
    public void alertNotificationBKS(){
        media_player.get("alert_notification_bks").play();
    }
    
    public void notifyStation(){
        media_player.get("notify_station").play();
    }
    
    public void bell(boolean is_on){
        if (is_on){
            media_player.get("bell").play();
        } else {
            media_player.get("bell").stop();
        }
    }
    
    public void alertArrival(String from_station){
        double audio_balance = 0;
        audio_balance = from_station.equals("JNG") ? -1.0 : 1.0;
//        media_player.get("alert_arrival").setBalance(audio_balance);
        media_player.get("alert_arrival").play();
    }
    
    public void playBackground(){
        int random_background = self.utility.getRandomNumber(1, 3);
        String filename = once_file_path.get("bg-idle_"+String.valueOf(random_background));
        String media_name = "background";
        prepareAudio(filename,media_name,true);
        media_player.get(media_name).play();
    }
    
    private int getMaxCountOfAudio(String filename){
        int count = 0;
        boolean finish = false;
        while (!finish){
            count++;
            File file = new File(filename+"_"+String.valueOf(count)+".mp3");
            if(file.exists() == false) { 
                finish = true;
                count--;
            }
        }
        return count;
    }
    
    public String getRandomAudioFilename(String train_number,String sequence){
        String train_type = self.train_type.get(train_number);
        String train_audio_type = "";
        if (train_type.equals("krl") || train_type.equals("krlf") || train_type.equals("klb krl")) train_audio_type = "emu";
        else if (train_type.equals("jj") || train_type.equals("jjf") || train_type.equals("klb jj")) train_audio_type = "train";
        else if (train_type.equals("barang") || train_type.equals("barangf") || train_type.equals("klb barang") || train_type.equals("klb lokomotif")) train_audio_type = "freight";
        else if (train_type.equals("krd") || train_type.equals("krdf") || train_type.equals("klb krd")) train_audio_type = "dmu";
        String base_filename = path_train+"cuk_"+train_audio_type+"_"+sequence;
        int get_audio_random = self.utility.getRandomNumber(1, getMaxCountOfAudio(base_filename));
        System.out.println("Get audio random number : "+get_audio_random);
        String filename = "";
        filename = once_file_path.get(train_audio_type+"_"+sequence+"_"+get_audio_random);
        System.out.println("Filename : "+(filename.isEmpty() ? ("cannot get filename : "+train_audio_type+"_"+sequence+"_"+get_audio_random) : filename));
        return filename;
    }
    
    public String getRandomAudioFilenameAnnouncement(String basename){
        String base_filename = path_announcement+basename;
        int get_audio_random = self.utility.getRandomNumber(1, getMaxCountOfAudio(base_filename));
        System.out.println("Get audio random number announcement: "+get_audio_random);
        String filename = once_file_path.get(basename+"_"+get_audio_random);
        System.out.println("Filename : "+filename);
        return filename;
    }
    
    public int getLength(String media_name){
        Double audio_length = media.get(media_name).getDuration().toSeconds();
        return audio_length.intValue();
    }
    
    public String getTrainAudio(String train_number,String sequence,boolean is_loop){
        String filename = getRandomAudioFilename(train_number,sequence);
        String media_name = train_number+"_"+sequence;
        prepareAudio(filename,media_name,is_loop);
        return media_name;
    }
    
    public void prepareAudio(String filename,String media_name,boolean is_loop){
        media.put(media_name,new Media(new File(filename).toURI().toString()));
        media_player.put(media_name,new MediaPlayer(media.get(media_name)));
        if (!is_loop){
            media_player.get(media_name).setOnEndOfMedia(new Runnable(){
                @Override
                public void run(){
                    media.remove(media_name);
                    media_player.remove(media_name);
                }
            });
        } else {
            media.put(media_name+"_loop", new Media(new File(filename).toURI().toString()));
            media_player.put(media_name+"_loop", new MediaPlayer(media.get(media_name+"_loop")));
            media_player.get(media_name).setVolume(0.5);
            media_player.get(media_name).setOnPlaying(new Runnable(){
                @Override
                public void run(){
                    new Thread(()->{
                        try {
                            System.out.println(getLength(media_name));
                            Thread.sleep((getLength(media_name)-1)*1000);
                            media_player.get(media_name+"_loop").play();
                        } catch (InterruptedException ex) {
                            Logger.getLogger(Audio.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }).start();
                }
            });
            media_player.get(media_name).setOnEndOfMedia(new Runnable(){
                @Override
                public void run(){
                    media_player.get(media_name).pause();
                    media_player.get(media_name).seek(Duration.ZERO);
                }
            });
            media_player.get(media_name).setOnStopped(new Runnable(){
                @Override
                public void run(){
                    media.remove(media_name);
                    media_player.remove(media_name);
                    media.remove(media_name+"_loop");
                    media_player.remove(media_name+"_loop");
                    
                }
            });
            media_player.get(media_name+"_loop").setOnPlaying(new Runnable(){
                @Override
                public void run(){
                    new Thread(()->{
                        try {
                            Thread.sleep((getLength(media_name+"_loop")-1)*1000);
                            media_player.get(media_name).play();
                        } catch (InterruptedException ex) {
                            Logger.getLogger(Audio.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }).start();
                }
            });
            media_player.get(media_name+"_loop").setOnEndOfMedia(new Runnable(){
                @Override
                public void run(){
                    media_player.get(media_name+"_loop").pause();
                    media_player.get(media_name+"_loop").seek(Duration.ZERO);
                }
            });
        }
        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Audio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void trainToStation(String train_number,Object segment,int travel_time,int restricted_speed){
        new Thread(()->{
            try {
                System.out.println("Playing audio train entering station");
                String etd = self.train_etd.get(train_number);
                String signal_state = self.signal_state.get(self.signal_of_block.get(segment));
                String sequence = "arr";
                if (etd.equals("Ls")){
                    if (signal_state.equals("red") == false && restricted_speed == 0){
                        sequence = "pass_highspeed";
                    } else {
                        sequence = "pass_lowspeed";
                    }
                }
                System.out.println("train number : "+train_number+" sequence : "+sequence);
                String media_name = getTrainAudio(train_number,sequence,false);
                int length = getLength(media_name);
                int wait_time = (travel_time-length-3)+7;
                System.out.println("audio length : "+length+" and wait time : "+wait_time);
                Thread.sleep(wait_time*1000);
                media_player.get(media_name).play();
                Thread.sleep(5000);
                checkIdleSound(train_number,segment);
            } catch (InterruptedException ex) {
                Logger.getLogger(Audio.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }
    
    public void checkIdleSound(String train_number,Object segment){
        String etd = self.train_etd.get(train_number);
        String signal_state = self.signal_state.get(self.signal_of_block.get(segment));
        if (etd.equals("Ls") == false || signal_state.equals("red") == false){
            System.out.println("Getting idle sound for train : "+train_number);
            String media_name = getTrainAudio(train_number,"idle",true);
            media_player.get(media_name).play();
            double volume = 0.0;
            while (volume <= 1){
                media_player.get(media_name).setVolume(volume);
                media_player.get(media_name+"_loop").setVolume(volume);
                volume = volume + 0.1;
                try {
                    Thread.sleep(200);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Audio.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            boolean departed = false;
            while (!departed){
                boolean is_platform_departed = self.platform_departure_approved.get(segment);
                signal_state = self.signal_state.get(self.signal_of_block.get(segment));
                if (is_platform_departed && signal_state.equals("red") == false){
                    volume = 1.0;
                    while (volume >= 0){
                        media_player.get(media_name).setVolume(volume);
                        media_player.get(media_name+"_loop").setVolume(volume);
                        volume = volume - 0.1;
                        try {
                            Thread.sleep(200);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(Audio.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    media_player.get(media_name).stop();
                    departed = true;
                }
                
            }
        }
    }
    
    public void openDoor(String train_number){
        String media_name = getTrainAudio(train_number,"open_door",false);
        media_player.get(media_name).play();
    }
    
    public void closeDoor(String train_number){
        String media_name = getTrainAudio(train_number,"close_door",false);
        media_player.get(media_name).play();
    }
    
    public void checkTrainDeparting(String train_number,Object segment){
        if (self.platform_number.containsKey(segment)){
            System.out.println("Playing departure train sound");
            String media_name = getTrainAudio(train_number,"dep",false);
            media_player.get(media_name).play();
        }
    }
    
    public void closeAll(){
        for (Map.Entry<String,MediaPlayer> entry : media_player.entrySet()) {
            entry.getValue().stop();
        }
    }
}
