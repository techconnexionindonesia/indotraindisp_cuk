/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indotraindisp_cuk;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author 62896
 */
public class Scoring {
    ControlPanelCUK self = null;
    
    public Scoring(ControlPanelCUK parent){
        self = parent;
    }
    
    public void loadScore(){
        try {
            ResultSet rs = self.utility.dbGet("main");
            rs.next();
            int score = rs.getInt("score");
            self.label_score.setText(String.valueOf(score));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Aplikasi Error , Silahkan reinstall.", "Error", 0);
        }
    }
    
    public void addScore(int add_score, String log){
        new Thread(()->{
            try {
                ResultSet rs = self.utility.dbGet("main");
                rs.next();
                int score = rs.getInt("score") + add_score;
                String log_add = self.lang.text("point_add",String.valueOf(add_score),log);
                if (add_score < 0){
                    log_add = self.lang.text("point_minus",String.valueOf(add_score),log);
                }
                Map<String,String> criteria = new HashMap<String,String>();
                criteria.put("score",String.valueOf(score));
                System.out.println(log_add);
                self.utility.dbUpdate(criteria, "main");
                self.label_score.setText(String.valueOf(score));
                self.information.log(log_add);
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Aplikasi Error , Silahkan reinstall.", "Error", 0);
            }
        }).start();
    }
    
    /* --- Train scoring methods --- */
    public void checkEnteringPlatform(String train_number, Object segment){
        String platform_number = self.platform_number.get(segment);
        String estimated_platform = self.train_platform.get(train_number);
        String tolerated_platform = estimated_platform;
        switch (estimated_platform){
            case "1":
                tolerated_platform = "2";
                break;
                
            case "2":
                tolerated_platform = "1";
                break;
                
            case "3":
                tolerated_platform = "4";
                break;
                
            case "4":
                tolerated_platform = "3";
                break;
        }
        String log = self.lang.text("train_enter_correct_platform",train_number);
        int score = 1;
        if (platform_number.equals(estimated_platform) == false && platform_number.equals(tolerated_platform) == false){
            log = self.lang.text("train_enter_wrong_platform",train_number);
            score = -5;
        }
        System.out.println("Check train "+train_number+" entering platform : platform number : "+platform_number+" , estimated platform : "+estimated_platform+" tolerated platform : "+tolerated_platform);
        addScore(score,log);
    }
    
    public void checkNotificationDeparture(String train_number){
        String log = self.lang.text("train_departure_notified_waystation",train_number);
        int score = 1;
        if (!self.train_notification_next_station_sent.get(train_number)){
            log = self.lang.text("train_departure_unnotified_waystation",train_number);
            score = -5;
        }
        addScore(score,log);
    }
    
    public void checkArrivalTime(String train_number){
        int delay = Integer.valueOf(self.train_delay.get(train_number));
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy/MM/dd HH:mm:ss");
        DateTime time_arrival = formatter.parseDateTime(self.date_play+" "+self.train_eta.get(train_number)+":00");
        time_arrival = time_arrival.plusMinutes(delay);
        String log = self.lang.text("train_arrive_on_time",train_number);
        int score = 1;
        if (!(self.time_play.isBefore(time_arrival.plusSeconds(+15)) && time_arrival.isAfter(time_arrival.plusSeconds(-15)))){
            log = self.lang.text("train_arrive_delayed",train_number);
            score = -5;
        }
        addScore(score,log);
    }
    
    public void checkDepartureTime(String train_number){
        String etd = self.train_etd.get(train_number);
        if (!etd.equalsIgnoreCase("Ls")){
            int delay = Integer.valueOf(self.train_delay.get(train_number));
            DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy/MM/dd HH:mm:ss");
            DateTime time_etd = formatter.parseDateTime(self.date_play+" "+etd+":00");
            time_etd = time_etd.plusMinutes(delay);
            String log = self.lang.text("train_depart_on_time",train_number);
            int score = 1;
            if (delay == 0){
                if (!(self.time_play.isBefore(time_etd.plusSeconds(+15)) && self.time_play.isAfter(time_etd.plusSeconds(-15)))){
                    log = self.lang.text("train_depart_delayed",train_number);
                    score = -5;
                }
            }else if (delay != 0 && (delay != -1 && delay != -2)){
                if (!(self.time_play.isBefore(time_etd.plusSeconds(+15)) && self.time_play.isAfter(time_etd.plusSeconds(-45)))){
                    log = self.lang.text("train_depart_delayed",train_number);
                    score = -5;
                }
            }else if (delay != 0 && (delay == -1 || delay == -2)){
                time_etd = formatter.parseDateTime(self.date_play+" "+etd+":00");
                if (!(self.time_play.isBefore(time_etd.plusSeconds(+15)) && self.time_play.isAfter(time_etd.plusSeconds(-45)))){
                    log = self.lang.text("train_depart_delayed",train_number);
                    score = -5;
                }
            }
            addScore(score,log);
        }
    }
    
    public void giveBonusPlayTime(){
        String log = self.lang.text("got_time_play_bonus");
        int score = 10;
        addScore(score,log);
    }
}
