package indotraindisp_cuk.language;

import static indotraindisp_cuk.language.En.text;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author TCIPRO
 */


public class Id {
    public static Map<String, String> text = new HashMap<String, String>();
    
    public Id(){
        text.put("panel_locked","Panel telah dikunci");
        text.put("panel_unlocked","Panel telah dibuka");
        text.put("has_changed","%1 telah dirubah");
        text.put("junction","Wesel");
        text.put("junction_is_locked", "Wesel dikunci");
        text.put("junction_is_locked_route_created", "Wesel terkunci. Rute telah dibuat");
        text.put("has_been_locked", "%1 telah dikunci");
        text.put("has_been_unlocked", "%1 telah dibuka");
        text.put("panel_is_locked", "Panel dikunci");
        text.put("all_junction_locked", "Semua wesel telah dikunci");
        text.put("all_junction_unlocked", "Semua wesel telah dibuka");
        text.put("junction_has_obstacle","%1 tidak dapat berubah. Wesel terdapat sekat");
        text.put("junction_need_reposition","Posisi %1 tidak terdeteksi. Wesel butuh reposisi");
        text.put("has_been_fixed", "%1 telah diperbaiki. Sistem dalam operasional penuh");
        text.put("junction_is_on_failure", "Wesel sedang mengalami gangguan");
        text.put("signal_is_on_failure", "Sinyal sedang mengalami gangguan");
        text.put("route_cannot_created_junction_turned", "Rute tidak dapat dibentuk. Wesel terbelokan");
        text.put("route_cannot_created_segment_on_failure", "Rute tidak dapat dibentuk. Segment dalam gangguan");
        text.put("route_cannot_created_opposite_route", "Rute tidak dapat dibentuk. Rute terbentuk dari arah berlawanan");
        text.put("route_created","Rute telah dibuat dari %1 menuju %2.");
        text.put("route_removed","Rute dihapus");
        text.put("block", "Blok");
        text.put("formation_not_available","SF tidak tersedia");
        text.put("time_cannot_empty","Kolom waktu tidak boleh kosong");
        text.put("timer_cannot_empty","Jika timer diaktifkan, kolom waktu timer tidak boleh kosong");
        text.put("loading","Loading . . .");
        text.put("happy_working","Selamat Bekerja !");
        text.put("time_up","Waktu Habis");
        text.put("time_up_text","Waktu habis, apakah anda ingin melanjutkan simulasi ?");
        text.put("delay_-2","Awal 2 menit");
        text.put("delay_-1","Awal 1 menit");
        text.put("delay_0","Tepat waktu");
        text.put("delay_1","Telat 1 menit");
        text.put("delay_2","Telat 2 menit");
        text.put("delay_3","Telat 3 menit");
        text.put("type_krl","KRL");
        text.put("type_jj","KA Jarak Jauh");
        text.put("type_jjf","KA Jarak Jauh Fakultatif");
        text.put("type_barang","KA Barang");
        text.put("type_barangf","KA Barang Fakultatif");
        text.put("type_klb jj","KLB KA Jarak Jauh");
        text.put("type_klb barang","KLB Barang");
        text.put("type_klb krl","KLB Krl");
        text.put("type_klb krd","KLB Krd");
        text.put("type_klb lokomotif","Lokomotif");
        text.put("running", "Berjalan");
        text.put("stopped", "Berhenti");
        text.put("train_enter", "KA %1 masuk dari arah %2");
        text.put("point_add", "Point bertambah %1 : %2");
        text.put("point_minus", "Point berkurang %1 : %2");
        text.put("train_enter_correct_platform","KA %1 memasuki peron yang tepat");
        text.put("train_enter_wrong_platform","KA %1 memasuki peron yang salah");
        text.put("train_departure_notified_waystation","Keberangkatan KA %1 dikabarkan ke PPKA berikutnya");
        text.put("train_departure_unnotified_waystation","Keberangkatan KA %1 tidak dikabarkan ke PPKA berikutnya");
        text.put("train_departure_notified_oc","Keberangkatan KA %1 dikabarkan ke PKOC");
        text.put("train_departure_unnotified_oc","Keberangkatan KA %1 tidak dikabarkan ke PKOC");
        text.put("train_arrive_on_time","KA %1 datang tepat waktu");
        text.put("train_arrive_delayed","KA %1 datang terlambat");
        text.put("train_depart_on_time","KA %1 berangkat tepat waktu");
        text.put("train_depart_delayed","KA %1 berangkat terlambat");
        text.put("train_stop_at_platform","KA %1 berhenti di peron %2");
        text.put("train_depart_from_platform","KA %1 berangkat di peron %2");
        text.put("got_time_play_bonus","Mendapatkan bonus dari waktu permainan setiap 30 menit");
    }
}
