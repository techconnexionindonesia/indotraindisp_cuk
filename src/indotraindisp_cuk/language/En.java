package indotraindisp_cuk.language;

import static indotraindisp_cuk.language.Id.text;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author TCIPRO
 */


public class En {
    public static Map<String, String> text = new HashMap<String, String>();
    
    public En(){
        text.put("panel_locked","Panel has been locked");
        text.put("panel_unlocked","Panel has been unlocked");
        text.put("has_changed","%1 has been changed");
        text.put("junction","Junction");
        text.put("junction_is_locked", "Junction is locked");
        text.put("junction_is_locked_route_created", "Junction is locked. Route has been created");
        text.put("has_been_locked", "%1 has been locked");
        text.put("has_been_locked", "%1 has been locked");
        text.put("has_been_unlocked", "%1 has been unlocked");
        text.put("panel_is_locked", "Panel is locked");
        text.put("all_junction_locked", "All junction has been locked");
        text.put("all_junction_unlocked", "All junction has been unlocked");
        text.put("junction_has_obstacle","%1 cannot be turned. The junction has obstacle");
        text.put("junction_need_reposition","%1 position not detected. Need repositioning");
        text.put("has_been_fixed", "%1 has been fixed. System on full operational");
        text.put("junction_is_on_failure", "Junction is on failure");
        text.put("signal_is_on_failure", "Signal is on failure");
        text.put("route_cannot_created_junction_turned", "Route cannot created. Junction on turned");
        text.put("route_cannot_created_segment_on_failure", "Route cannot created. Segment on failure");
        text.put("route_cannot_created_opposite_route", "Route cannot created. Route created on opposite direction");
        text.put("route_created","Route created from %1 to %2.");
        text.put("route_removed","Route removed");
        text.put("block", "Block");
        text.put("formation_not_available","Formation not available");
        text.put("time_cannot_empty","Time field cannot be empty");
        text.put("timer_cannot_empty","If timer activated, timer field cannot be empty");
        text.put("loading","Loading . . .");
        text.put("happy_working","Happy Working !");
        text.put("time_up","Time's Up");
        text.put("time_up_text","Time's up, would you like to continue simulation ?");
        text.put("delay_-2","Early 2 minute");
        text.put("delay_-1","Early 1 minute");
        text.put("delay_0","On time");
        text.put("delay_1","Late 1 minute");
        text.put("delay_2","Late 2 minute");
        text.put("delay_3","Late 3 minute");
        text.put("type_krl","Commuter Train");
        text.put("type_jj","Intercity Train");
        text.put("type_jjf","Facultative Intercity Train");
        text.put("type_barang","Freight Train");
        text.put("type_barangf","Facultative Freight Train");
        text.put("type_klb jj","Special Intercity Train");
        text.put("type_klb barang","Special Freight Train");
        text.put("type_klb krl","Special Commuter Train");
        text.put("type_klb krd","Special Diesel Train");
        text.put("type_klb lokomotif","Locomotive");
        text.put("running", "Running");
        text.put("stopped", "Stopped");
        text.put("train_enter", "Train %1 enter from %2 direction");
        text.put("point_add", "Point plus %1 : %2");
        text.put("point_minus", "Point minus %1 : %2");
        text.put("train_enter_correct_platform","Train %1 enter correct platform");
        text.put("train_enter_wrong_platform","Train %1 enter wrong platform");
        text.put("train_departure_notified_waystation","Train %1 departure notified to next dispatcher");
        text.put("train_departure_unnotified_waystation","Train %1 departure unnotified to next dispatcher");
        text.put("train_departure_notified_oc","Train %1 departure notified to OC");
        text.put("train_departure_unnotified_oc","Train %1 departure unnotified to OC");
        text.put("train_arrive_on_time","Train %1 arrive on time");
        text.put("train_arrive_delayed","Train %1 arrive delayed");
        text.put("train_depart_on_time","Train %1 depart on time");
        text.put("train_depart_delayed","Train %1 depart delayed");
        text.put("train_stop_at_platform","Train %1 stop at platform %2");
        text.put("train_depart_from_platform","Train %1 depart from platform %2");
        text.put("got_time_play_bonus","Got bonus by play time each 30 minutes");
    }
}
