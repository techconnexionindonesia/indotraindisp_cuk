/*
    Indonesia Train Dispatcher
    Layout & Algoritma Citayam
    Dibuat dan dikembangkan oleh
    Tech Connexion Indonesia Game Developer
 */

//Control Panel PPKA Cakung v1.2

package indotraindisp_cuk;
import indotraindisp.control.Koneksi;
import javax.swing.JOptionPane;
import java.net.*;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JLabel;
import indotraindisp.model.fungsi;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.JTextField;
import org.joda.time.DateTime;

/**
 *
 * @author Tech Connexion Indonesia
 */
public class ControlPanelCUK extends javax.swing.JFrame {
    // Variables    
    boolean is_simulation_running = false;
    boolean is_master_on = false;
    boolean is_btn_unlock_1_pressed = false;
    boolean is_btn_unlock_2_pressed = false;   
    boolean is_btn_lock_junction_pressed = false;
    boolean is_btn_unlock_junction_pressed = false;
    boolean is_btn_lock_all_junction_pressed = false;
    boolean is_btn_unlock_all_junction_pressed = false;
    boolean is_btn_remove_obstacle_pressed = false;
    boolean is_btn_reposition_junction_pressed = false;
    
    /*--- Failure Variable ---*/
    int failure_range_hour = 1;
    
    /*--- Time Variable ---*/
    public String date_play = null;
    public DateTime time_play = null;
    public DateTime time_end = null;
    
    /*--- Segment Variables (segment mean multi type of components such as signal track or junction ---*/
    Map type_of_element = new HashMap();
    Map<Object,String> number_of_segment = new HashMap<Object,String>();
    Map<Object,String> segment_type = new HashMap<Object,String>();
    Map<Object,Boolean> is_segment_clickable = new HashMap<Object,Boolean>();
    Map<Object,Boolean> is_segment_on_failure = new HashMap<Object,Boolean>();
    Map<Object,Object> segment_next_left = new HashMap<Object,Object>();
    Map<Object,Object> segment_next_right = new HashMap<Object,Object>();
    Map<Object,Object> segment_next_turn = new HashMap<Object,Object>();
    Map<Object,Boolean> is_segment_reserved = new HashMap<Object,Boolean>();
    Map<Object,Boolean> is_train_on_segment = new HashMap<Object,Boolean>();
    Map<Integer,Object> arrival_block_segment = new HashMap<Integer,Object>();
    Map<Integer,String> arrival_block_direction = new HashMap<Integer,String>();
    Map<Integer,String> arrival_block_station = new HashMap<Integer,String>();
    Map<Integer,String> arrival_block_name = new HashMap<Integer,String>();
    Map<Object,String> exit_block_segment = new HashMap<Object,String>();
    Map<Object,Map<Object,Integer>> segment_distance = new HashMap<Object,Map<Object,Integer>>();
    Map<Object,Integer> entry_exit_distance = new HashMap<Object,Integer>();
    
    /*--- Junction Variables ---*/
    Map<JLabel,String> junction_type = new HashMap<JLabel,String>();
    Map<JLabel,String> junction_vertical_heading = new HashMap<JLabel,String>();
    Map<JLabel,String> junction_horizontal_heading = new HashMap<JLabel,String>();
    Map<JLabel,Boolean> is_junction_turned = new HashMap<JLabel,Boolean>();
    Map<JLabel,Boolean> is_junction_locked = new HashMap<JLabel,Boolean>();
    Map<JLabel,Boolean> is_junction_has_obstacle = new HashMap<JLabel,Boolean>();
    Map<JLabel,Boolean> is_junction_need_reposition = new HashMap<JLabel,Boolean>();
    Map<JLabel,Boolean> is_junction_need_fixing = new HashMap<JLabel,Boolean>();
    Map<JLabel,JLabel> label_of_junction = new HashMap<JLabel,JLabel>(); 
    
    /*--- Signal Variables ---*/
    Map<JLabel,String> signal_type = new HashMap<JLabel,String>();
    Map<JLabel,String> signal_state = new HashMap<JLabel,String>();
    Map<JLabel,String> signal_direction = new HashMap<JLabel,String>();
    Map<JLabel,JTextField> block_of_signal = new HashMap<JLabel,JTextField>();
    Map<JTextField,JLabel> signal_of_block = new HashMap<JTextField,JLabel>();
    Map<JLabel,JLabel> relative_signal = new HashMap<JLabel,JLabel>();
    Map<JLabel,JLabel> repeater_of_signal = new HashMap<JLabel,JLabel>();
    Map<JLabel,JLabel> speed_signal = new HashMap<JLabel,JLabel>();
    Map<JLabel,Boolean> is_speed_signal_on = new HashMap<JLabel,Boolean>();
    Map<JLabel,JLabel> entry_signal_adjacent = new HashMap<JLabel,JLabel>();
    JLabel[] controlable_signal;
    
    /*--- Track Variables ---*/
    Map<JLabel,String> track_type = new HashMap<JLabel,String>();
    Map<JLabel,String> track_direction = new HashMap<JLabel,String>();
    
    /*--- Block Variables ---*/
    Map<Object,Boolean> is_turn_route_created = new HashMap<Object,Boolean>();
    Map<Object,Object[]> turn_condition = new HashMap<Object,Object[]>();
    Map<Object,Boolean> is_entry_signal_block = new HashMap<Object,Boolean>();
    Map<Object,Boolean> is_adjacent_small_station = new HashMap<Object,Boolean>();
    Map<Object,String> platform_number = new HashMap<Object,String>();
    Map<Object,Boolean> platform_departure_approved = new HashMap<Object,Boolean>();
    Map<Object,String> block_notification_trigger = new HashMap<Object,String>();
    
    /*--- Data Variables ---*/
    Map<String,String> station = new HashMap<String,String>();
    boolean is_bell_on = false;
    
    /*--- Driver Variables ---*/
    String[] driver = new String[200];
    String[] driver_assistant = new String[200];
    int driver_count = 0;
    int driver_assistant_count = 0;
    
    /*--- Train Variables ---*/
    Map<String,ArrayList<String>> unit = new HashMap<String,ArrayList<String>>();
    Map<String,Integer> unit_sf = new HashMap<String,Integer>();
    Map<String,Integer> unit_count = new HashMap<String,Integer>();
    Map<String,String> unit_type = new HashMap<String,String>();
    Map<String,String> train_name = new HashMap<String,String>();
    Map<String,String> train_from = new HashMap<String,String>();
    Map<String,String> train_destination = new HashMap<String,String>();
    Map<String,String> train_eta = new HashMap<String,String>();
    Map<String,String> train_etd = new HashMap<String,String>();
    Map<String,String> train_platform = new HashMap<String,String>();
    Map<String,String> train_selected_platform = new HashMap<String,String>();
    Map<String,String> train_type = new HashMap<String,String>();
    Map<String,Integer> train_length = new HashMap<String,Integer>();
    Map<String,ArrayList<String>> train_unit = new HashMap<String,ArrayList<String>>();
    Map<String,String> train_info = new HashMap<String,String>();
    Map<String,Integer> train_arrival_block = new HashMap<String,Integer>();
    Map<String,String> train_driver = new HashMap<String,String>();
    Map<String,String> train_driver_assistant = new HashMap<String,String>();
    Map<String,Integer> train_delay = new HashMap<String,Integer>();
    Map<String,String> train_exiting_segment = new HashMap<String,String>();
    ArrayList<String> active_trains = new ArrayList<String>();
    ArrayList<String> running_train = new ArrayList<String>();
    ArrayList<String> entry_waiting = new ArrayList<String>();
    ArrayList<String> exiting_list = new ArrayList<String>();
    Map<String,Boolean> is_train_entered_zone = new HashMap<String,Boolean>();
    Map<String,String> train_position = new HashMap<String,String>();
    Map<String,String> train_movement = new HashMap<String,String>();
    Map<String,Boolean> train_notification_previous_station_sent = new HashMap<String,Boolean>();
    Map<String,Boolean> train_notification_next_station_sent = new HashMap<String,Boolean>();
    boolean activate_facultative = true;
    String announcer_voice = "";
    int failure_percentage = 0;
    int additional_percentage = 0;
    
    /* --- Voicing Variable --- */
    ArrayList<String> announcement_on_going = new ArrayList<String>();
    
    // Instances
    fungsi utility = new fungsi();
    Initiator initiator = new Initiator(this);
    Switcher switcher = new Switcher(this);
    HeadPanel headPanel = new HeadPanel(this);
    MainControl mainControl = new MainControl(this);
    Failure failure = new Failure(this);
    Information information = new Information(this);
    Audio audio = null;
    TextParser textParser = new TextParser(this);
    Lang lang = new Lang(this);
    DataParser dataParser = new DataParser(this);
    Time time = new Time(this);
    Train train = new Train(this);
    Scoring scoring = new Scoring(this);
    Announcement announcement = new Announcement(this);
    SetSimulation setSimulation = new SetSimulation(this);
    
    // URL asign for menu
    // Temporary
//    String URL_ITD = "https://www.techconnexionindonesia.com/store/view/?id=TCI1INDOTRAINDISP";
//    String URL_CTA = "https://www.techconnexionindonesia.com/content/view/?id=TCI2INDOTRAINDISPCTA";
    String URL_ITD = "https://discord.gg/EfnqZvXhFC";
    String URL_CTA = "https://discord.gg/EfnqZvXhFC";
    /**
     * Creates new form controlPanel
     */
    public ControlPanelCUK(fungsi model) {
        utility = model;
        lang = new Lang(this);
        initComponents();
        utility.setScrollSpeed(scroll_pane,16);
        utility.setFullscreen(this);
        initiator.initiate_all();
        setSimulation.setVisible(true);
    }
    
    public void loadAll(){
        audio = new Audio(this);
        headPanel.toggleEnablePanel(false);
        dataParser.loadAll();
        scoring.loadScore();
        information.updatePosition();
        time.addMinutes();
        time.startTime();
        announcement.stationaryAnnouncement();
    }
    
    public void afterParseData(){
        audio.playBackground();
        information.notification(lang.text("happy_working"));
    }
    
    public void closeSetSimulation(){
        setSimulation.setVisible(false);
    }
    
    public void closeSimulation(){
        is_simulation_running = false;
        audio.media_player.get("background").stop();
        this.dispose();
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel7 = new javax.swing.JPanel();
        panelAtas = new javax.swing.JPanel();
        labelTime = new javax.swing.JLabel();
        label_score = new javax.swing.JLabel();
        labelPoint = new javax.swing.JLabel();
        label_time = new javax.swing.JLabel();
        txt_notification = new javax.swing.JLabel();
        labelPlayTime = new javax.swing.JLabel();
        label_elapsed = new javax.swing.JLabel();
        scroll_pane = new javax.swing.JScrollPane();
        panelUtama = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        c17 = new javax.swing.JLabel();
        signal_j32 = new javax.swing.JLabel();
        junction_13a = new javax.swing.JLabel();
        label_cuk4 = new javax.swing.JLabel();
        signal_uj10 = new javax.swing.JLabel();
        btn_bell = new javax.swing.JLabel();
        labelBellOnOff = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        label_signal_b209 = new javax.swing.JLabel();
        block_201 = new javax.swing.JTextField();
        block_101 = new javax.swing.JTextField();
        label_101 = new javax.swing.JLabel();
        signal_b208 = new javax.swing.JLabel();
        a1 = new javax.swing.JLabel();
        b5 = new javax.swing.JLabel();
        a17 = new javax.swing.JLabel();
        block_102 = new javax.swing.JTextField();
        label_block_102 = new javax.swing.JLabel();
        a13 = new javax.swing.JLabel();
        c10 = new javax.swing.JLabel();
        a3 = new javax.swing.JLabel();
        a5 = new javax.swing.JLabel();
        c4 = new javax.swing.JLabel();
        block_302 = new javax.swing.JTextField();
        label_block_302 = new javax.swing.JLabel();
        signal_j60 = new javax.swing.JLabel();
        c3 = new javax.swing.JLabel();
        c2 = new javax.swing.JLabel();
        block_301 = new javax.swing.JTextField();
        label_block_301 = new javax.swing.JLabel();
        c1 = new javax.swing.JLabel();
        label_jng_2 = new javax.swing.JLabel();
        d1 = new javax.swing.JLabel();
        signal_b406 = new javax.swing.JLabel();
        block_401 = new javax.swing.JTextField();
        label_block_401 = new javax.swing.JLabel();
        d2 = new javax.swing.JLabel();
        d3 = new javax.swing.JLabel();
        d4 = new javax.swing.JLabel();
        d5 = new javax.swing.JLabel();
        block_202 = new javax.swing.JTextField();
        label_kri = new javax.swing.JLabel();
        signal_j42 = new javax.swing.JLabel();
        signal_b301 = new javax.swing.JLabel();
        speed_signal_j24 = new javax.swing.JLabel();
        junction_21a = new javax.swing.JLabel();
        x_21a_11a = new javax.swing.JLabel();
        junction_24 = new javax.swing.JLabel();
        b6 = new javax.swing.JLabel();
        junction_24a2 = new javax.swing.JLabel();
        bb8 = new javax.swing.JLabel();
        junction_21b = new javax.swing.JLabel();
        ab8 = new javax.swing.JLabel();
        a4 = new javax.swing.JLabel();
        bb2 = new javax.swing.JLabel();
        b4 = new javax.swing.JLabel();
        block_030 = new javax.swing.JTextField();
        label_block_030 = new javax.swing.JLabel();
        ab4 = new javax.swing.JLabel();
        block_040 = new javax.swing.JTextField();
        label_block_040 = new javax.swing.JLabel();
        label_jng = new javax.swing.JLabel();
        b19 = new javax.swing.JLabel();
        a8 = new javax.swing.JLabel();
        b7 = new javax.swing.JLabel();
        label_bks = new javax.swing.JLabel();
        block_020 = new javax.swing.JTextField();
        label_block_020 = new javax.swing.JLabel();
        ab2 = new javax.swing.JLabel();
        label_kldb2 = new javax.swing.JLabel();
        block_010 = new javax.swing.JTextField();
        label_block_010 = new javax.swing.JLabel();
        label_cuk1 = new javax.swing.JLabel();
        bb5 = new javax.swing.JLabel();
        ab5 = new javax.swing.JLabel();
        ab3 = new javax.swing.JLabel();
        bb6 = new javax.swing.JLabel();
        b8 = new javax.swing.JLabel();
        b9 = new javax.swing.JLabel();
        a9 = new javax.swing.JLabel();
        a7 = new javax.swing.JLabel();
        ab7 = new javax.swing.JLabel();
        a6 = new javax.swing.JLabel();
        bb4 = new javax.swing.JLabel();
        a11 = new javax.swing.JLabel();
        b10 = new javax.swing.JLabel();
        c18 = new javax.swing.JLabel();
        junction_11a = new javax.swing.JLabel();
        a10 = new javax.swing.JLabel();
        bb3 = new javax.swing.JLabel();
        b15 = new javax.swing.JLabel();
        ab6 = new javax.swing.JLabel();
        bb1 = new javax.swing.JLabel();
        signal_b209 = new javax.swing.JLabel();
        signal_j82 = new javax.swing.JLabel();
        signal_j10 = new javax.swing.JLabel();
        signal_j62 = new javax.swing.JLabel();
        b2 = new javax.swing.JLabel();
        c5 = new javax.swing.JLabel();
        c6 = new javax.swing.JLabel();
        c7 = new javax.swing.JLabel();
        c8 = new javax.swing.JLabel();
        label_cuk5 = new javax.swing.JLabel();
        label_block_050 = new javax.swing.JLabel();
        block_050 = new javax.swing.JTextField();
        block_060 = new javax.swing.JTextField();
        label_block_060 = new javax.swing.JLabel();
        label_cuk6 = new javax.swing.JLabel();
        d6 = new javax.swing.JLabel();
        d7 = new javax.swing.JLabel();
        d8 = new javax.swing.JLabel();
        d9 = new javax.swing.JLabel();
        d11 = new javax.swing.JLabel();
        junction_13b = new javax.swing.JLabel();
        x_13b_23b = new javax.swing.JLabel();
        junction_23b = new javax.swing.JLabel();
        b12 = new javax.swing.JLabel();
        a16 = new javax.swing.JLabel();
        b13 = new javax.swing.JLabel();
        c9 = new javax.swing.JLabel();
        d10 = new javax.swing.JLabel();
        bb7 = new javax.swing.JLabel();
        c11 = new javax.swing.JLabel();
        c12 = new javax.swing.JLabel();
        c13 = new javax.swing.JLabel();
        c14 = new javax.swing.JLabel();
        c15 = new javax.swing.JLabel();
        c16 = new javax.swing.JLabel();
        ab1 = new javax.swing.JLabel();
        junction_23a = new javax.swing.JLabel();
        a14 = new javax.swing.JLabel();
        junction_11b = new javax.swing.JLabel();
        x_24a2_14 = new javax.swing.JLabel();
        junction_14 = new javax.swing.JLabel();
        a12 = new javax.swing.JLabel();
        a15 = new javax.swing.JLabel();
        b11 = new javax.swing.JLabel();
        b14 = new javax.swing.JLabel();
        junction_24a1 = new javax.swing.JLabel();
        d22 = new javax.swing.JLabel();
        d12 = new javax.swing.JLabel();
        d21 = new javax.swing.JLabel();
        d20 = new javax.swing.JLabel();
        d19 = new javax.swing.JLabel();
        d18 = new javax.swing.JLabel();
        d17 = new javax.swing.JLabel();
        d16 = new javax.swing.JLabel();
        d15 = new javax.swing.JLabel();
        d14 = new javax.swing.JLabel();
        d13 = new javax.swing.JLabel();
        a2 = new javax.swing.JLabel();
        b16 = new javax.swing.JLabel();
        block_202b = new javax.swing.JTextField();
        label_202b = new javax.swing.JLabel();
        signal_j24 = new javax.swing.JLabel();
        b3 = new javax.swing.JLabel();
        b18 = new javax.swing.JLabel();
        a19 = new javax.swing.JLabel();
        b17 = new javax.swing.JLabel();
        block_203 = new javax.swing.JTextField();
        label_block_203 = new javax.swing.JLabel();
        signal_b201 = new javax.swing.JLabel();
        blok_129 = new javax.swing.JTextField();
        label_129 = new javax.swing.JLabel();
        sl_b102_w14 = new javax.swing.JLabel();
        a18 = new javax.swing.JLabel();
        block_103 = new javax.swing.JTextField();
        label_block_103 = new javax.swing.JLabel();
        signal_b105 = new javax.swing.JLabel();
        b1 = new javax.swing.JLabel();
        a20 = new javax.swing.JLabel();
        label_cuk2 = new javax.swing.JLabel();
        label_junction_24 = new javax.swing.JLabel();
        label_signal_b406 = new javax.swing.JLabel();
        label_block_202 = new javax.swing.JLabel();
        label_signal_j60 = new javax.swing.JLabel();
        label_cuk3 = new javax.swing.JLabel();
        label_kldb1 = new javax.swing.JLabel();
        label_signal_b208 = new javax.swing.JLabel();
        label_signal_uj10 = new javax.swing.JLabel();
        label_signal_b301 = new javax.swing.JLabel();
        label_signal_j32 = new javax.swing.JLabel();
        label_signal_b101 = new javax.swing.JLabel();
        label_201 = new javax.swing.JLabel();
        label_junction_21a = new javax.swing.JLabel();
        label_junction_11a = new javax.swing.JLabel();
        label_junction_11b = new javax.swing.JLabel();
        label_signal_j10 = new javax.swing.JLabel();
        label_signal_j42 = new javax.swing.JLabel();
        label_signal_j22 = new javax.swing.JLabel();
        label_signal_b105 = new javax.swing.JLabel();
        label_junction_21b = new javax.swing.JLabel();
        label_junction_23a = new javax.swing.JLabel();
        label_junction_13a = new javax.swing.JLabel();
        label_junction_13b = new javax.swing.JLabel();
        label_junction_23b = new javax.swing.JLabel();
        label_junction_24a1 = new javax.swing.JLabel();
        label_junction_24a2 = new javax.swing.JLabel();
        label_junction_14 = new javax.swing.JLabel();
        label_signal_j82 = new javax.swing.JLabel();
        label_signal_j24 = new javax.swing.JLabel();
        label_signal_b201 = new javax.swing.JLabel();
        signal_j22 = new javax.swing.JLabel();
        signal_j12 = new javax.swing.JLabel();
        label_signal_j12 = new javax.swing.JLabel();
        label_signal_j62 = new javax.swing.JLabel();
        label_cuk = new javax.swing.JLabel();
        panelBawah = new javax.swing.JTabbedPane();
        pbJadwalStatus = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        table_timetable = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        table_train_status = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        pbInfo = new javax.swing.JPanel();
        info_input_number = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();
        jPanel3 = new javax.swing.JPanel();
        panel_information = new javax.swing.JTabbedPane();
        panel_info_general_detail = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        info_number = new javax.swing.JLabel();
        info_name = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        info_type = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        info_driver = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        info_driver_assistant = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        info_sf = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        info_from = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        info_destination = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        info_eta = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        info_etd = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        info_platform = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        info_delay = new javax.swing.JLabel();
        panel_info_sf = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        table_sf = new javax.swing.JTable();
        panel_info_info = new javax.swing.JPanel();
        info_info = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jSeparator5 = new javax.swing.JSeparator();
        jScrollPane4 = new javax.swing.JScrollPane();
        table_log = new javax.swing.JTable();
        jScrollPane5 = new javax.swing.JScrollPane();
        table_note = new javax.swing.JTable();
        txtCatatan = new javax.swing.JTextField();
        pb_status = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        ST_jpl = new javax.swing.JLabel();
        ST_gangguan = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        ST_gangguan_komponen1 = new javax.swing.JLabel();
        ST_gangguan_komponen2 = new javax.swing.JLabel();
        ST_gangguan_komponen3 = new javax.swing.JLabel();
        ST_repair = new javax.swing.JButton();
        ST_info = new javax.swing.JLabel();
        label_bua = new javax.swing.JLabel();
        label_kldb = new javax.swing.JLabel();
        signal_b101 = new javax.swing.JLabel();
        speed_signal_j10 = new javax.swing.JLabel();
        panel_master = new javax.swing.JPanel();
        label_panel_lock_unlock = new java.awt.Label();
        btn_unlock_1 = new java.awt.Button();
        btn_unlock_2 = new java.awt.Button();
        btn_lock = new java.awt.Button();
        indicator_master = new javax.swing.JTextField();
        panel_indicator = new javax.swing.JPanel();
        indicator_arrival_jng = new javax.swing.JTextField();
        label_arrival_indicator = new java.awt.Label();
        indicator_arrival_bks = new javax.swing.JTextField();
        counter_arrival = new javax.swing.JTextField();
        label_failure_indicator = new java.awt.Label();
        indicator_failure_signal = new javax.swing.JTextField();
        indicator_failure_junction = new javax.swing.JTextField();
        counter_failure = new javax.swing.JTextField();
        btn_reset_indicator = new java.awt.Button();
        panel_notification = new javax.swing.JPanel();
        label_send_notification = new java.awt.Label();
        btn_notification_jng = new java.awt.Button();
        btn_notification_bks = new java.awt.Button();
        indicator_notification_jng = new javax.swing.JTextField();
        indicator_notification_bks = new javax.swing.JTextField();
        label_send_notification1 = new java.awt.Label();
        label_notification_arrival_jng = new java.awt.Label();
        label_notification_arrival_bks = new java.awt.Label();
        indicator_notification_arrival_bks = new javax.swing.JTextField();
        indicator_notification_arrival_jng = new javax.swing.JTextField();
        panel_junction = new javax.swing.JPanel();
        counter_lock_all_junction = new javax.swing.JTextField();
        btn_lock_all_junction = new java.awt.Button();
        btn_remove_obstacle = new java.awt.Button();
        counter_remove_obstacle = new javax.swing.JTextField();
        counter_unlock_all_junction = new javax.swing.JTextField();
        btn_reposition_junction = new java.awt.Button();
        counter_reposition_junction = new javax.swing.JTextField();
        counter_lock_junction = new javax.swing.JTextField();
        counter_unlock_junction = new javax.swing.JTextField();
        btn_unlock_all_junction = new java.awt.Button();
        btn_lock_junction = new java.awt.Button();
        btn_unlock_junction = new java.awt.Button();
        counter_junction_changed = new javax.swing.JTextField();
        label_junction_changed1 = new java.awt.Label();
        panel_route = new javax.swing.JPanel();
        label_created_route = new java.awt.Label();
        label_removed_route = new java.awt.Label();
        counter_created_route = new javax.swing.JTextField();
        counter_removed_route = new javax.swing.JTextField();
        label_title = new javax.swing.JLabel();
        btn_depart_platform_1 = new java.awt.Button();
        btn_depart_platform_2 = new java.awt.Button();
        btn_depart_platform_3 = new java.awt.Button();
        btn_depart_platform_4 = new java.awt.Button();
        btn_depart_platform_5 = new java.awt.Button();
        btn_depart_platform_6 = new java.awt.Button();
        label_cuk7 = new javax.swing.JLabel();
        bar = new javax.swing.JMenuBar();
        mnPilihan = new javax.swing.JMenu();
        mnPilihanReset = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        mnPilihanMainMenu = new javax.swing.JMenuItem();
        mnPilihanExit = new javax.swing.JMenuItem();
        mnHelp = new javax.swing.JMenu();
        mnOpenITD = new javax.swing.JMenuItem();
        mnOpenCTA = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("PPKA Cakung- Indonesia Train Dispatcher");
        setIconImage(Toolkit.getDefaultToolkit().getImage(ControlPanelCUK.class.getResource("/indotraindisp/img/logo.png")));

        panelAtas.setBackground(new java.awt.Color(153, 153, 153));

        labelTime.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        labelTime.setForeground(new java.awt.Color(0, 102, 102));
        labelTime.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelTime.setText("WAKTU SEKARANG:");

        label_score.setFont(new java.awt.Font("Quartz", 1, 24)); // NOI18N
        label_score.setForeground(new java.awt.Color(0, 0, 255));
        label_score.setText("0");

        labelPoint.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        labelPoint.setForeground(new java.awt.Color(0, 102, 102));
        labelPoint.setText("SCORE:");

        label_time.setFont(new java.awt.Font("Quartz", 1, 24)); // NOI18N
        label_time.setText("07:00:00");

        txt_notification.setFont(new java.awt.Font("Rockwell", 2, 18)); // NOI18N
        txt_notification.setForeground(new java.awt.Color(153, 0, 0));
        txt_notification.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txt_notification.setText("Mengatur Simulasi . .");

        labelPlayTime.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        labelPlayTime.setForeground(new java.awt.Color(0, 102, 102));
        labelPlayTime.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelPlayTime.setText("LAMA DINASAN:");

        label_elapsed.setFont(new java.awt.Font("Quartz", 1, 24)); // NOI18N
        label_elapsed.setText("00:00:00");

        javax.swing.GroupLayout panelAtasLayout = new javax.swing.GroupLayout(panelAtas);
        panelAtas.setLayout(panelAtasLayout);
        panelAtasLayout.setHorizontalGroup(
            panelAtasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelAtasLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(labelPoint)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(label_score)
                .addGap(18, 18, 18)
                .addGroup(panelAtasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelAtasLayout.createSequentialGroup()
                        .addComponent(labelTime)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(label_time))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelAtasLayout.createSequentialGroup()
                        .addComponent(labelPlayTime)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(label_elapsed)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 185, Short.MAX_VALUE)
                .addComponent(txt_notification)
                .addContainerGap())
        );
        panelAtasLayout.setVerticalGroup(
            panelAtasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(labelPoint, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(label_score, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelAtasLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txt_notification, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelAtasLayout.createSequentialGroup()
                .addGroup(panelAtasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(label_time, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(labelTime, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelAtasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelPlayTime, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(label_elapsed, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
        );

        panelUtama.setBackground(new java.awt.Color(153, 153, 153));
        panelUtama.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel3.setFont(new java.awt.Font("Book Antiqua", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(51, 51, 51));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Layout Cakung V1.2 by TCI");
        panelUtama.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 680, 300, 30));

        c17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(c17, new org.netbeans.lib.awtextra.AbsoluteConstraints(1380, 440, 60, 30));

        signal_j32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokMerahKanan.png"))); // NOI18N
        signal_j32.setToolTipText("Klik untuk membentuk rute");
        signal_j32.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        signal_j32.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                signal_j32MouseClicked(evt);
            }
        });
        panelUtama.add(signal_j32, new org.netbeans.lib.awtextra.AbsoluteConstraints(910, 640, 60, 20));

        junction_13a.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchLeftStraightBlack.png"))); // NOI18N
        junction_13a.setToolTipText("Klik untuk mengubah arah wesel");
        junction_13a.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        junction_13a.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                junction_13aMouseClicked(evt);
            }
        });
        panelUtama.add(junction_13a, new org.netbeans.lib.awtextra.AbsoluteConstraints(1130, 550, 60, 30));

        label_cuk4.setBackground(new java.awt.Color(102, 102, 102));
        label_cuk4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        label_cuk4.setForeground(new java.awt.Color(255, 255, 255));
        label_cuk4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_cuk4.setText("IV");
        label_cuk4.setOpaque(true);
        panelUtama.add(label_cuk4, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 390, 40, 20));

        signal_uj10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalRepeaterMerahKanan.png"))); // NOI18N
        signal_uj10.setToolTipText("Sinyal Repeater");
        signal_uj10.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        panelUtama.add(signal_uj10, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 570, 60, 20));

        btn_bell.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/iconTeleponOff.png"))); // NOI18N
        btn_bell.setToolTipText("Tekan untuk memutar/menghentikan bell");
        btn_bell.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_bell.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_bellMouseClicked(evt);
            }
        });
        panelUtama.add(btn_bell, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 610, -1, -1));

        labelBellOnOff.setBackground(new java.awt.Color(102, 102, 102));
        labelBellOnOff.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        labelBellOnOff.setForeground(new java.awt.Color(255, 255, 255));
        labelBellOnOff.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelBellOnOff.setText("Bell On/Off");
        labelBellOnOff.setOpaque(true);
        panelUtama.add(labelBellOnOff, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 690, 96, -1));

        jLabel6.setFont(new java.awt.Font("Book Antiqua", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 0, 255));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("Cakung +18M");
        panelUtama.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 680, 160, 30));

        label_signal_b209.setBackground(new java.awt.Color(153, 153, 153));
        label_signal_b209.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_signal_b209.setForeground(new java.awt.Color(255, 255, 255));
        label_signal_b209.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_signal_b209.setText("B209");
        label_signal_b209.setOpaque(true);
        panelUtama.add(label_signal_b209, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 460, 40, 10));

        block_201.setEditable(false);
        block_201.setBackground(new java.awt.Color(204, 204, 204));
        block_201.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        block_201.setForeground(new java.awt.Color(255, 0, 0));
        block_201.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        block_201.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUtama.add(block_201, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 490, 60, 30));

        block_101.setEditable(false);
        block_101.setBackground(new java.awt.Color(204, 204, 204));
        block_101.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        block_101.setForeground(new java.awt.Color(255, 0, 0));
        block_101.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        block_101.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUtama.add(block_101, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 540, 60, 30));

        label_101.setBackground(new java.awt.Color(102, 102, 102));
        label_101.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_101.setForeground(new java.awt.Color(255, 255, 255));
        label_101.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_101.setText("101");
        label_101.setOpaque(true);
        panelUtama.add(label_101, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 570, 40, 10));

        signal_b208.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokHijauKiri.png"))); // NOI18N
        signal_b208.setToolTipText("Sinyal Blok");
        signal_b208.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        panelUtama.add(signal_b208, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 470, 60, 20));

        a1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 550, 60, 10));

        b5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b5, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 500, 90, 10));

        a17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a17, new org.netbeans.lib.awtextra.AbsoluteConstraints(1830, 550, 60, 10));

        block_102.setEditable(false);
        block_102.setBackground(new java.awt.Color(204, 204, 204));
        block_102.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        block_102.setForeground(new java.awt.Color(255, 0, 0));
        block_102.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        block_102.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUtama.add(block_102, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 540, 60, 30));

        label_block_102.setBackground(new java.awt.Color(102, 102, 102));
        label_block_102.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_block_102.setForeground(new java.awt.Color(255, 255, 255));
        label_block_102.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_block_102.setText("102");
        label_block_102.setOpaque(true);
        panelUtama.add(label_block_102, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 570, 40, 10));

        a13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a13, new org.netbeans.lib.awtextra.AbsoluteConstraints(1340, 550, 150, 10));

        c10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(c10, new org.netbeans.lib.awtextra.AbsoluteConstraints(910, 320, 110, 10));

        a3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a3, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 550, 80, 10));

        a5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a5, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 550, 90, 10));

        c4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(c4, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 320, 80, 10));

        block_302.setEditable(false);
        block_302.setBackground(new java.awt.Color(204, 204, 204));
        block_302.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        block_302.setForeground(new java.awt.Color(255, 0, 0));
        block_302.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        block_302.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUtama.add(block_302, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 310, 60, 30));

        label_block_302.setBackground(new java.awt.Color(102, 102, 102));
        label_block_302.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_block_302.setForeground(new java.awt.Color(255, 255, 255));
        label_block_302.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_block_302.setText("302");
        label_block_302.setOpaque(true);
        panelUtama.add(label_block_302, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 340, 40, 10));

        signal_j60.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalMasuk2MerahKanan.png"))); // NOI18N
        signal_j60.setToolTipText("klik untuk membentuk rute");
        signal_j60.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        signal_j60.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                signal_j60MouseClicked(evt);
            }
        });
        panelUtama.add(signal_j60, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 340, 50, 20));

        c3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(c3, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 320, 80, 10));

        c2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(c2, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 320, 80, 10));

        block_301.setEditable(false);
        block_301.setBackground(new java.awt.Color(204, 204, 204));
        block_301.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        block_301.setForeground(new java.awt.Color(255, 0, 0));
        block_301.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        block_301.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUtama.add(block_301, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 310, 60, 30));

        label_block_301.setBackground(new java.awt.Color(102, 102, 102));
        label_block_301.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_block_301.setForeground(new java.awt.Color(255, 255, 255));
        label_block_301.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_block_301.setText("301");
        label_block_301.setOpaque(true);
        panelUtama.add(label_block_301, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 340, 40, 10));

        c1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        c1.addContainerListener(new java.awt.event.ContainerAdapter() {
            public void componentAdded(java.awt.event.ContainerEvent evt) {
                c1ComponentAdded(evt);
            }
        });
        panelUtama.add(c1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 320, 60, 10));

        label_jng_2.setBackground(new java.awt.Color(102, 102, 102));
        label_jng_2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        label_jng_2.setForeground(new java.awt.Color(255, 255, 255));
        label_jng_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_jng_2.setText("<- JNG");
        label_jng_2.setOpaque(true);
        panelUtama.add(label_jng_2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 290, 40, 20));

        d1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(d1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 270, 60, 10));

        signal_b406.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokHijauKiri.png"))); // NOI18N
        signal_b406.setToolTipText("B102");
        signal_b406.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        panelUtama.add(signal_b406, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 240, 60, 20));

        block_401.setEditable(false);
        block_401.setBackground(new java.awt.Color(204, 204, 204));
        block_401.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        block_401.setForeground(new java.awt.Color(255, 0, 0));
        block_401.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        block_401.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUtama.add(block_401, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 260, 60, 30));

        label_block_401.setBackground(new java.awt.Color(102, 102, 102));
        label_block_401.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_block_401.setForeground(new java.awt.Color(255, 255, 255));
        label_block_401.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_block_401.setText("401");
        label_block_401.setOpaque(true);
        panelUtama.add(label_block_401, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 250, 40, 10));

        d2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(d2, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 270, 80, 10));

        d3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(d3, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 270, 80, 10));

        d4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(d4, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 270, 60, 10));

        d5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(d5, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 270, 80, 10));

        block_202.setEditable(false);
        block_202.setBackground(new java.awt.Color(204, 204, 204));
        block_202.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        block_202.setForeground(new java.awt.Color(255, 0, 0));
        block_202.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        block_202.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUtama.add(block_202, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 490, 60, 30));

        label_kri.setBackground(new java.awt.Color(102, 102, 102));
        label_kri.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_kri.setForeground(new java.awt.Color(255, 255, 255));
        label_kri.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_kri.setText("Kranji (KRI) ->");
        label_kri.setOpaque(true);
        panelUtama.add(label_kri, new org.netbeans.lib.awtextra.AbsoluteConstraints(2090, 450, 80, -1));

        signal_j42.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokMerahKiri.png"))); // NOI18N
        signal_j42.setToolTipText("Klik untuk membentuk rute");
        signal_j42.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        signal_j42.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                signal_j42MouseClicked(evt);
            }
        });
        panelUtama.add(signal_j42, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 400, 60, 20));

        signal_b301.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokKuningKanan.png"))); // NOI18N
        signal_b301.setToolTipText("Sinyal Blok");
        signal_b301.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        panelUtama.add(signal_b301, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 340, 60, 20));

        speed_signal_j24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/taspat30OffKiri.png"))); // NOI18N
        speed_signal_j24.setToolTipText("");
        speed_signal_j24.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        panelUtama.add(speed_signal_j24, new org.netbeans.lib.awtextra.AbsoluteConstraints(1750, 470, -1, 20));

        junction_21a.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchRightStraightBlack.png"))); // NOI18N
        junction_21a.setToolTipText("Klik untuk mengubah arah wesel");
        junction_21a.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        junction_21a.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                junction_21aMouseClicked(evt);
            }
        });
        panelUtama.add(junction_21a, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 500, 60, 30));

        x_21a_11a.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(x_21a_11a, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 520, 30, 30));

        junction_24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchLeftStraightBlackUpside.png"))); // NOI18N
        junction_24.setToolTipText("Klik untuk mengubah arah wesel");
        junction_24.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        junction_24.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                junction_24MouseClicked(evt);
            }
        });
        panelUtama.add(junction_24, new org.netbeans.lib.awtextra.AbsoluteConstraints(1670, 480, 60, 30));

        b6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b6, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 500, 80, 10));

        junction_24a2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchRightStraightBlack.png"))); // NOI18N
        junction_24a2.setToolTipText("Klik untuk mengubah arah wesel");
        junction_24a2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        junction_24a2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                junction_24a2MouseClicked(evt);
            }
        });
        panelUtama.add(junction_24a2, new org.netbeans.lib.awtextra.AbsoluteConstraints(1580, 500, 60, 30));

        bb8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(bb8, new org.netbeans.lib.awtextra.AbsoluteConstraints(1100, 470, 30, 30));

        junction_21b.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchRightStraightBlackUpside.png"))); // NOI18N
        junction_21b.setToolTipText("Klik untuk mengubah arah wesel");
        junction_21b.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        junction_21b.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                junction_21bMouseClicked(evt);
            }
        });
        panelUtama.add(junction_21b, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 480, 60, 30));

        ab8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalRightBlack.png"))); // NOI18N
        panelUtama.add(ab8, new org.netbeans.lib.awtextra.AbsoluteConstraints(1100, 560, 30, 30));

        a4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a4, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 550, 80, 10));

        bb2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalRightBlack.png"))); // NOI18N
        panelUtama.add(bb2, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 450, 60, 30));

        b4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b4, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 500, 80, 10));

        block_030.setEditable(false);
        block_030.setBackground(new java.awt.Color(204, 204, 204));
        block_030.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        block_030.setForeground(new java.awt.Color(255, 0, 0));
        block_030.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        block_030.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUtama.add(block_030, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 490, 60, 30));

        label_block_030.setBackground(new java.awt.Color(102, 102, 102));
        label_block_030.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_block_030.setForeground(new java.awt.Color(255, 255, 255));
        label_block_030.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_block_030.setText("030");
        label_block_030.setOpaque(true);
        panelUtama.add(label_block_030, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 480, 40, 10));

        ab4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(ab4, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 620, 70, 10));

        block_040.setEditable(false);
        block_040.setBackground(new java.awt.Color(204, 204, 204));
        block_040.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        block_040.setForeground(new java.awt.Color(255, 0, 0));
        block_040.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        block_040.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUtama.add(block_040, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 420, 60, 30));

        label_block_040.setBackground(new java.awt.Color(102, 102, 102));
        label_block_040.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_block_040.setForeground(new java.awt.Color(255, 255, 255));
        label_block_040.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_block_040.setText("040");
        label_block_040.setOpaque(true);
        panelUtama.add(label_block_040, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 410, 40, 10));

        label_jng.setBackground(new java.awt.Color(102, 102, 102));
        label_jng.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        label_jng.setForeground(new java.awt.Color(255, 255, 255));
        label_jng.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_jng.setText("<- JNG");
        label_jng.setOpaque(true);
        panelUtama.add(label_jng, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 520, 40, 20));

        b19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b19, new org.netbeans.lib.awtextra.AbsoluteConstraints(2110, 500, 60, 10));

        a8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a8, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 550, 60, 10));

        b7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b7, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 500, 80, 10));

        label_bks.setBackground(new java.awt.Color(102, 102, 102));
        label_bks.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        label_bks.setForeground(new java.awt.Color(255, 255, 255));
        label_bks.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_bks.setText("BKS ->");
        label_bks.setOpaque(true);
        panelUtama.add(label_bks, new org.netbeans.lib.awtextra.AbsoluteConstraints(2130, 520, 40, 20));

        block_020.setEditable(false);
        block_020.setBackground(new java.awt.Color(204, 204, 204));
        block_020.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        block_020.setForeground(new java.awt.Color(255, 0, 0));
        block_020.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        block_020.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUtama.add(block_020, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 540, 60, 30));

        label_block_020.setBackground(new java.awt.Color(102, 102, 102));
        label_block_020.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_block_020.setForeground(new java.awt.Color(255, 255, 255));
        label_block_020.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_block_020.setText("020");
        label_block_020.setOpaque(true);
        panelUtama.add(label_block_020, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 570, 40, 10));

        ab2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(ab2, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 580, 60, 30));

        label_kldb2.setBackground(new java.awt.Color(102, 102, 102));
        label_kldb2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        label_kldb2.setForeground(new java.awt.Color(255, 255, 255));
        label_kldb2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_kldb2.setText("KLDB2");
        label_kldb2.setOpaque(true);
        panelUtama.add(label_kldb2, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 580, 40, 20));

        block_010.setEditable(false);
        block_010.setBackground(new java.awt.Color(204, 204, 204));
        block_010.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        block_010.setForeground(new java.awt.Color(255, 0, 0));
        block_010.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        block_010.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUtama.add(block_010, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 610, 60, 30));

        label_block_010.setBackground(new java.awt.Color(102, 102, 102));
        label_block_010.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_block_010.setForeground(new java.awt.Color(255, 255, 255));
        label_block_010.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_block_010.setText("010");
        label_block_010.setOpaque(true);
        panelUtama.add(label_block_010, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 640, 40, 10));

        label_cuk1.setBackground(new java.awt.Color(102, 102, 102));
        label_cuk1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        label_cuk1.setForeground(new java.awt.Color(255, 255, 255));
        label_cuk1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_cuk1.setText("I");
        label_cuk1.setOpaque(true);
        panelUtama.add(label_cuk1, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 650, 40, 20));

        bb5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(bb5, new org.netbeans.lib.awtextra.AbsoluteConstraints(910, 430, 70, 10));

        ab5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(ab5, new org.netbeans.lib.awtextra.AbsoluteConstraints(910, 620, 70, 10));

        ab3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(ab3, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 600, 60, 30));

        bb6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(bb6, new org.netbeans.lib.awtextra.AbsoluteConstraints(980, 430, 60, 30));

        b8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b8, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 500, 60, 10));

        b9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b9, new org.netbeans.lib.awtextra.AbsoluteConstraints(910, 500, 60, 10));

        a9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a9, new org.netbeans.lib.awtextra.AbsoluteConstraints(910, 550, 60, 10));

        a7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a7, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 550, 80, 10));

        ab7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalRightBlack.png"))); // NOI18N
        panelUtama.add(ab7, new org.netbeans.lib.awtextra.AbsoluteConstraints(1040, 580, 60, 30));

        a6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a6, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 550, 80, 10));

        bb4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(bb4, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 430, 70, 10));

        a11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a11, new org.netbeans.lib.awtextra.AbsoluteConstraints(1050, 550, 80, 10));

        b10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b10, new org.netbeans.lib.awtextra.AbsoluteConstraints(970, 500, 80, 10));

        c18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(c18, new org.netbeans.lib.awtextra.AbsoluteConstraints(1440, 460, -1, 30));

        junction_11a.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchLeftStraightBlackUpside.png"))); // NOI18N
        junction_11a.setToolTipText("Klik untuk mengubah arah wesel");
        junction_11a.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        junction_11a.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                junction_11aMouseClicked(evt);
            }
        });
        panelUtama.add(junction_11a, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 530, 60, 30));

        a10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a10, new org.netbeans.lib.awtextra.AbsoluteConstraints(970, 550, 80, 10));

        bb3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalRightBlack.png"))); // NOI18N
        panelUtama.add(bb3, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 430, 60, 30));

        b15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b15, new org.netbeans.lib.awtextra.AbsoluteConstraints(1640, 500, 30, 10));

        ab6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalRightBlack.png"))); // NOI18N
        panelUtama.add(ab6, new org.netbeans.lib.awtextra.AbsoluteConstraints(980, 600, 60, 30));

        bb1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalRightBlack.png"))); // NOI18N
        panelUtama.add(bb1, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 460, 30, 30));

        signal_b209.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokHijauKiri.png"))); // NOI18N
        signal_b209.setToolTipText("Sinyal Blok");
        signal_b209.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        panelUtama.add(signal_b209, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 470, 60, 20));

        signal_j82.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokMerahKiri.png"))); // NOI18N
        signal_j82.setToolTipText("Klik untuk membentuk rute");
        signal_j82.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        signal_j82.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                signal_j82MouseClicked(evt);
            }
        });
        panelUtama.add(signal_j82, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 240, 60, 20));

        signal_j10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokMerahKanan.png"))); // NOI18N
        signal_j10.setToolTipText("klik untuk membentuk rute");
        signal_j10.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        signal_j10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                signal_j10MouseClicked(evt);
            }
        });
        panelUtama.add(signal_j10, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 570, 60, 20));

        signal_j62.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokMerahKanan.png"))); // NOI18N
        signal_j62.setToolTipText("Klik untuk membentuk rute");
        signal_j62.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        signal_j62.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                signal_j62MouseClicked(evt);
            }
        });
        panelUtama.add(signal_j62, new org.netbeans.lib.awtextra.AbsoluteConstraints(910, 340, 60, 20));

        b2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b2, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 500, 80, 10));

        c5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(c5, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 320, 80, 10));

        c6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(c6, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 320, 80, 10));

        c7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(c7, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 320, 80, 10));

        c8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(c8, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 320, 80, 10));

        label_cuk5.setBackground(new java.awt.Color(102, 102, 102));
        label_cuk5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        label_cuk5.setForeground(new java.awt.Color(255, 255, 255));
        label_cuk5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_cuk5.setText("V");
        label_cuk5.setOpaque(true);
        panelUtama.add(label_cuk5, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 350, 40, 20));

        label_block_050.setBackground(new java.awt.Color(102, 102, 102));
        label_block_050.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_block_050.setForeground(new java.awt.Color(255, 255, 255));
        label_block_050.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_block_050.setText("050");
        label_block_050.setOpaque(true);
        panelUtama.add(label_block_050, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 340, 40, 10));

        block_050.setEditable(false);
        block_050.setBackground(new java.awt.Color(204, 204, 204));
        block_050.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        block_050.setForeground(new java.awt.Color(255, 0, 0));
        block_050.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        block_050.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUtama.add(block_050, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 310, 60, 30));

        block_060.setEditable(false);
        block_060.setBackground(new java.awt.Color(204, 204, 204));
        block_060.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        block_060.setForeground(new java.awt.Color(255, 0, 0));
        block_060.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        block_060.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUtama.add(block_060, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 260, 60, 30));

        label_block_060.setBackground(new java.awt.Color(102, 102, 102));
        label_block_060.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_block_060.setForeground(new java.awt.Color(255, 255, 255));
        label_block_060.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_block_060.setText("060");
        label_block_060.setOpaque(true);
        panelUtama.add(label_block_060, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 250, 40, 10));

        label_cuk6.setBackground(new java.awt.Color(102, 102, 102));
        label_cuk6.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        label_cuk6.setForeground(new java.awt.Color(255, 255, 255));
        label_cuk6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_cuk6.setText("VI");
        label_cuk6.setOpaque(true);
        panelUtama.add(label_cuk6, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 230, 40, 20));

        d6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(d6, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 270, 80, 10));

        d7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(d7, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 270, 80, 10));

        d8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(d8, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 270, 80, 10));

        d9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(d9, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 270, 80, 10));

        d11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(d11, new org.netbeans.lib.awtextra.AbsoluteConstraints(910, 270, 130, 10));

        junction_13b.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchRightStraightBlackUpside.png"))); // NOI18N
        junction_13b.setToolTipText("Klik untuk mengubah arah wesel");
        junction_13b.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        junction_13b.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                junction_13bMouseClicked(evt);
            }
        });
        panelUtama.add(junction_13b, new org.netbeans.lib.awtextra.AbsoluteConstraints(1190, 530, 60, 30));

        x_13b_23b.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalRightBlack.png"))); // NOI18N
        panelUtama.add(x_13b_23b, new org.netbeans.lib.awtextra.AbsoluteConstraints(1250, 510, 30, 30));

        junction_23b.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchLeftStraightBlack.png"))); // NOI18N
        junction_23b.setToolTipText("Klik untuk mengubah arah wesel");
        junction_23b.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        junction_23b.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                junction_23bMouseClicked(evt);
            }
        });
        panelUtama.add(junction_23b, new org.netbeans.lib.awtextra.AbsoluteConstraints(1280, 500, 60, 30));

        b12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b12, new org.netbeans.lib.awtextra.AbsoluteConstraints(1190, 500, 90, 10));

        a16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a16, new org.netbeans.lib.awtextra.AbsoluteConstraints(1730, 550, 100, 10));

        b13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b13, new org.netbeans.lib.awtextra.AbsoluteConstraints(1340, 500, 160, 10));

        c9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(c9, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 320, 110, 10));

        d10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(d10, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 270, 110, 10));

        bb7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(bb7, new org.netbeans.lib.awtextra.AbsoluteConstraints(1040, 450, 60, 30));

        c11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(c11, new org.netbeans.lib.awtextra.AbsoluteConstraints(1020, 320, 60, 30));

        c12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(c12, new org.netbeans.lib.awtextra.AbsoluteConstraints(1080, 340, 60, 30));

        c13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(c13, new org.netbeans.lib.awtextra.AbsoluteConstraints(1140, 360, 60, 30));

        c14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(c14, new org.netbeans.lib.awtextra.AbsoluteConstraints(1200, 380, 60, 30));

        c15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(c15, new org.netbeans.lib.awtextra.AbsoluteConstraints(1260, 400, 60, 30));

        c16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(c16, new org.netbeans.lib.awtextra.AbsoluteConstraints(1320, 420, 60, 30));

        ab1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(ab1, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 570, 30, 30));

        junction_23a.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchLeftStraightBlackUpside.png"))); // NOI18N
        junction_23a.setToolTipText("Klik untuk mengubah arah wesel");
        junction_23a.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        junction_23a.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                junction_23aMouseClicked(evt);
            }
        });
        panelUtama.add(junction_23a, new org.netbeans.lib.awtextra.AbsoluteConstraints(1130, 480, 60, 30));

        a14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a14, new org.netbeans.lib.awtextra.AbsoluteConstraints(1490, 550, 100, 10));

        junction_11b.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchRightStraightBlack.png"))); // NOI18N
        junction_11b.setToolTipText("Klik untuk mengubah arah wesel");
        junction_11b.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        junction_11b.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                junction_11bMouseClicked(evt);
            }
        });
        panelUtama.add(junction_11b, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 550, 60, 30));

        x_24a2_14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(x_24a2_14, new org.netbeans.lib.awtextra.AbsoluteConstraints(1640, 520, 30, 30));

        junction_14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchLeftStraightBlackUpside.png"))); // NOI18N
        junction_14.setToolTipText("Klik untuk mengubah arah wesel");
        junction_14.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        junction_14.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                junction_14MouseClicked(evt);
            }
        });
        panelUtama.add(junction_14, new org.netbeans.lib.awtextra.AbsoluteConstraints(1670, 530, 60, 30));

        a12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a12, new org.netbeans.lib.awtextra.AbsoluteConstraints(1250, 550, 90, 10));

        a15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a15, new org.netbeans.lib.awtextra.AbsoluteConstraints(1590, 550, 80, 10));

        b11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b11, new org.netbeans.lib.awtextra.AbsoluteConstraints(1050, 500, 80, 10));

        b14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b14, new org.netbeans.lib.awtextra.AbsoluteConstraints(1560, 500, 20, 10));

        junction_24a1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchLeftStraightBlackUpside.png"))); // NOI18N
        junction_24a1.setToolTipText("Klik untuk mengubah arah wesel");
        junction_24a1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        junction_24a1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                junction_24a1MouseClicked(evt);
            }
        });
        panelUtama.add(junction_24a1, new org.netbeans.lib.awtextra.AbsoluteConstraints(1500, 480, 60, 30));

        d22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(d22, new org.netbeans.lib.awtextra.AbsoluteConstraints(1640, 470, 30, 30));

        d12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(d12, new org.netbeans.lib.awtextra.AbsoluteConstraints(1040, 270, 60, 30));

        d21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(d21, new org.netbeans.lib.awtextra.AbsoluteConstraints(1580, 450, 60, 30));

        d20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(d20, new org.netbeans.lib.awtextra.AbsoluteConstraints(1520, 430, 60, 30));

        d19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(d19, new org.netbeans.lib.awtextra.AbsoluteConstraints(1460, 410, 60, 30));

        d18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(d18, new org.netbeans.lib.awtextra.AbsoluteConstraints(1400, 390, 60, 30));

        d17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(d17, new org.netbeans.lib.awtextra.AbsoluteConstraints(1340, 370, 60, 30));

        d16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(d16, new org.netbeans.lib.awtextra.AbsoluteConstraints(1280, 350, 60, 30));

        d15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(d15, new org.netbeans.lib.awtextra.AbsoluteConstraints(1220, 330, 60, 30));

        d14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(d14, new org.netbeans.lib.awtextra.AbsoluteConstraints(1160, 310, 60, 30));

        d13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(d13, new org.netbeans.lib.awtextra.AbsoluteConstraints(1100, 290, 60, 30));

        a2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a2, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 550, 80, 10));

        b16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b16, new org.netbeans.lib.awtextra.AbsoluteConstraints(1730, 500, 100, 10));

        block_202b.setEditable(false);
        block_202b.setBackground(new java.awt.Color(204, 204, 204));
        block_202b.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        block_202b.setForeground(new java.awt.Color(255, 0, 0));
        block_202b.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        block_202b.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUtama.add(block_202b, new org.netbeans.lib.awtextra.AbsoluteConstraints(1830, 490, 60, 30));

        label_202b.setBackground(new java.awt.Color(102, 102, 102));
        label_202b.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_202b.setForeground(new java.awt.Color(255, 255, 255));
        label_202b.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_202b.setText("202B");
        label_202b.setOpaque(true);
        panelUtama.add(label_202b, new org.netbeans.lib.awtextra.AbsoluteConstraints(1840, 480, 40, 10));

        signal_j24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokMerahKiri.png"))); // NOI18N
        signal_j24.setToolTipText("Klik untuk membentuk rute");
        signal_j24.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        signal_j24.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                signal_j24MouseClicked(evt);
            }
        });
        panelUtama.add(signal_j24, new org.netbeans.lib.awtextra.AbsoluteConstraints(1770, 470, 60, 20));

        b3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b3, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 500, 80, 10));

        b18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b18, new org.netbeans.lib.awtextra.AbsoluteConstraints(1970, 500, 80, 10));

        a19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a19, new org.netbeans.lib.awtextra.AbsoluteConstraints(1970, 550, 80, 10));

        b17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b17, new org.netbeans.lib.awtextra.AbsoluteConstraints(1890, 500, 80, 10));

        block_203.setEditable(false);
        block_203.setBackground(new java.awt.Color(204, 204, 204));
        block_203.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        block_203.setForeground(new java.awt.Color(255, 0, 0));
        block_203.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        block_203.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUtama.add(block_203, new org.netbeans.lib.awtextra.AbsoluteConstraints(2050, 490, 60, 30));

        label_block_203.setBackground(new java.awt.Color(102, 102, 102));
        label_block_203.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_block_203.setForeground(new java.awt.Color(255, 255, 255));
        label_block_203.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_block_203.setText("203");
        label_block_203.setOpaque(true);
        panelUtama.add(label_block_203, new org.netbeans.lib.awtextra.AbsoluteConstraints(2060, 480, 40, 10));

        signal_b201.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokKuningKiri.png"))); // NOI18N
        signal_b201.setToolTipText("Sinyal Blok");
        signal_b201.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        panelUtama.add(signal_b201, new org.netbeans.lib.awtextra.AbsoluteConstraints(1990, 470, 60, 20));

        blok_129.setEditable(false);
        blok_129.setBackground(new java.awt.Color(204, 204, 204));
        blok_129.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        blok_129.setForeground(new java.awt.Color(255, 0, 0));
        blok_129.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        blok_129.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUtama.add(blok_129, new org.netbeans.lib.awtextra.AbsoluteConstraints(1830, 490, 60, 30));

        label_129.setBackground(new java.awt.Color(102, 102, 102));
        label_129.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_129.setForeground(new java.awt.Color(255, 255, 255));
        label_129.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_129.setText("201");
        label_129.setOpaque(true);
        panelUtama.add(label_129, new org.netbeans.lib.awtextra.AbsoluteConstraints(1840, 480, 40, 10));

        sl_b102_w14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokHijauKiri.png"))); // NOI18N
        sl_b102_w14.setToolTipText("B102");
        sl_b102_w14.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        panelUtama.add(sl_b102_w14, new org.netbeans.lib.awtextra.AbsoluteConstraints(1770, 470, 60, 20));

        a18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a18, new org.netbeans.lib.awtextra.AbsoluteConstraints(1890, 550, 80, 10));

        block_103.setEditable(false);
        block_103.setBackground(new java.awt.Color(204, 204, 204));
        block_103.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        block_103.setForeground(new java.awt.Color(255, 0, 0));
        block_103.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        block_103.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUtama.add(block_103, new org.netbeans.lib.awtextra.AbsoluteConstraints(2050, 540, 60, 30));

        label_block_103.setBackground(new java.awt.Color(102, 102, 102));
        label_block_103.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_block_103.setForeground(new java.awt.Color(255, 255, 255));
        label_block_103.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_block_103.setText("103");
        label_block_103.setOpaque(true);
        panelUtama.add(label_block_103, new org.netbeans.lib.awtextra.AbsoluteConstraints(2060, 570, 40, 10));

        signal_b105.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokHijauKanan.png"))); // NOI18N
        signal_b105.setToolTipText("Sinyal Blok");
        signal_b105.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        panelUtama.add(signal_b105, new org.netbeans.lib.awtextra.AbsoluteConstraints(2110, 570, 60, 20));

        b1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 500, 60, 10));

        a20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a20, new org.netbeans.lib.awtextra.AbsoluteConstraints(2110, 550, 60, 10));

        label_cuk2.setBackground(new java.awt.Color(102, 102, 102));
        label_cuk2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        label_cuk2.setForeground(new java.awt.Color(255, 255, 255));
        label_cuk2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_cuk2.setText("II");
        label_cuk2.setOpaque(true);
        panelUtama.add(label_cuk2, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 580, 40, 20));

        label_junction_24.setBackground(new java.awt.Color(102, 102, 102));
        label_junction_24.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_junction_24.setForeground(new java.awt.Color(255, 255, 255));
        label_junction_24.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_junction_24.setText("24");
        label_junction_24.setOpaque(true);
        panelUtama.add(label_junction_24, new org.netbeans.lib.awtextra.AbsoluteConstraints(1680, 460, 40, 10));

        label_signal_b406.setBackground(new java.awt.Color(153, 153, 153));
        label_signal_b406.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_signal_b406.setForeground(new java.awt.Color(255, 255, 255));
        label_signal_b406.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_signal_b406.setText("B406");
        label_signal_b406.setOpaque(true);
        panelUtama.add(label_signal_b406, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 230, 40, 10));

        label_block_202.setBackground(new java.awt.Color(102, 102, 102));
        label_block_202.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_block_202.setForeground(new java.awt.Color(255, 255, 255));
        label_block_202.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_block_202.setText("202");
        label_block_202.setOpaque(true);
        panelUtama.add(label_block_202, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 480, 40, 10));

        label_signal_j60.setBackground(new java.awt.Color(153, 153, 153));
        label_signal_j60.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_signal_j60.setForeground(new java.awt.Color(255, 255, 255));
        label_signal_j60.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_signal_j60.setText("J60");
        label_signal_j60.setOpaque(true);
        panelUtama.add(label_signal_j60, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 360, 30, 10));

        label_cuk3.setBackground(new java.awt.Color(102, 102, 102));
        label_cuk3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        label_cuk3.setForeground(new java.awt.Color(255, 255, 255));
        label_cuk3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_cuk3.setText("III");
        label_cuk3.setOpaque(true);
        panelUtama.add(label_cuk3, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 460, 40, 20));

        label_kldb1.setBackground(new java.awt.Color(102, 102, 102));
        label_kldb1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        label_kldb1.setForeground(new java.awt.Color(255, 255, 255));
        label_kldb1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_kldb1.setText("KLDB1");
        label_kldb1.setOpaque(true);
        panelUtama.add(label_kldb1, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 460, 40, 20));

        label_signal_b208.setBackground(new java.awt.Color(153, 153, 153));
        label_signal_b208.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_signal_b208.setForeground(new java.awt.Color(255, 255, 255));
        label_signal_b208.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_signal_b208.setText("B208");
        label_signal_b208.setOpaque(true);
        panelUtama.add(label_signal_b208, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 460, 40, 10));

        label_signal_uj10.setBackground(new java.awt.Color(153, 153, 153));
        label_signal_uj10.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_signal_uj10.setForeground(new java.awt.Color(255, 255, 255));
        label_signal_uj10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_signal_uj10.setText("UJ10");
        label_signal_uj10.setOpaque(true);
        panelUtama.add(label_signal_uj10, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 590, 40, 10));

        label_signal_b301.setBackground(new java.awt.Color(153, 153, 153));
        label_signal_b301.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_signal_b301.setForeground(new java.awt.Color(255, 255, 255));
        label_signal_b301.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_signal_b301.setText("B301");
        label_signal_b301.setOpaque(true);
        panelUtama.add(label_signal_b301, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 360, 40, 10));

        label_signal_j32.setBackground(new java.awt.Color(153, 153, 153));
        label_signal_j32.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_signal_j32.setForeground(new java.awt.Color(255, 255, 255));
        label_signal_j32.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_signal_j32.setText("J32");
        label_signal_j32.setOpaque(true);
        panelUtama.add(label_signal_j32, new org.netbeans.lib.awtextra.AbsoluteConstraints(920, 660, 40, 10));

        label_signal_b101.setBackground(new java.awt.Color(153, 153, 153));
        label_signal_b101.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_signal_b101.setForeground(new java.awt.Color(255, 255, 255));
        label_signal_b101.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_signal_b101.setText("B101");
        label_signal_b101.setOpaque(true);
        panelUtama.add(label_signal_b101, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 590, 40, 10));

        label_201.setBackground(new java.awt.Color(102, 102, 102));
        label_201.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_201.setForeground(new java.awt.Color(255, 255, 255));
        label_201.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_201.setText("201");
        label_201.setOpaque(true);
        panelUtama.add(label_201, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 480, 40, 10));

        label_junction_21a.setBackground(new java.awt.Color(102, 102, 102));
        label_junction_21a.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_junction_21a.setForeground(new java.awt.Color(255, 255, 255));
        label_junction_21a.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_junction_21a.setText("21A");
        label_junction_21a.setOpaque(true);
        panelUtama.add(label_junction_21a, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 480, 40, 10));

        label_junction_11a.setBackground(new java.awt.Color(102, 102, 102));
        label_junction_11a.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_junction_11a.setForeground(new java.awt.Color(255, 255, 255));
        label_junction_11a.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_junction_11a.setText("11A");
        label_junction_11a.setOpaque(true);
        panelUtama.add(label_junction_11a, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 570, 40, 10));

        label_junction_11b.setBackground(new java.awt.Color(102, 102, 102));
        label_junction_11b.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_junction_11b.setForeground(new java.awt.Color(255, 255, 255));
        label_junction_11b.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_junction_11b.setText("11B");
        label_junction_11b.setOpaque(true);
        panelUtama.add(label_junction_11b, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 590, 40, 10));

        label_signal_j10.setBackground(new java.awt.Color(153, 153, 153));
        label_signal_j10.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_signal_j10.setForeground(new java.awt.Color(255, 255, 255));
        label_signal_j10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_signal_j10.setText("J10");
        label_signal_j10.setOpaque(true);
        panelUtama.add(label_signal_j10, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 590, 40, 10));

        label_signal_j42.setBackground(new java.awt.Color(153, 153, 153));
        label_signal_j42.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_signal_j42.setForeground(new java.awt.Color(255, 255, 255));
        label_signal_j42.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_signal_j42.setText("J42");
        label_signal_j42.setOpaque(true);
        panelUtama.add(label_signal_j42, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 390, 40, 10));

        label_signal_j22.setBackground(new java.awt.Color(153, 153, 153));
        label_signal_j22.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_signal_j22.setForeground(new java.awt.Color(255, 255, 255));
        label_signal_j22.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_signal_j22.setText("J22");
        label_signal_j22.setOpaque(true);
        panelUtama.add(label_signal_j22, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 460, 40, 10));

        label_signal_b105.setBackground(new java.awt.Color(153, 153, 153));
        label_signal_b105.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_signal_b105.setForeground(new java.awt.Color(255, 255, 255));
        label_signal_b105.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_signal_b105.setText("B105");
        label_signal_b105.setOpaque(true);
        panelUtama.add(label_signal_b105, new org.netbeans.lib.awtextra.AbsoluteConstraints(2120, 590, 40, 10));

        label_junction_21b.setBackground(new java.awt.Color(102, 102, 102));
        label_junction_21b.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_junction_21b.setForeground(new java.awt.Color(255, 255, 255));
        label_junction_21b.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_junction_21b.setText("21B");
        label_junction_21b.setOpaque(true);
        panelUtama.add(label_junction_21b, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 460, 40, 10));

        label_junction_23a.setBackground(new java.awt.Color(102, 102, 102));
        label_junction_23a.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_junction_23a.setForeground(new java.awt.Color(255, 255, 255));
        label_junction_23a.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_junction_23a.setText("23A");
        label_junction_23a.setOpaque(true);
        panelUtama.add(label_junction_23a, new org.netbeans.lib.awtextra.AbsoluteConstraints(1140, 460, 40, 10));

        label_junction_13a.setBackground(new java.awt.Color(102, 102, 102));
        label_junction_13a.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_junction_13a.setForeground(new java.awt.Color(255, 255, 255));
        label_junction_13a.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_junction_13a.setText("13A");
        label_junction_13a.setOpaque(true);
        panelUtama.add(label_junction_13a, new org.netbeans.lib.awtextra.AbsoluteConstraints(1140, 590, 40, 10));

        label_junction_13b.setBackground(new java.awt.Color(102, 102, 102));
        label_junction_13b.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_junction_13b.setForeground(new java.awt.Color(255, 255, 255));
        label_junction_13b.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_junction_13b.setText("13B");
        label_junction_13b.setOpaque(true);
        panelUtama.add(label_junction_13b, new org.netbeans.lib.awtextra.AbsoluteConstraints(1200, 570, 40, 10));

        label_junction_23b.setBackground(new java.awt.Color(102, 102, 102));
        label_junction_23b.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_junction_23b.setForeground(new java.awt.Color(255, 255, 255));
        label_junction_23b.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_junction_23b.setText("23B");
        label_junction_23b.setOpaque(true);
        panelUtama.add(label_junction_23b, new org.netbeans.lib.awtextra.AbsoluteConstraints(1290, 480, 40, 10));

        label_junction_24a1.setBackground(new java.awt.Color(102, 102, 102));
        label_junction_24a1.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_junction_24a1.setForeground(new java.awt.Color(255, 255, 255));
        label_junction_24a1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_junction_24a1.setText("24A1");
        label_junction_24a1.setOpaque(true);
        panelUtama.add(label_junction_24a1, new org.netbeans.lib.awtextra.AbsoluteConstraints(1510, 570, 40, 10));

        label_junction_24a2.setBackground(new java.awt.Color(102, 102, 102));
        label_junction_24a2.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_junction_24a2.setForeground(new java.awt.Color(255, 255, 255));
        label_junction_24a2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_junction_24a2.setText("24A2");
        label_junction_24a2.setOpaque(true);
        panelUtama.add(label_junction_24a2, new org.netbeans.lib.awtextra.AbsoluteConstraints(1590, 570, 40, 10));

        label_junction_14.setBackground(new java.awt.Color(102, 102, 102));
        label_junction_14.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_junction_14.setForeground(new java.awt.Color(255, 255, 255));
        label_junction_14.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_junction_14.setText("14");
        label_junction_14.setOpaque(true);
        panelUtama.add(label_junction_14, new org.netbeans.lib.awtextra.AbsoluteConstraints(1680, 570, 40, 10));

        label_signal_j82.setBackground(new java.awt.Color(153, 153, 153));
        label_signal_j82.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_signal_j82.setForeground(new java.awt.Color(255, 255, 255));
        label_signal_j82.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_signal_j82.setText("J82");
        label_signal_j82.setOpaque(true);
        panelUtama.add(label_signal_j82, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 230, 40, 10));

        label_signal_j24.setBackground(new java.awt.Color(153, 153, 153));
        label_signal_j24.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_signal_j24.setForeground(new java.awt.Color(255, 255, 255));
        label_signal_j24.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_signal_j24.setText("J24");
        label_signal_j24.setOpaque(true);
        panelUtama.add(label_signal_j24, new org.netbeans.lib.awtextra.AbsoluteConstraints(1780, 460, 40, 10));

        label_signal_b201.setBackground(new java.awt.Color(153, 153, 153));
        label_signal_b201.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_signal_b201.setForeground(new java.awt.Color(255, 255, 255));
        label_signal_b201.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_signal_b201.setText("B201");
        label_signal_b201.setOpaque(true);
        panelUtama.add(label_signal_b201, new org.netbeans.lib.awtextra.AbsoluteConstraints(2000, 460, 40, 10));

        signal_j22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokMerahKiri.png"))); // NOI18N
        signal_j22.setToolTipText("Klik untuk membentuk rute");
        signal_j22.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        signal_j22.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                signal_j22MouseClicked(evt);
            }
        });
        panelUtama.add(signal_j22, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 470, 60, 20));

        signal_j12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokMerahKanan.png"))); // NOI18N
        signal_j12.setToolTipText("Klik untuk membentuk rute");
        signal_j12.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        signal_j12.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                signal_j12MouseClicked(evt);
            }
        });
        panelUtama.add(signal_j12, new org.netbeans.lib.awtextra.AbsoluteConstraints(910, 570, 60, 20));

        label_signal_j12.setBackground(new java.awt.Color(153, 153, 153));
        label_signal_j12.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_signal_j12.setForeground(new java.awt.Color(255, 255, 255));
        label_signal_j12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_signal_j12.setText("J12");
        label_signal_j12.setOpaque(true);
        panelUtama.add(label_signal_j12, new org.netbeans.lib.awtextra.AbsoluteConstraints(920, 590, 40, 10));

        label_signal_j62.setBackground(new java.awt.Color(153, 153, 153));
        label_signal_j62.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_signal_j62.setForeground(new java.awt.Color(255, 255, 255));
        label_signal_j62.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_signal_j62.setText("J62");
        label_signal_j62.setOpaque(true);
        panelUtama.add(label_signal_j62, new org.netbeans.lib.awtextra.AbsoluteConstraints(920, 360, 40, 10));

        label_cuk.setBackground(new java.awt.Color(102, 102, 102));
        label_cuk.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_cuk.setForeground(new java.awt.Color(255, 255, 255));
        label_cuk.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_cuk.setText("Cakung (CUK)");
        label_cuk.setOpaque(true);
        panelUtama.add(label_cuk, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 680, 120, -1));

        panelBawah.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        panelBawah.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        table_timetable.setBackground(new java.awt.Color(204, 204, 204));
        table_timetable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Nomor KA", "Nama KA", "Dari", "Tujuan", "Datang", "Berangkat", "Informasi", "Status Waktu"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        table_timetable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        table_timetable.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(table_timetable);
        if (table_timetable.getColumnModel().getColumnCount() > 0) {
            table_timetable.getColumnModel().getColumn(0).setPreferredWidth(7);
            table_timetable.getColumnModel().getColumn(1).setPreferredWidth(40);
            table_timetable.getColumnModel().getColumn(2).setPreferredWidth(10);
            table_timetable.getColumnModel().getColumn(3).setPreferredWidth(10);
            table_timetable.getColumnModel().getColumn(4).setPreferredWidth(10);
            table_timetable.getColumnModel().getColumn(5).setPreferredWidth(10);
            table_timetable.getColumnModel().getColumn(6).setPreferredWidth(80);
            table_timetable.getColumnModel().getColumn(7).setPreferredWidth(80);
        }

        jLabel1.setFont(new java.awt.Font("Tahoma", 2, 14)); // NOI18N
        jLabel1.setText("Jadwal Kereta");

        table_train_status.setBackground(new java.awt.Color(153, 153, 255));
        table_train_status.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Nomor KA", "Nama KA", "Tujuan", "Peron Masuk", "SF", "Posisi", "Pergerakan"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, true, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        table_train_status.setGridColor(new java.awt.Color(0, 102, 0));
        table_train_status.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        table_train_status.getTableHeader().setReorderingAllowed(false);
        table_train_status.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                table_train_statusPropertyChange(evt);
            }
        });
        table_train_status.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                table_train_statusKeyPressed(evt);
            }
        });
        jScrollPane3.setViewportView(table_train_status);
        if (table_train_status.getColumnModel().getColumnCount() > 0) {
            table_train_status.getColumnModel().getColumn(0).setPreferredWidth(35);
            table_train_status.getColumnModel().getColumn(1).setPreferredWidth(50);
            table_train_status.getColumnModel().getColumn(2).setPreferredWidth(35);
            table_train_status.getColumnModel().getColumn(3).setPreferredWidth(30);
            table_train_status.getColumnModel().getColumn(4).setPreferredWidth(30);
            table_train_status.getColumnModel().getColumn(5).setPreferredWidth(30);
        }

        jLabel4.setFont(new java.awt.Font("Tahoma", 2, 14)); // NOI18N
        jLabel4.setText("Status Kereta");

        jSeparator2.setBackground(new java.awt.Color(51, 51, 51));
        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator2.setOpaque(true);

        javax.swing.GroupLayout pbJadwalStatusLayout = new javax.swing.GroupLayout(pbJadwalStatus);
        pbJadwalStatus.setLayout(pbJadwalStatusLayout);
        pbJadwalStatusLayout.setHorizontalGroup(
            pbJadwalStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pbJadwalStatusLayout.createSequentialGroup()
                .addGroup(pbJadwalStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pbJadwalStatusLayout.createSequentialGroup()
                        .addGap(247, 247, 247)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pbJadwalStatusLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 615, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(34, 34, 34)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                .addGroup(pbJadwalStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pbJadwalStatusLayout.createSequentialGroup()
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(233, 233, 233))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pbJadwalStatusLayout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 605, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        pbJadwalStatusLayout.setVerticalGroup(
            pbJadwalStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pbJadwalStatusLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pbJadwalStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pbJadwalStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel4))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pbJadwalStatusLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        panelBawah.addTab("Jadwal dan Status Kereta", pbJadwalStatus);

        info_input_number.setBackground(new java.awt.Color(204, 204, 204));
        info_input_number.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        info_input_number.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        info_input_number.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                info_input_numberActionPerformed(evt);
            }
        });
        info_input_number.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                info_input_numberKeyPressed(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(102, 102, 102));
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setText("Masukan nomor Kereta");

        jSeparator4.setBackground(new java.awt.Color(153, 153, 153));
        jSeparator4.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator4.setOpaque(true);

        jPanel3.setBackground(new java.awt.Color(0, 102, 102));

        panel_information.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(102, 102, 102));
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel13.setText("Nomor KA :");

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(102, 102, 102));
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel14.setText("Nama KA :");

        info_number.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        info_number.setForeground(new java.awt.Color(102, 102, 102));
        info_number.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        info_name.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        info_name.setForeground(new java.awt.Color(102, 102, 102));
        info_name.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jLabel17.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(102, 102, 102));
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel17.setText("Jenis KA :");

        info_type.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        info_type.setForeground(new java.awt.Color(102, 102, 102));
        info_type.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jLabel19.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(102, 102, 102));
        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel19.setText("Nama Masinis :");

        info_driver.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        info_driver.setForeground(new java.awt.Color(102, 102, 102));
        info_driver.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jLabel21.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(102, 102, 102));
        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel21.setText("Nama Ass Masinis :");

        info_driver_assistant.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        info_driver_assistant.setForeground(new java.awt.Color(102, 102, 102));
        info_driver_assistant.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jLabel23.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(102, 102, 102));
        jLabel23.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel23.setText("Stanformasi Unit :");

        info_sf.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        info_sf.setForeground(new java.awt.Color(102, 102, 102));
        info_sf.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jLabel25.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel25.setForeground(new java.awt.Color(51, 51, 51));
        jLabel25.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel25.setText("Stasiun Pemberangkatan :");

        info_from.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        info_from.setForeground(new java.awt.Color(51, 51, 51));
        info_from.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jLabel27.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel27.setForeground(new java.awt.Color(51, 51, 51));
        jLabel27.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel27.setText("Stasiun Tujuan :");

        info_destination.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        info_destination.setForeground(new java.awt.Color(51, 51, 51));
        info_destination.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jLabel29.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel29.setForeground(new java.awt.Color(51, 51, 51));
        jLabel29.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel29.setText("Datang Cakung :");

        info_eta.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        info_eta.setForeground(new java.awt.Color(51, 51, 51));
        info_eta.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jLabel31.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel31.setForeground(new java.awt.Color(51, 51, 51));
        jLabel31.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel31.setText("Berangkat Cakung :");

        info_etd.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        info_etd.setForeground(new java.awt.Color(51, 51, 51));
        info_etd.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jLabel33.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel33.setForeground(new java.awt.Color(51, 51, 51));
        jLabel33.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel33.setText("Peron Masuk :");

        info_platform.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        info_platform.setForeground(new java.awt.Color(51, 51, 51));
        info_platform.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jLabel35.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel35.setForeground(new java.awt.Color(51, 51, 51));
        jLabel35.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel35.setText("Status Ketepatan Waktu :");

        info_delay.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        info_delay.setForeground(new java.awt.Color(51, 51, 51));
        info_delay.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        javax.swing.GroupLayout panel_info_general_detailLayout = new javax.swing.GroupLayout(panel_info_general_detail);
        panel_info_general_detail.setLayout(panel_info_general_detailLayout);
        panel_info_general_detailLayout.setHorizontalGroup(
            panel_info_general_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_info_general_detailLayout.createSequentialGroup()
                .addGroup(panel_info_general_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(panel_info_general_detailLayout.createSequentialGroup()
                        .addGap(86, 86, 86)
                        .addGroup(panel_info_general_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(panel_info_general_detailLayout.createSequentialGroup()
                                .addComponent(jLabel13)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(info_number, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(panel_info_general_detailLayout.createSequentialGroup()
                                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(info_name, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(panel_info_general_detailLayout.createSequentialGroup()
                                .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(info_type, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(panel_info_general_detailLayout.createSequentialGroup()
                        .addGap(43, 43, 43)
                        .addGroup(panel_info_general_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel21)
                            .addComponent(jLabel19)
                            .addComponent(jLabel23))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(panel_info_general_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(info_sf, javax.swing.GroupLayout.DEFAULT_SIZE, 145, Short.MAX_VALUE)
                            .addComponent(info_driver_assistant, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(info_driver, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(16, 16, 16)
                .addGroup(panel_info_general_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel27)
                    .addComponent(jLabel25)
                    .addComponent(jLabel29)
                    .addComponent(jLabel31)
                    .addComponent(jLabel33)
                    .addComponent(jLabel35))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel_info_general_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(info_from, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(info_destination, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(info_eta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(info_etd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(info_platform, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(info_delay, javax.swing.GroupLayout.DEFAULT_SIZE, 146, Short.MAX_VALUE))
                .addContainerGap())
        );
        panel_info_general_detailLayout.setVerticalGroup(
            panel_info_general_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_info_general_detailLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_info_general_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(info_number, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(info_from, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel_info_general_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel13)
                        .addComponent(jLabel25)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel_info_general_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(info_name, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(info_destination, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel_info_general_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel14)
                        .addComponent(jLabel27)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel_info_general_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(info_type, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(info_eta, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel_info_general_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel29)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel_info_general_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(info_driver, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(info_etd, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel_info_general_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel31)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel_info_general_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(info_driver_assistant, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(info_platform, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel_info_general_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel33)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel_info_general_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(info_sf, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel35)
                    .addComponent(info_delay, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(34, Short.MAX_VALUE))
        );

        panel_information.addTab("Detail Umum", panel_info_general_detail);

        table_sf.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Nomor Rangkaian", "No.Kereta", "Tipe"
            }
        ));
        jScrollPane1.setViewportView(table_sf);

        javax.swing.GroupLayout panel_info_sfLayout = new javax.swing.GroupLayout(panel_info_sf);
        panel_info_sf.setLayout(panel_info_sfLayout);
        panel_info_sfLayout.setHorizontalGroup(
            panel_info_sfLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_info_sfLayout.createSequentialGroup()
                .addGap(79, 79, 79)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 488, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(79, Short.MAX_VALUE))
        );
        panel_info_sfLayout.setVerticalGroup(
            panel_info_sfLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_info_sfLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE)
                .addContainerGap())
        );

        panel_information.addTab("Formasi Rangkaian", panel_info_sf);

        info_info.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        info_info.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        info_info.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        javax.swing.GroupLayout panel_info_infoLayout = new javax.swing.GroupLayout(panel_info_info);
        panel_info_info.setLayout(panel_info_infoLayout);
        panel_info_infoLayout.setHorizontalGroup(
            panel_info_infoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_info_infoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(info_info, javax.swing.GroupLayout.DEFAULT_SIZE, 626, Short.MAX_VALUE)
                .addContainerGap())
        );
        panel_info_infoLayout.setVerticalGroup(
            panel_info_infoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_info_infoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(info_info, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(91, Short.MAX_VALUE))
        );

        panel_information.addTab("Informasi Kereta", panel_info_info);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(236, Short.MAX_VALUE)
                .addComponent(panel_information, javax.swing.GroupLayout.PREFERRED_SIZE, 655, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(96, 96, 96))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panel_information, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        javax.swing.GroupLayout pbInfoLayout = new javax.swing.GroupLayout(pbInfo);
        pbInfo.setLayout(pbInfoLayout);
        pbInfoLayout.setHorizontalGroup(
            pbInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pbInfoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pbInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(info_input_number, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pbInfoLayout.setVerticalGroup(
            pbInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pbInfoLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(info_input_number, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel12)
                .addGap(79, 79, 79))
            .addComponent(jSeparator4)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        panelBawah.addTab("Informasi Kereta", pbInfo);

        jSeparator5.setBackground(new java.awt.Color(51, 51, 51));
        jSeparator5.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator5.setOpaque(true);

        table_log.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null}
            },
            new String [] {
                "Jam", "Log"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane4.setViewportView(table_log);
        if (table_log.getColumnModel().getColumnCount() > 0) {
            table_log.getColumnModel().getColumn(0).setPreferredWidth(10);
            table_log.getColumnModel().getColumn(1).setPreferredWidth(400);
        }

        table_note.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null}
            },
            new String [] {
                "Jam", "Catatan"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane5.setViewportView(table_note);
        if (table_note.getColumnModel().getColumnCount() > 0) {
            table_note.getColumnModel().getColumn(0).setPreferredWidth(10);
            table_note.getColumnModel().getColumn(1).setPreferredWidth(400);
        }

        txtCatatan.setBackground(new java.awt.Color(204, 204, 204));
        txtCatatan.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txtCatatan.setText("Tulis disini , tekan enter untuk submit catatan , tulis clear dan tekan enter untuk hapus catatan");
        txtCatatan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtCatatanMouseClicked(evt);
            }
        });
        txtCatatan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCatatanKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 580, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane5)
                    .addComponent(txtCatatan, javax.swing.GroupLayout.DEFAULT_SIZE, 550, Short.MAX_VALUE))
                .addGap(69, 69, 69))
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel5Layout.createSequentialGroup()
                    .addGap(596, 596, 596)
                    .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(710, Short.MAX_VALUE)))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtCatatan)))
                .addContainerGap(27, Short.MAX_VALUE))
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel5Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(32, Short.MAX_VALUE)))
        );

        panelBawah.addTab("Log & Catatan", jPanel5);

        jPanel6.setBackground(new java.awt.Color(0, 102, 102));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Status JPL :");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("Status Gangguan :");

        ST_jpl.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        ST_jpl.setForeground(new java.awt.Color(102, 255, 255));
        ST_jpl.setText("Terbuka");

        ST_gangguan.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        ST_gangguan.setForeground(new java.awt.Color(102, 255, 255));
        ST_gangguan.setText("Tidak ada Gangguan");

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel18.setText("Gangguan Komponen :");

        ST_gangguan_komponen1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        ST_gangguan_komponen1.setForeground(new java.awt.Color(255, 51, 51));

        ST_gangguan_komponen2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        ST_gangguan_komponen2.setForeground(new java.awt.Color(255, 51, 51));

        ST_gangguan_komponen3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        ST_gangguan_komponen3.setForeground(new java.awt.Color(255, 51, 51));

        ST_repair.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        ST_repair.setText("Reparasi Komponen");
        ST_repair.setEnabled(false);
        ST_repair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ST_repairActionPerformed(evt);
            }
        });

        ST_info.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(ST_repair, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel18, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(ST_gangguan_komponen1, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
                                    .addComponent(ST_gangguan_komponen2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(ST_gangguan_komponen3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap())
                            .addComponent(ST_info, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addGap(26, 26, 26)
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(ST_gangguan))
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addGap(67, 67, 67)
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(ST_jpl)))
                        .addContainerGap(199, Short.MAX_VALUE))))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(ST_jpl))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(ST_gangguan))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel18)
                    .addComponent(ST_gangguan_komponen1, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ST_gangguan_komponen2, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(ST_gangguan_komponen3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 35, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ST_repair, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ST_info, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout pb_statusLayout = new javax.swing.GroupLayout(pb_status);
        pb_status.setLayout(pb_statusLayout);
        pb_statusLayout.setHorizontalGroup(
            pb_statusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pb_statusLayout.createSequentialGroup()
                .addGap(419, 419, 419)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(388, Short.MAX_VALUE))
        );
        pb_statusLayout.setVerticalGroup(
            pb_statusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pb_statusLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        panelBawah.addTab("Status", pb_status);

        panelUtama.add(panelBawah, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 710, -1, -1));

        label_bua.setBackground(new java.awt.Color(102, 102, 102));
        label_bua.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_bua.setForeground(new java.awt.Color(255, 255, 255));
        label_bua.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_bua.setText("<- Buaran (BUA)");
        label_bua.setOpaque(true);
        panelUtama.add(label_bua, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 440, 90, -1));

        label_kldb.setBackground(new java.awt.Color(102, 102, 102));
        label_kldb.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_kldb.setForeground(new java.awt.Color(255, 255, 255));
        label_kldb.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_kldb.setText("Klender Baru (KLDB)");
        label_kldb.setOpaque(true);
        panelUtama.add(label_kldb, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 610, 120, -1));

        signal_b101.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokKuningKanan.png"))); // NOI18N
        signal_b101.setToolTipText("Sinyal Blok");
        signal_b101.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        panelUtama.add(signal_b101, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 570, 60, 20));

        speed_signal_j10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/taspat30OffKanan.png"))); // NOI18N
        speed_signal_j10.setToolTipText("");
        speed_signal_j10.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        panelUtama.add(speed_signal_j10, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 570, -1, 20));

        panel_master.setBackground(new java.awt.Color(204, 204, 204));
        panel_master.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Master", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        label_panel_lock_unlock.setAlignment(java.awt.Label.CENTER);
        label_panel_lock_unlock.setText("Panel Lock / Unlock");

        btn_unlock_1.setBackground(new java.awt.Color(204, 204, 204));
        btn_unlock_1.setLabel("Tombol 1");
        btn_unlock_1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_unlock_1ActionPerformed(evt);
            }
        });

        btn_unlock_2.setBackground(new java.awt.Color(204, 204, 204));
        btn_unlock_2.setLabel("Tombol 2");
        btn_unlock_2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_unlock_2ActionPerformed(evt);
            }
        });

        btn_lock.setBackground(new java.awt.Color(204, 204, 204));
        btn_lock.setLabel("Kunci");
        btn_lock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_lockActionPerformed(evt);
            }
        });

        indicator_master.setEditable(false);
        indicator_master.setBackground(new java.awt.Color(255, 0, 0));
        indicator_master.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout panel_masterLayout = new javax.swing.GroupLayout(panel_master);
        panel_master.setLayout(panel_masterLayout);
        panel_masterLayout.setHorizontalGroup(
            panel_masterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(label_panel_lock_unlock, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(panel_masterLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_masterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(btn_lock, javax.swing.GroupLayout.DEFAULT_SIZE, 69, Short.MAX_VALUE)
                    .addComponent(btn_unlock_2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 69, Short.MAX_VALUE)
                    .addComponent(btn_unlock_1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(indicator_master, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(19, Short.MAX_VALUE))
        );
        panel_masterLayout.setVerticalGroup(
            panel_masterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_masterLayout.createSequentialGroup()
                .addComponent(label_panel_lock_unlock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel_masterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(indicator_master)
                    .addComponent(btn_unlock_1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_unlock_2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_lock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(15, Short.MAX_VALUE))
        );

        panelUtama.add(panel_master, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 150, 160));

        panel_indicator.setBackground(new java.awt.Color(204, 204, 204));
        panel_indicator.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Indikator", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        indicator_arrival_jng.setEditable(false);
        indicator_arrival_jng.setBackground(new java.awt.Color(204, 204, 204));
        indicator_arrival_jng.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        indicator_arrival_jng.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        indicator_arrival_jng.setText("JNG");
        indicator_arrival_jng.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        label_arrival_indicator.setAlignment(java.awt.Label.CENTER);
        label_arrival_indicator.setText("Indikator Kedatangan");

        indicator_arrival_bks.setEditable(false);
        indicator_arrival_bks.setBackground(new java.awt.Color(204, 204, 204));
        indicator_arrival_bks.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        indicator_arrival_bks.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        indicator_arrival_bks.setText("BKS");
        indicator_arrival_bks.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        counter_arrival.setEditable(false);
        counter_arrival.setBackground(new java.awt.Color(204, 204, 204));
        counter_arrival.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        counter_arrival.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        counter_arrival.setText("0");
        counter_arrival.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        label_failure_indicator.setAlignment(java.awt.Label.CENTER);
        label_failure_indicator.setText("Indikator Gangguan");

        indicator_failure_signal.setEditable(false);
        indicator_failure_signal.setBackground(new java.awt.Color(204, 204, 204));
        indicator_failure_signal.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        indicator_failure_signal.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        indicator_failure_signal.setText("Sinyal");
        indicator_failure_signal.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        indicator_failure_junction.setEditable(false);
        indicator_failure_junction.setBackground(new java.awt.Color(204, 204, 204));
        indicator_failure_junction.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        indicator_failure_junction.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        indicator_failure_junction.setText("Wesel");
        indicator_failure_junction.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        counter_failure.setEditable(false);
        counter_failure.setBackground(new java.awt.Color(204, 204, 204));
        counter_failure.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        counter_failure.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        counter_failure.setText("0");
        counter_failure.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        btn_reset_indicator.setBackground(new java.awt.Color(204, 204, 204));
        btn_reset_indicator.setLabel("Reset");
        btn_reset_indicator.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_reset_indicatorActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panel_indicatorLayout = new javax.swing.GroupLayout(panel_indicator);
        panel_indicator.setLayout(panel_indicatorLayout);
        panel_indicatorLayout.setHorizontalGroup(
            panel_indicatorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_indicatorLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_indicatorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(indicator_arrival_bks)
                    .addComponent(indicator_arrival_jng)
                    .addComponent(label_arrival_indicator, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(counter_arrival))
                .addGap(18, 18, 18)
                .addGroup(panel_indicatorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(indicator_failure_junction)
                    .addComponent(indicator_failure_signal)
                    .addComponent(label_failure_indicator, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(counter_failure, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addComponent(btn_reset_indicator, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(18, Short.MAX_VALUE))
        );
        panel_indicatorLayout.setVerticalGroup(
            panel_indicatorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_indicatorLayout.createSequentialGroup()
                .addGroup(panel_indicatorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_indicatorLayout.createSequentialGroup()
                        .addComponent(btn_reset_indicator, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(indicator_failure_junction, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(counter_failure, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_indicatorLayout.createSequentialGroup()
                        .addGroup(panel_indicatorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(label_arrival_indicator, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(label_failure_indicator, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panel_indicatorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(indicator_arrival_jng, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(indicator_failure_signal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(indicator_arrival_bks, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(counter_arrival, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 14, Short.MAX_VALUE))
        );

        panelUtama.add(panel_indicator, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 10, 410, 160));

        panel_notification.setBackground(new java.awt.Color(204, 204, 204));
        panel_notification.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Berita", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        label_send_notification.setAlignment(java.awt.Label.CENTER);
        label_send_notification.setText("Kirim Berita Berangkat");

        btn_notification_jng.setBackground(new java.awt.Color(204, 204, 204));
        btn_notification_jng.setLabel("JNG");
        btn_notification_jng.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_notification_jngActionPerformed(evt);
            }
        });

        btn_notification_bks.setBackground(new java.awt.Color(204, 204, 204));
        btn_notification_bks.setLabel("BKS");
        btn_notification_bks.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_notification_bksActionPerformed(evt);
            }
        });

        indicator_notification_jng.setEditable(false);
        indicator_notification_jng.setBackground(new java.awt.Color(255, 0, 0));
        indicator_notification_jng.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        indicator_notification_bks.setEditable(false);
        indicator_notification_bks.setBackground(new java.awt.Color(255, 0, 0));
        indicator_notification_bks.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        label_send_notification1.setAlignment(java.awt.Label.CENTER);
        label_send_notification1.setText("Berita Keberangkatan");

        label_notification_arrival_jng.setAlignment(java.awt.Label.CENTER);
        label_notification_arrival_jng.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        label_notification_arrival_jng.setText("JNG");

        label_notification_arrival_bks.setAlignment(java.awt.Label.CENTER);
        label_notification_arrival_bks.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        label_notification_arrival_bks.setText("BKS");

        indicator_notification_arrival_bks.setEditable(false);
        indicator_notification_arrival_bks.setBackground(new java.awt.Color(204, 204, 204));
        indicator_notification_arrival_bks.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        indicator_notification_arrival_jng.setEditable(false);
        indicator_notification_arrival_jng.setBackground(new java.awt.Color(204, 204, 204));
        indicator_notification_arrival_jng.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout panel_notificationLayout = new javax.swing.GroupLayout(panel_notification);
        panel_notification.setLayout(panel_notificationLayout);
        panel_notificationLayout.setHorizontalGroup(
            panel_notificationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_notificationLayout.createSequentialGroup()
                .addGroup(panel_notificationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_notificationLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panel_notificationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(btn_notification_bks, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 69, Short.MAX_VALUE)
                            .addComponent(btn_notification_jng, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panel_notificationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(indicator_notification_jng, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(indicator_notification_bks, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(label_send_notification, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                .addGroup(panel_notificationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(label_send_notification1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel_notificationLayout.createSequentialGroup()
                        .addComponent(label_notification_arrival_jng, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(indicator_notification_arrival_jng, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel_notificationLayout.createSequentialGroup()
                        .addComponent(label_notification_arrival_bks, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(indicator_notification_arrival_bks, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        panel_notificationLayout.setVerticalGroup(
            panel_notificationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_notificationLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(panel_notificationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(label_send_notification, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(label_send_notification1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(panel_notificationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_notificationLayout.createSequentialGroup()
                        .addGroup(panel_notificationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(label_notification_arrival_jng, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(indicator_notification_arrival_jng, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(6, 6, 6)
                        .addGroup(panel_notificationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(label_notification_arrival_bks, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(indicator_notification_arrival_bks, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_notificationLayout.createSequentialGroup()
                        .addGroup(panel_notificationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(indicator_notification_jng)
                            .addComponent(btn_notification_jng, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panel_notificationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btn_notification_bks, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(indicator_notification_bks))))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        panelUtama.add(panel_notification, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 10, 320, 140));

        panel_junction.setBackground(new java.awt.Color(204, 204, 204));
        panel_junction.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Wesel", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        counter_lock_all_junction.setEditable(false);
        counter_lock_all_junction.setBackground(new java.awt.Color(204, 204, 204));
        counter_lock_all_junction.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        counter_lock_all_junction.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        counter_lock_all_junction.setText("0");
        counter_lock_all_junction.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        btn_lock_all_junction.setBackground(new java.awt.Color(204, 204, 204));
        btn_lock_all_junction.setLabel("Kunci semua wesel");
        btn_lock_all_junction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_lock_all_junctionActionPerformed(evt);
            }
        });

        btn_remove_obstacle.setBackground(new java.awt.Color(204, 204, 204));
        btn_remove_obstacle.setLabel("Hapus sekat");
        btn_remove_obstacle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_remove_obstacleActionPerformed(evt);
            }
        });

        counter_remove_obstacle.setEditable(false);
        counter_remove_obstacle.setBackground(new java.awt.Color(204, 204, 204));
        counter_remove_obstacle.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        counter_remove_obstacle.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        counter_remove_obstacle.setText("0");
        counter_remove_obstacle.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        counter_unlock_all_junction.setEditable(false);
        counter_unlock_all_junction.setBackground(new java.awt.Color(204, 204, 204));
        counter_unlock_all_junction.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        counter_unlock_all_junction.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        counter_unlock_all_junction.setText("0");
        counter_unlock_all_junction.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        btn_reposition_junction.setBackground(new java.awt.Color(204, 204, 204));
        btn_reposition_junction.setLabel("Reposisi wesel");
        btn_reposition_junction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_reposition_junctionActionPerformed(evt);
            }
        });

        counter_reposition_junction.setEditable(false);
        counter_reposition_junction.setBackground(new java.awt.Color(204, 204, 204));
        counter_reposition_junction.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        counter_reposition_junction.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        counter_reposition_junction.setText("0");
        counter_reposition_junction.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        counter_lock_junction.setEditable(false);
        counter_lock_junction.setBackground(new java.awt.Color(204, 204, 204));
        counter_lock_junction.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        counter_lock_junction.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        counter_lock_junction.setText("0");
        counter_lock_junction.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        counter_unlock_junction.setEditable(false);
        counter_unlock_junction.setBackground(new java.awt.Color(204, 204, 204));
        counter_unlock_junction.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        counter_unlock_junction.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        counter_unlock_junction.setText("0");
        counter_unlock_junction.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        btn_unlock_all_junction.setBackground(new java.awt.Color(204, 204, 204));
        btn_unlock_all_junction.setLabel("Buka semua wesel");
        btn_unlock_all_junction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_unlock_all_junctionActionPerformed(evt);
            }
        });

        btn_lock_junction.setBackground(new java.awt.Color(204, 204, 204));
        btn_lock_junction.setLabel("Kunci wesel");
        btn_lock_junction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_lock_junctionActionPerformed(evt);
            }
        });

        btn_unlock_junction.setBackground(new java.awt.Color(204, 204, 204));
        btn_unlock_junction.setLabel("Buka wesel");
        btn_unlock_junction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_unlock_junctionActionPerformed(evt);
            }
        });

        counter_junction_changed.setEditable(false);
        counter_junction_changed.setBackground(new java.awt.Color(204, 204, 204));
        counter_junction_changed.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        counter_junction_changed.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        counter_junction_changed.setText("0");
        counter_junction_changed.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        label_junction_changed1.setAlignment(java.awt.Label.CENTER);
        label_junction_changed1.setText("Wesel terubah");

        javax.swing.GroupLayout panel_junctionLayout = new javax.swing.GroupLayout(panel_junction);
        panel_junction.setLayout(panel_junctionLayout);
        panel_junctionLayout.setHorizontalGroup(
            panel_junctionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_junctionLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_junctionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_junctionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(btn_lock_all_junction, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(counter_lock_all_junction))
                    .addGroup(panel_junctionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(btn_remove_obstacle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(counter_remove_obstacle, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(panel_junctionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_junctionLayout.createSequentialGroup()
                        .addComponent(btn_unlock_all_junction, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_lock_junction, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel_junctionLayout.createSequentialGroup()
                        .addGroup(panel_junctionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panel_junctionLayout.createSequentialGroup()
                                .addComponent(counter_unlock_all_junction, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(counter_lock_junction, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panel_junctionLayout.createSequentialGroup()
                                .addGroup(panel_junctionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(btn_reposition_junction, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(counter_reposition_junction, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addComponent(counter_junction_changed, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(18, 18, 18)
                .addGroup(panel_junctionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_unlock_junction, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(counter_unlock_junction, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(14, Short.MAX_VALUE))
            .addGroup(panel_junctionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panel_junctionLayout.createSequentialGroup()
                    .addGap(306, 306, 306)
                    .addComponent(label_junction_changed1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(147, 147, 147)))
        );
        panel_junctionLayout.setVerticalGroup(
            panel_junctionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_junctionLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(panel_junctionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btn_unlock_all_junction, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_lock_junction, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_unlock_junction, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_lock_all_junction, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel_junctionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_junctionLayout.createSequentialGroup()
                        .addGroup(panel_junctionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(counter_lock_junction, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(counter_unlock_junction, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(counter_unlock_all_junction, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(counter_lock_all_junction, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_reposition_junction, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(11, 11, 11)
                        .addGroup(panel_junctionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(counter_reposition_junction, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(counter_junction_changed, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_junctionLayout.createSequentialGroup()
                        .addComponent(btn_remove_obstacle, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(11, 11, 11)
                        .addComponent(counter_remove_obstacle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 32, Short.MAX_VALUE))
            .addGroup(panel_junctionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_junctionLayout.createSequentialGroup()
                    .addContainerGap(79, Short.MAX_VALUE)
                    .addComponent(label_junction_changed1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(51, 51, 51)))
        );

        panelUtama.add(panel_junction, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 10, 590, -1));

        panel_route.setBackground(new java.awt.Color(204, 204, 204));
        panel_route.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Rute", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        label_created_route.setAlignment(java.awt.Label.CENTER);
        label_created_route.setText("Rute terbentuk");

        label_removed_route.setAlignment(java.awt.Label.CENTER);
        label_removed_route.setText("Rute terhapus");

        counter_created_route.setEditable(false);
        counter_created_route.setBackground(new java.awt.Color(204, 204, 204));
        counter_created_route.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        counter_created_route.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        counter_created_route.setText("0");
        counter_created_route.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        counter_removed_route.setEditable(false);
        counter_removed_route.setBackground(new java.awt.Color(204, 204, 204));
        counter_removed_route.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        counter_removed_route.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        counter_removed_route.setText("0");
        counter_removed_route.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout panel_routeLayout = new javax.swing.GroupLayout(panel_route);
        panel_route.setLayout(panel_routeLayout);
        panel_routeLayout.setHorizontalGroup(
            panel_routeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_routeLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_routeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_routeLayout.createSequentialGroup()
                        .addGroup(panel_routeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(label_created_route, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                            .addComponent(label_removed_route, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(counter_created_route))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(counter_removed_route))
                .addContainerGap())
        );
        panel_routeLayout.setVerticalGroup(
            panel_routeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_routeLayout.createSequentialGroup()
                .addComponent(label_created_route, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(counter_created_route, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(label_removed_route, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(counter_removed_route, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(29, Short.MAX_VALUE))
        );

        panelUtama.add(panel_route, new org.netbeans.lib.awtextra.AbsoluteConstraints(1560, 10, -1, -1));

        label_title.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        label_title.setForeground(new java.awt.Color(255, 255, 255));
        label_title.setText("STASIUN CAKUNG");
        panelUtama.add(label_title, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 200, -1, -1));

        btn_depart_platform_1.setBackground(new java.awt.Color(204, 204, 204));
        btn_depart_platform_1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_depart_platform_1.setEnabled(false);
        btn_depart_platform_1.setLabel("I");
        btn_depart_platform_1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_depart_platform_1ActionPerformed(evt);
            }
        });
        panelUtama.add(btn_depart_platform_1, new org.netbeans.lib.awtextra.AbsoluteConstraints(990, 670, 30, -1));

        btn_depart_platform_2.setBackground(new java.awt.Color(204, 204, 204));
        btn_depart_platform_2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_depart_platform_2.setEnabled(false);
        btn_depart_platform_2.setLabel("II");
        btn_depart_platform_2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_depart_platform_2ActionPerformed(evt);
            }
        });
        panelUtama.add(btn_depart_platform_2, new org.netbeans.lib.awtextra.AbsoluteConstraints(1030, 670, 30, -1));

        btn_depart_platform_3.setBackground(new java.awt.Color(204, 204, 204));
        btn_depart_platform_3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_depart_platform_3.setEnabled(false);
        btn_depart_platform_3.setLabel("III");
        btn_depart_platform_3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_depart_platform_3ActionPerformed(evt);
            }
        });
        panelUtama.add(btn_depart_platform_3, new org.netbeans.lib.awtextra.AbsoluteConstraints(1070, 670, 30, -1));

        btn_depart_platform_4.setBackground(new java.awt.Color(204, 204, 204));
        btn_depart_platform_4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_depart_platform_4.setEnabled(false);
        btn_depart_platform_4.setLabel("IV");
        btn_depart_platform_4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_depart_platform_4ActionPerformed(evt);
            }
        });
        panelUtama.add(btn_depart_platform_4, new org.netbeans.lib.awtextra.AbsoluteConstraints(1110, 670, 30, -1));

        btn_depart_platform_5.setBackground(new java.awt.Color(204, 204, 204));
        btn_depart_platform_5.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_depart_platform_5.setEnabled(false);
        btn_depart_platform_5.setLabel("V");
        btn_depart_platform_5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_depart_platform_5ActionPerformed(evt);
            }
        });
        panelUtama.add(btn_depart_platform_5, new org.netbeans.lib.awtextra.AbsoluteConstraints(1150, 670, 30, -1));

        btn_depart_platform_6.setBackground(new java.awt.Color(204, 204, 204));
        btn_depart_platform_6.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_depart_platform_6.setEnabled(false);
        btn_depart_platform_6.setLabel("VI");
        btn_depart_platform_6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_depart_platform_6ActionPerformed(evt);
            }
        });
        panelUtama.add(btn_depart_platform_6, new org.netbeans.lib.awtextra.AbsoluteConstraints(1190, 670, 30, -1));

        label_cuk7.setBackground(new java.awt.Color(102, 102, 102));
        label_cuk7.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_cuk7.setForeground(new java.awt.Color(255, 255, 255));
        label_cuk7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_cuk7.setText("Berangkatkan Kereta");
        label_cuk7.setOpaque(true);
        panelUtama.add(label_cuk7, new org.netbeans.lib.awtextra.AbsoluteConstraints(1040, 650, 130, -1));

        scroll_pane.setViewportView(panelUtama);

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelAtas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(scroll_pane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addComponent(panelAtas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scroll_pane, javax.swing.GroupLayout.DEFAULT_SIZE, 488, Short.MAX_VALUE))
        );

        mnPilihan.setText("Pilihan");

        mnPilihanReset.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.CTRL_MASK));
        mnPilihanReset.setText("Reset Game");
        mnPilihanReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnPilihanResetActionPerformed(evt);
            }
        });
        mnPilihan.add(mnPilihanReset);
        mnPilihan.add(jSeparator1);

        mnPilihanMainMenu.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_ESCAPE, 0));
        mnPilihanMainMenu.setText("Main Menu");
        mnPilihanMainMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnPilihanMainMenuActionPerformed(evt);
            }
        });
        mnPilihan.add(mnPilihanMainMenu);

        mnPilihanExit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        mnPilihanExit.setText("Keluar");
        mnPilihanExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnPilihanExitActionPerformed(evt);
            }
        });
        mnPilihan.add(mnPilihanExit);

        bar.add(mnPilihan);

        mnHelp.setText("Bantuan");

        mnOpenITD.setText("Cek Update ITD");
        mnOpenITD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnOpenITDActionPerformed(evt);
            }
        });
        mnHelp.add(mnOpenITD);

        mnOpenCTA.setText("Cek Update Layout Cakung");
        mnOpenCTA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnOpenCTAActionPerformed(evt);
            }
        });
        mnHelp.add(mnOpenCTA);

        bar.add(mnHelp);

        setJMenuBar(bar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(2, 2, 2))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mnPilihanExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnPilihanExitActionPerformed
        // TODO add your handling code here:
        if (JOptionPane.showConfirmDialog(null, "Anda akan keluar dari game ?", "Keluar dari game", 0) == 0){
            System.exit(0);
        }
    }//GEN-LAST:event_mnPilihanExitActionPerformed

    private void mnPilihanResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnPilihanResetActionPerformed
        if (is_simulation_running && JOptionPane.showConfirmDialog(null, "Anda yakin untuk restart game ? (Score dan progress anda telah tersimpan pada server)", "Konfirmasi", 0) == 0){
            closeSimulation();
            new ControlPanelCUK(utility).setVisible(true);
        }
    }//GEN-LAST:event_mnPilihanResetActionPerformed

    
    private void mnPilihanMainMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnPilihanMainMenuActionPerformed
        if (JOptionPane.showConfirmDialog(null, "Anda yakin untuk kembali ke Main Panel (score dan progress anda telah tersimpan pada database) ?", "Konfirmasi", 0) == 0){
            closeSimulation();
            indotraindisp.main.mainPanel main_panel = new indotraindisp.main.mainPanel();
            main_panel.importKoneksi(Koneksi.db);
            main_panel.importModelAndRun(utility);
            main_panel.setVisible(true);
        }
    }//GEN-LAST:event_mnPilihanMainMenuActionPerformed

    private void mnOpenITDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnOpenITDActionPerformed
        try{
            URI URI_URL_ITD = new URI(URL_ITD);
            java.awt.Desktop.getDesktop().browse(URI_URL_ITD);
        }catch(Exception ex){
            System.out.println(ex);
        }
    }//GEN-LAST:event_mnOpenITDActionPerformed

    private void mnOpenCTAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnOpenCTAActionPerformed
        try{
            URI URI_URL_CTA = new URI(URL_CTA);
            java.awt.Desktop.getDesktop().browse(URI_URL_CTA);
        }catch(Exception ex){
            System.out.println(ex);
        }
    }//GEN-LAST:event_mnOpenCTAActionPerformed

    private void ST_repairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ST_repairActionPerformed

    }//GEN-LAST:event_ST_repairActionPerformed

    private void txtCatatanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCatatanKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER){
            if (txtCatatan.getText().equalsIgnoreCase("clear")){
                information.clearNote();
                txtCatatan.setText("");
            }
            else{
                information.submitNote(txtCatatan.getText());
                txtCatatan.setText("");
            }
        }
    }//GEN-LAST:event_txtCatatanKeyPressed

    private void txtCatatanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtCatatanMouseClicked
        if (txtCatatan.getText().equals("Tulis disini , tekan enter untuk submit catatan , tulis clear dan tekan enter untuk hapus catatan"))txtCatatan.setText("");
    }//GEN-LAST:event_txtCatatanMouseClicked

    private void info_input_numberKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_info_input_numberKeyPressed
        information.getTrainDetail();
    }//GEN-LAST:event_info_input_numberKeyPressed

    private void info_input_numberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_info_input_numberActionPerformed

    }//GEN-LAST:event_info_input_numberActionPerformed

    private void junction_24a1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_junction_24a1MouseClicked
        mainControl.switchJunction(junction_24a1);
    }//GEN-LAST:event_junction_24a1MouseClicked

    private void junction_14MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_junction_14MouseClicked
        mainControl.switchJunction(junction_14);
    }//GEN-LAST:event_junction_14MouseClicked

    private void junction_11bMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_junction_11bMouseClicked
        mainControl.switchJunction(junction_11b);
    }//GEN-LAST:event_junction_11bMouseClicked

    private void junction_23aMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_junction_23aMouseClicked
        mainControl.switchJunction(junction_23a);
    }//GEN-LAST:event_junction_23aMouseClicked

    private void junction_23bMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_junction_23bMouseClicked
        mainControl.switchJunction(junction_23b);
    }//GEN-LAST:event_junction_23bMouseClicked

    private void junction_13bMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_junction_13bMouseClicked
        mainControl.switchJunction(junction_13b);
    }//GEN-LAST:event_junction_13bMouseClicked

    private void junction_11aMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_junction_11aMouseClicked
        mainControl.switchJunction(junction_11a);
    }//GEN-LAST:event_junction_11aMouseClicked

    private void junction_21bMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_junction_21bMouseClicked
        mainControl.switchJunction(junction_21b);
    }//GEN-LAST:event_junction_21bMouseClicked

    private void junction_24a2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_junction_24a2MouseClicked
        mainControl.switchJunction(junction_24a2);
    }//GEN-LAST:event_junction_24a2MouseClicked

    private void junction_24MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_junction_24MouseClicked
        mainControl.switchJunction(junction_24);
    }//GEN-LAST:event_junction_24MouseClicked

    private void junction_21aMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_junction_21aMouseClicked
        mainControl.switchJunction(junction_21a);
    }//GEN-LAST:event_junction_21aMouseClicked

    private void btn_bellMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_bellMouseClicked
        mainControl.bellOnOff();
    }//GEN-LAST:event_btn_bellMouseClicked

    private void junction_13aMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_junction_13aMouseClicked
        mainControl.switchJunction(junction_13a);
    }//GEN-LAST:event_junction_13aMouseClicked

    private void btn_lock_all_junctionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_lock_all_junctionActionPerformed
        audio.headpanelClick();
        headPanel.lockAllJunction();
    }//GEN-LAST:event_btn_lock_all_junctionActionPerformed

    private void c1ComponentAdded(java.awt.event.ContainerEvent evt) {//GEN-FIRST:event_c1ComponentAdded
        // TODO add your handling code here:
    }//GEN-LAST:event_c1ComponentAdded

    private void btn_unlock_1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_unlock_1ActionPerformed
        audio.headpanelClick();
        headPanel.unlockMaster(btn_unlock_1);
    }//GEN-LAST:event_btn_unlock_1ActionPerformed

    private void btn_unlock_2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_unlock_2ActionPerformed
        audio.headpanelClick();
        headPanel.unlockMaster(btn_unlock_2);
    }//GEN-LAST:event_btn_unlock_2ActionPerformed

    private void btn_lockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_lockActionPerformed
        audio.headpanelClick();
        headPanel.lockMaster();
    }//GEN-LAST:event_btn_lockActionPerformed

    private void btn_lock_junctionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_lock_junctionActionPerformed
        audio.headpanelClick();
        headPanel.lockJunction();
    }//GEN-LAST:event_btn_lock_junctionActionPerformed

    private void btn_unlock_junctionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_unlock_junctionActionPerformed
        audio.headpanelClick();
        headPanel.unlockJunction();
    }//GEN-LAST:event_btn_unlock_junctionActionPerformed

    private void btn_unlock_all_junctionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_unlock_all_junctionActionPerformed
        audio.headpanelClick();
        headPanel.unlockAllJunction();
    }//GEN-LAST:event_btn_unlock_all_junctionActionPerformed

    private void btn_reset_indicatorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_reset_indicatorActionPerformed
        audio.headpanelClick();
        headPanel.resetFailure();
    }//GEN-LAST:event_btn_reset_indicatorActionPerformed

    private void btn_remove_obstacleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_remove_obstacleActionPerformed
        audio.headpanelClick();
        headPanel.removeObstacle();
    }//GEN-LAST:event_btn_remove_obstacleActionPerformed

    private void btn_reposition_junctionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_reposition_junctionActionPerformed
        audio.headpanelClick();
        headPanel.repositionJunction();
    }//GEN-LAST:event_btn_reposition_junctionActionPerformed

    private void signal_j10MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_signal_j10MouseClicked
        mainControl.createRemoveRoute(signal_j10);
    }//GEN-LAST:event_signal_j10MouseClicked

    private void signal_j12MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_signal_j12MouseClicked
        mainControl.createRemoveRoute(signal_j12);
    }//GEN-LAST:event_signal_j12MouseClicked

    private void signal_j32MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_signal_j32MouseClicked
        mainControl.createRemoveRoute(signal_j32);
    }//GEN-LAST:event_signal_j32MouseClicked

    private void signal_j22MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_signal_j22MouseClicked
        mainControl.createRemoveRoute(signal_j22);
    }//GEN-LAST:event_signal_j22MouseClicked

    private void signal_j42MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_signal_j42MouseClicked
        mainControl.createRemoveRoute(signal_j42);
    }//GEN-LAST:event_signal_j42MouseClicked

    private void signal_j62MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_signal_j62MouseClicked
        mainControl.createRemoveRoute(signal_j62);
    }//GEN-LAST:event_signal_j62MouseClicked

    private void signal_j82MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_signal_j82MouseClicked
        mainControl.createRemoveRoute(signal_j82);
    }//GEN-LAST:event_signal_j82MouseClicked

    private void signal_j24MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_signal_j24MouseClicked
        mainControl.createRemoveRoute(signal_j24);
    }//GEN-LAST:event_signal_j24MouseClicked

    private void signal_j60MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_signal_j60MouseClicked
        mainControl.createRemoveRoute(signal_j60);
    }//GEN-LAST:event_signal_j60MouseClicked

    private void btn_notification_jngActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_notification_jngActionPerformed
        audio.headpanelClick();
        headPanel.notifyAdjacentStation("JNG");
    }//GEN-LAST:event_btn_notification_jngActionPerformed

    private void btn_notification_bksActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_notification_bksActionPerformed
        audio.headpanelClick();
        headPanel.notifyAdjacentStation("BKS");
    }//GEN-LAST:event_btn_notification_bksActionPerformed

    private void table_train_statusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_table_train_statusKeyPressed

    }//GEN-LAST:event_table_train_statusKeyPressed

    private void table_train_statusPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_table_train_statusPropertyChange
        // TODO add your handling code here:
    }//GEN-LAST:event_table_train_statusPropertyChange

    private void btn_depart_platform_1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_depart_platform_1ActionPerformed
        switcher.quickBgColor(btn_depart_platform_1, "green");
        mainControl.departingTrain(btn_depart_platform_1);
    }//GEN-LAST:event_btn_depart_platform_1ActionPerformed

    private void btn_depart_platform_2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_depart_platform_2ActionPerformed
        switcher.quickBgColor(btn_depart_platform_2, "green");
        mainControl.departingTrain(btn_depart_platform_2);
    }//GEN-LAST:event_btn_depart_platform_2ActionPerformed

    private void btn_depart_platform_3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_depart_platform_3ActionPerformed
        switcher.quickBgColor(btn_depart_platform_3, "green");
        mainControl.departingTrain(btn_depart_platform_3);
    }//GEN-LAST:event_btn_depart_platform_3ActionPerformed

    private void btn_depart_platform_4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_depart_platform_4ActionPerformed
        switcher.quickBgColor(btn_depart_platform_4, "green");
        mainControl.departingTrain(btn_depart_platform_4);
    }//GEN-LAST:event_btn_depart_platform_4ActionPerformed

    private void btn_depart_platform_5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_depart_platform_5ActionPerformed
        switcher.quickBgColor(btn_depart_platform_5, "green");
        mainControl.departingTrain(btn_depart_platform_5);
    }//GEN-LAST:event_btn_depart_platform_5ActionPerformed

    private void btn_depart_platform_6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_depart_platform_6ActionPerformed
        switcher.quickBgColor(btn_depart_platform_6, "green");
        mainControl.departingTrain(btn_depart_platform_6);
    }//GEN-LAST:event_btn_depart_platform_6ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ControlPanelCUK.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ControlPanelCUK.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ControlPanelCUK.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ControlPanelCUK.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
//                new ControlPanelCUK().setVisible(true);
            }
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel ST_gangguan;
    private javax.swing.JLabel ST_gangguan_komponen1;
    private javax.swing.JLabel ST_gangguan_komponen2;
    private javax.swing.JLabel ST_gangguan_komponen3;
    private javax.swing.JLabel ST_info;
    private javax.swing.JLabel ST_jpl;
    private javax.swing.JButton ST_repair;
    public javax.swing.JLabel a1;
    public javax.swing.JLabel a10;
    public javax.swing.JLabel a11;
    public javax.swing.JLabel a12;
    public javax.swing.JLabel a13;
    public javax.swing.JLabel a14;
    public javax.swing.JLabel a15;
    public javax.swing.JLabel a16;
    public javax.swing.JLabel a17;
    public javax.swing.JLabel a18;
    public javax.swing.JLabel a19;
    public javax.swing.JLabel a2;
    public javax.swing.JLabel a20;
    public javax.swing.JLabel a3;
    public javax.swing.JLabel a4;
    public javax.swing.JLabel a5;
    public javax.swing.JLabel a6;
    public javax.swing.JLabel a7;
    public javax.swing.JLabel a8;
    public javax.swing.JLabel a9;
    public javax.swing.JLabel ab1;
    public javax.swing.JLabel ab2;
    public javax.swing.JLabel ab3;
    public javax.swing.JLabel ab4;
    public javax.swing.JLabel ab5;
    public javax.swing.JLabel ab6;
    public javax.swing.JLabel ab7;
    public javax.swing.JLabel ab8;
    public javax.swing.JLabel b1;
    public javax.swing.JLabel b10;
    public javax.swing.JLabel b11;
    public javax.swing.JLabel b12;
    public javax.swing.JLabel b13;
    public javax.swing.JLabel b14;
    public javax.swing.JLabel b15;
    public javax.swing.JLabel b16;
    public javax.swing.JLabel b17;
    public javax.swing.JLabel b18;
    public javax.swing.JLabel b19;
    public javax.swing.JLabel b2;
    public javax.swing.JLabel b3;
    public javax.swing.JLabel b4;
    public javax.swing.JLabel b5;
    public javax.swing.JLabel b6;
    public javax.swing.JLabel b7;
    public javax.swing.JLabel b8;
    public javax.swing.JLabel b9;
    private javax.swing.JMenuBar bar;
    public javax.swing.JLabel bb1;
    public javax.swing.JLabel bb2;
    public javax.swing.JLabel bb3;
    public javax.swing.JLabel bb4;
    public javax.swing.JLabel bb5;
    public javax.swing.JLabel bb6;
    public javax.swing.JLabel bb7;
    public javax.swing.JLabel bb8;
    public javax.swing.JTextField block_010;
    public javax.swing.JTextField block_020;
    public javax.swing.JTextField block_030;
    public javax.swing.JTextField block_040;
    public javax.swing.JTextField block_050;
    public javax.swing.JTextField block_060;
    public javax.swing.JTextField block_101;
    public javax.swing.JTextField block_102;
    public javax.swing.JTextField block_103;
    public javax.swing.JTextField block_201;
    public javax.swing.JTextField block_202;
    public javax.swing.JTextField block_202b;
    public javax.swing.JTextField block_203;
    public javax.swing.JTextField block_301;
    public javax.swing.JTextField block_302;
    public javax.swing.JTextField block_401;
    public javax.swing.JTextField blok_129;
    public javax.swing.JLabel btn_bell;
    public java.awt.Button btn_depart_platform_1;
    public java.awt.Button btn_depart_platform_2;
    public java.awt.Button btn_depart_platform_3;
    public java.awt.Button btn_depart_platform_4;
    public java.awt.Button btn_depart_platform_5;
    public java.awt.Button btn_depart_platform_6;
    public java.awt.Button btn_lock;
    public java.awt.Button btn_lock_all_junction;
    public java.awt.Button btn_lock_junction;
    public java.awt.Button btn_notification_bks;
    public java.awt.Button btn_notification_jng;
    public java.awt.Button btn_remove_obstacle;
    public java.awt.Button btn_reposition_junction;
    public java.awt.Button btn_reset_indicator;
    public java.awt.Button btn_unlock_1;
    public java.awt.Button btn_unlock_2;
    public java.awt.Button btn_unlock_all_junction;
    public java.awt.Button btn_unlock_junction;
    public javax.swing.JLabel c1;
    public javax.swing.JLabel c10;
    public javax.swing.JLabel c11;
    public javax.swing.JLabel c12;
    public javax.swing.JLabel c13;
    public javax.swing.JLabel c14;
    public javax.swing.JLabel c15;
    public javax.swing.JLabel c16;
    public javax.swing.JLabel c17;
    public javax.swing.JLabel c18;
    public javax.swing.JLabel c2;
    public javax.swing.JLabel c3;
    public javax.swing.JLabel c4;
    public javax.swing.JLabel c5;
    public javax.swing.JLabel c6;
    public javax.swing.JLabel c7;
    public javax.swing.JLabel c8;
    public javax.swing.JLabel c9;
    public javax.swing.JTextField counter_arrival;
    public javax.swing.JTextField counter_created_route;
    public javax.swing.JTextField counter_failure;
    public javax.swing.JTextField counter_junction_changed;
    public javax.swing.JTextField counter_lock_all_junction;
    public javax.swing.JTextField counter_lock_junction;
    public javax.swing.JTextField counter_remove_obstacle;
    public javax.swing.JTextField counter_removed_route;
    public javax.swing.JTextField counter_reposition_junction;
    public javax.swing.JTextField counter_unlock_all_junction;
    public javax.swing.JTextField counter_unlock_junction;
    public javax.swing.JLabel d1;
    public javax.swing.JLabel d10;
    public javax.swing.JLabel d11;
    public javax.swing.JLabel d12;
    public javax.swing.JLabel d13;
    public javax.swing.JLabel d14;
    public javax.swing.JLabel d15;
    public javax.swing.JLabel d16;
    public javax.swing.JLabel d17;
    public javax.swing.JLabel d18;
    public javax.swing.JLabel d19;
    public javax.swing.JLabel d2;
    public javax.swing.JLabel d20;
    public javax.swing.JLabel d21;
    public javax.swing.JLabel d22;
    public javax.swing.JLabel d3;
    public javax.swing.JLabel d4;
    public javax.swing.JLabel d5;
    public javax.swing.JLabel d6;
    public javax.swing.JLabel d7;
    public javax.swing.JLabel d8;
    public javax.swing.JLabel d9;
    public javax.swing.JTextField indicator_arrival_bks;
    public javax.swing.JTextField indicator_arrival_jng;
    public javax.swing.JTextField indicator_failure_junction;
    public javax.swing.JTextField indicator_failure_signal;
    public javax.swing.JTextField indicator_master;
    public javax.swing.JTextField indicator_notification_arrival_bks;
    public javax.swing.JTextField indicator_notification_arrival_jng;
    public javax.swing.JTextField indicator_notification_bks;
    public javax.swing.JTextField indicator_notification_jng;
    public javax.swing.JLabel info_delay;
    public javax.swing.JLabel info_destination;
    public javax.swing.JLabel info_driver;
    public javax.swing.JLabel info_driver_assistant;
    public javax.swing.JLabel info_eta;
    public javax.swing.JLabel info_etd;
    public javax.swing.JLabel info_from;
    public javax.swing.JLabel info_info;
    public javax.swing.JTextField info_input_number;
    public javax.swing.JLabel info_name;
    public javax.swing.JLabel info_number;
    public javax.swing.JLabel info_platform;
    public javax.swing.JLabel info_sf;
    public javax.swing.JLabel info_type;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel29;
    public javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    public javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    public javax.swing.JLabel junction_11a;
    public javax.swing.JLabel junction_11b;
    public javax.swing.JLabel junction_13a;
    public javax.swing.JLabel junction_13b;
    public javax.swing.JLabel junction_14;
    public javax.swing.JLabel junction_21a;
    public javax.swing.JLabel junction_21b;
    public javax.swing.JLabel junction_23a;
    public javax.swing.JLabel junction_23b;
    public javax.swing.JLabel junction_24;
    public javax.swing.JLabel junction_24a1;
    public javax.swing.JLabel junction_24a2;
    public javax.swing.JLabel labelBellOnOff;
    public javax.swing.JLabel labelPlayTime;
    public javax.swing.JLabel labelPoint;
    public javax.swing.JLabel labelTime;
    public javax.swing.JLabel label_101;
    public javax.swing.JLabel label_129;
    public javax.swing.JLabel label_201;
    public javax.swing.JLabel label_202b;
    private java.awt.Label label_arrival_indicator;
    public javax.swing.JLabel label_bks;
    public javax.swing.JLabel label_block_010;
    public javax.swing.JLabel label_block_020;
    public javax.swing.JLabel label_block_030;
    public javax.swing.JLabel label_block_040;
    public javax.swing.JLabel label_block_050;
    public javax.swing.JLabel label_block_060;
    public javax.swing.JLabel label_block_102;
    public javax.swing.JLabel label_block_103;
    public javax.swing.JLabel label_block_202;
    public javax.swing.JLabel label_block_203;
    public javax.swing.JLabel label_block_301;
    public javax.swing.JLabel label_block_302;
    public javax.swing.JLabel label_block_401;
    public javax.swing.JLabel label_bua;
    public java.awt.Label label_created_route;
    public javax.swing.JLabel label_cuk;
    public javax.swing.JLabel label_cuk1;
    public javax.swing.JLabel label_cuk2;
    public javax.swing.JLabel label_cuk3;
    public javax.swing.JLabel label_cuk4;
    public javax.swing.JLabel label_cuk5;
    public javax.swing.JLabel label_cuk6;
    public javax.swing.JLabel label_cuk7;
    public javax.swing.JLabel label_elapsed;
    private java.awt.Label label_failure_indicator;
    public javax.swing.JLabel label_jng;
    public javax.swing.JLabel label_jng_2;
    public javax.swing.JLabel label_junction_11a;
    public javax.swing.JLabel label_junction_11b;
    public javax.swing.JLabel label_junction_13a;
    public javax.swing.JLabel label_junction_13b;
    public javax.swing.JLabel label_junction_14;
    public javax.swing.JLabel label_junction_21a;
    public javax.swing.JLabel label_junction_21b;
    public javax.swing.JLabel label_junction_23a;
    public javax.swing.JLabel label_junction_23b;
    public javax.swing.JLabel label_junction_24;
    public javax.swing.JLabel label_junction_24a1;
    public javax.swing.JLabel label_junction_24a2;
    public java.awt.Label label_junction_changed1;
    public javax.swing.JLabel label_kldb;
    public javax.swing.JLabel label_kldb1;
    public javax.swing.JLabel label_kldb2;
    public javax.swing.JLabel label_kri;
    public java.awt.Label label_notification_arrival_bks;
    public java.awt.Label label_notification_arrival_jng;
    private java.awt.Label label_panel_lock_unlock;
    public java.awt.Label label_removed_route;
    public javax.swing.JLabel label_score;
    private java.awt.Label label_send_notification;
    private java.awt.Label label_send_notification1;
    public javax.swing.JLabel label_signal_b101;
    public javax.swing.JLabel label_signal_b105;
    public javax.swing.JLabel label_signal_b201;
    public javax.swing.JLabel label_signal_b208;
    public javax.swing.JLabel label_signal_b209;
    public javax.swing.JLabel label_signal_b301;
    public javax.swing.JLabel label_signal_b406;
    public javax.swing.JLabel label_signal_j10;
    public javax.swing.JLabel label_signal_j12;
    public javax.swing.JLabel label_signal_j22;
    public javax.swing.JLabel label_signal_j24;
    public javax.swing.JLabel label_signal_j32;
    public javax.swing.JLabel label_signal_j42;
    public javax.swing.JLabel label_signal_j60;
    public javax.swing.JLabel label_signal_j62;
    public javax.swing.JLabel label_signal_j82;
    public javax.swing.JLabel label_signal_uj10;
    public javax.swing.JLabel label_time;
    public javax.swing.JLabel label_title;
    private javax.swing.JMenu mnHelp;
    private javax.swing.JMenuItem mnOpenCTA;
    private javax.swing.JMenuItem mnOpenITD;
    private javax.swing.JMenu mnPilihan;
    private javax.swing.JMenuItem mnPilihanExit;
    private javax.swing.JMenuItem mnPilihanMainMenu;
    private javax.swing.JMenuItem mnPilihanReset;
    private javax.swing.JPanel panelAtas;
    public javax.swing.JTabbedPane panelBawah;
    private javax.swing.JPanel panelUtama;
    public javax.swing.JPanel panel_indicator;
    private javax.swing.JPanel panel_info_general_detail;
    private javax.swing.JPanel panel_info_info;
    private javax.swing.JPanel panel_info_sf;
    private javax.swing.JTabbedPane panel_information;
    public javax.swing.JPanel panel_junction;
    public javax.swing.JPanel panel_master;
    public javax.swing.JPanel panel_notification;
    public javax.swing.JPanel panel_route;
    private javax.swing.JPanel pbInfo;
    private javax.swing.JPanel pbJadwalStatus;
    private javax.swing.JPanel pb_status;
    private javax.swing.JScrollPane scroll_pane;
    public javax.swing.JLabel signal_b101;
    public javax.swing.JLabel signal_b105;
    public javax.swing.JLabel signal_b201;
    public javax.swing.JLabel signal_b208;
    public javax.swing.JLabel signal_b209;
    public javax.swing.JLabel signal_b301;
    public javax.swing.JLabel signal_b406;
    public javax.swing.JLabel signal_j10;
    public javax.swing.JLabel signal_j12;
    public javax.swing.JLabel signal_j22;
    public javax.swing.JLabel signal_j24;
    public javax.swing.JLabel signal_j32;
    public javax.swing.JLabel signal_j42;
    public javax.swing.JLabel signal_j60;
    public javax.swing.JLabel signal_j62;
    public javax.swing.JLabel signal_j82;
    public javax.swing.JLabel signal_uj10;
    public javax.swing.JLabel sl_b102_w14;
    public javax.swing.JLabel speed_signal_j10;
    public javax.swing.JLabel speed_signal_j24;
    public javax.swing.JTable table_log;
    public javax.swing.JTable table_note;
    public javax.swing.JTable table_sf;
    public javax.swing.JTable table_timetable;
    public javax.swing.JTable table_train_status;
    private javax.swing.JTextField txtCatatan;
    public javax.swing.JLabel txt_notification;
    public javax.swing.JLabel x_13b_23b;
    public javax.swing.JLabel x_21a_11a;
    public javax.swing.JLabel x_24a2_14;
    // End of variables declaration//GEN-END:variables
}