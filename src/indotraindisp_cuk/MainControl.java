/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indotraindisp_cuk;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;

/**
 *
 * @author Tech Connexion Indonesia
 */
public class MainControl {
    ControlPanelCUK self = null;
    
    public MainControl(ControlPanelCUK parent){
        self = parent;
    }
    
    public void switchJunction(JLabel junction){
        if (self.is_master_on && self.is_segment_clickable.get(junction)){
            Boolean is_junction_changable = self.is_junction_has_obstacle.get(junction) == false && self.is_junction_need_reposition.get(junction) == false && self.is_junction_need_fixing.get(junction) == false && self.is_segment_reserved.get(junction) == false;
            if (!self.is_segment_on_failure.get(junction)){
                if (self.is_btn_lock_junction_pressed || self.is_btn_lock_all_junction_pressed){
                    self.is_junction_locked.put(junction,true);
                    JLabel label_of_junction = self.label_of_junction.get(junction);
                    self.switcher.changeBgColor(label_of_junction, "red");
                    if (!self.is_btn_lock_all_junction_pressed){
                        self.headPanel.lockJunction();
                        self.headPanel.increaseCounter(self.counter_lock_junction);
                        self.audio.alertGeneral();
                        String log_text = self.textParser.textJunctionHasBeenLocked(junction);
                        self.information.notifyAndLog(log_text);
                    }
                } else if (self.is_btn_unlock_junction_pressed || self.is_btn_unlock_all_junction_pressed){
                    self.is_junction_locked.put(junction,false);
                    JLabel label_of_junction = self.label_of_junction.get(junction);
                    self.switcher.changeBgColor(label_of_junction, "dark_gray");
                    if (!self.is_btn_unlock_all_junction_pressed){
                        self.headPanel.unlockJunction();
                        self.audio.alertGeneral();
                        self.headPanel.increaseCounter(self.counter_unlock_junction);
                        String log_text = self.textParser.textJunctionHasBeenUnlocked(junction);
                        self.information.notifyAndLog(log_text);
                    }
                } else if (self.is_junction_locked.get(junction) == false && is_junction_changable) {
                    self.switcher.switchJunction(junction);
                    self.audio.junctionClick();
                    self.headPanel.increaseCounter(self.counter_junction_changed);
                    String log_text = self.textParser.textJunctionChanged(junction);
                    self.information.log(log_text);
                } else if (self.is_junction_locked.get(junction)){
                    self.information.alertUnavailable("junction_is_locked");
                } else if (!is_junction_changable && self.is_segment_reserved.get(junction)){
                    self.information.alertUnavailable("junction_is_locked_route_created");
                } else if (!is_junction_changable){
                    checkJunctionFailure(junction);
                }
            } else {
                checkJunctionFailure(junction);
            }
        } else if (!self.is_master_on){
            self.information.alertUnavailable("panel_is_locked");
        }
    }
    
    public void checkJunctionFailure(JLabel junction){
        if (self.is_junction_has_obstacle.get(junction) && self.is_segment_on_failure.get(junction) == false){
            self.failure.showObstacle(junction);
        } else if (self.is_junction_need_reposition.get(junction) && self.is_segment_on_failure.get(junction) == false){
            self.failure.showNeedReposition(junction);
        } else if (self.is_junction_has_obstacle.get(junction) && self.is_segment_on_failure.get(junction) && self.is_btn_remove_obstacle_pressed){
            self.failure.fixJunctionHasObstacle(junction);
        } else if (self.is_junction_need_reposition.get(junction) && self.is_segment_on_failure.get(junction) && self.is_btn_reposition_junction_pressed){
            self.failure.fixJunctionNeedReposition(junction);
        } else {
            self.information.alertUnavailable("junction_is_on_failure");
        }
    }
    
    public void createRemoveRoute(JLabel signal){
        if (self.is_master_on && self.is_segment_clickable.get(signal)){
                String state = self.signal_state.get(signal);
                Object block_of_signal = self.block_of_signal.get(signal);
                boolean is_route_created = self.is_turn_route_created.containsKey(block_of_signal) && self.is_turn_route_created.get(block_of_signal);
                if (state.equals("red") && !is_route_created){
                    createRoute(signal);
                } else {
                    removeRoute(signal);
                }
        } else {
            self.information.alertUnavailable("panel_is_locked");
        }
    }
    
    private void createRoute(JLabel signal){
        new Thread(()->{
            String direction = self.signal_direction.get(signal);
            Object current_segment = null;
            current_segment = self.block_of_signal.get(signal);
            current_segment = direction.equals("left") ? self.segment_next_left.get(current_segment) : self.segment_next_right.get(current_segment);

            /* --- Check pattern --- */
            boolean not_available = false;
            boolean is_from_diagonal_track = false;
            while(current_segment != null && self.segment_type.get(current_segment) != "block" && not_available == false){
                String segment_type = self.segment_type.get(current_segment);
                boolean is_crossing_junction_not_available = false;
                boolean is_on_failure = false;
                if (self.segment_type.get(current_segment).equals("junction")){
                    /* --- Check junction crossing --- */
                    if (self.junction_horizontal_heading.get(current_segment).equals(direction) == false && ((self.is_junction_turned.get(current_segment) && is_from_diagonal_track == false) || (self.is_junction_turned.get(current_segment) == false && is_from_diagonal_track == true)) ){
                        is_crossing_junction_not_available = true;
                    }
                    /* --- Check failure --- */
                    if (self.is_segment_on_failure.get(current_segment)){
                        is_on_failure = true;
                    }
                }
                if (self.is_segment_reserved.get(current_segment) == false && is_crossing_junction_not_available == false && is_on_failure == false){
                    if ((segment_type.equals("track") && self.track_type.get(current_segment).equals("diagonal")) || (segment_type.equals("junction") && self.is_junction_turned.get(current_segment))){
                        is_from_diagonal_track = true;
                    } else {
                        is_from_diagonal_track = false;
                    }
                    if (self.segment_type.get(current_segment).equals("junction") && self.junction_horizontal_heading.get(current_segment).equals(direction) && self.is_junction_turned.get(current_segment)){
                        current_segment = self.segment_next_turn.get(current_segment);
                    } else {
                        current_segment = direction.equals("left") ? self.segment_next_left.get(current_segment) : self.segment_next_right.get(current_segment);
                    }
                } else {
                    if (is_crossing_junction_not_available){
                        self.information.alertUnavailable("route_cannot_created_junction_turned");
                    } else if (is_on_failure){
                        self.information.alertUnavailable("route_cannot_created_segment_on_failure");
                    }
                    not_available = true;
                }
            }
            
            if (self.segment_type.get(current_segment).equals("block") && self.is_segment_reserved.get(current_segment)){
                self.information.alertUnavailable("route_cannot_created_opposite_route");
                not_available = true;
            }

            if (!not_available){
                /* --- Create Pattern ---*/
                createRoutePattern(signal);
            }
        }).start();
    }
    
    public void createRoutePattern(JLabel signal){
        try {
            if (isControlableSignal(signal)) self.is_segment_clickable.put(signal,false);
            String direction = self.signal_direction.get(signal);
            Object start_block = self.block_of_signal.get(signal);
            Object end_block = null;
            Object current_segment = null;
            current_segment = self.block_of_signal.get(signal);
            current_segment = direction.equals("left") ? self.segment_next_left.get(current_segment) : self.segment_next_right.get(current_segment);
            Object[] passed_turned_junction = new Object[20];
            int count = 0;
            int junction_turned_count = 0;
            /* --- Start switch track color --- */
            while(current_segment != null && self.segment_type.get(current_segment) != "block" && self.is_train_on_segment.get(current_segment) == false){
                self.is_segment_reserved.put(current_segment, true);
                self.switcher.switchTrack(current_segment, "yellow");
                self.audio.routePattern();
                if (count == 2){
                    try {
                        Thread.sleep(400);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(MainControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    count = 0;
                } else {
                    count++;
                }
                if (self.segment_type.get(current_segment).equals("junction") && self.junction_horizontal_heading.get(current_segment).equals(direction) && self.is_junction_turned.get(current_segment)){
                    passed_turned_junction[junction_turned_count] = current_segment;
                    junction_turned_count++;
                    current_segment = self.segment_next_turn.get(current_segment);
                } else {
                    current_segment = direction.equals("left") ? self.segment_next_left.get(current_segment) : self.segment_next_right.get(current_segment);
                }
            }
            Thread.sleep(1000);
            /* --- Signal Switching --- */
            if (current_segment != null && self.segment_type.get(current_segment).equals("block")){
                self.is_segment_reserved.put(current_segment,true);
                end_block = current_segment;
                JLabel signal_of_block = self.signal_of_block.get(current_segment);
                String state_of_block = self.signal_state.get(signal_of_block);
                
                /* --- Set adjacent signal --- */
                self.entry_signal_adjacent.put(signal,signal_of_block);
                
                /* --- Signal switching condition --- */
                boolean is_entry_signal_and_route_turned = checkTurnRouteCondition(signal, passed_turned_junction);
                if (!is_entry_signal_and_route_turned || (is_entry_signal_and_route_turned && self.is_train_on_segment.get(start_block))){
                    self.relative_signal.put(signal_of_block,signal);
                    switchEntrySignal(signal,is_entry_signal_and_route_turned);
                }
                /* --- Trigger next block to create route pattern --- */
                if (!isControlableSignal(signal_of_block)){
                    createRoutePattern(signal_of_block);
                }
                /* --- Alert if route has created --- */
                if (isControlableSignal(signal)){
                    self.audio.alertGeneral();
                }
                self.headPanel.increaseCounter(self.counter_created_route);
                String text = self.textParser.routeCreated(start_block, end_block);
                self.information.notifyAndLog(text);
            }
            if (self.is_segment_clickable.containsKey(signal)) self.is_segment_clickable.put(signal,true);
        } catch (InterruptedException ex) {
            Logger.getLogger(MainControl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void switchEntrySignal(JLabel signal, Boolean is_entry_signal_and_route_turned){
        try {
            JLabel signal_adjacent = self.entry_signal_adjacent.get(signal);
            String state_of_block = self.signal_state.get(signal_adjacent);
            if (state_of_block.equals("red")){
                self.switcher.switchSignal(signal, "yellow");
            } else {
                String signal_color = "green";
                if (!self.signal_type.get(signal).equals("blok")){
                    signal_color = "yellow";
                }
                self.switcher.switchSignal(signal, signal_color);
            }
            if (is_entry_signal_and_route_turned){self.switcher.switchSpeedSignal(signal, "on");};
            Thread.sleep(100);
            triggerRelativeSignal(signal);
        } catch (InterruptedException ex) {
            Logger.getLogger(MainControl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private boolean checkTurnRouteCondition(JLabel signal, Object[] turned_junctions){
        boolean is_turned = false;
        Object block = self.block_of_signal.get(signal);
        if (self.is_entry_signal_block.containsKey(block)){
            Object[] turn_condition = self.turn_condition.get(block);
            for (int i = 0; i < turn_condition.length; i++) {
                if (Arrays.asList(turned_junctions).contains(turn_condition[i])){
                    is_turned = true;
                }
            }
            /* --- Check if is turned --- */
            if (is_turned){
                self.is_turn_route_created.put(block, true);
            }
        }
        return is_turned;
    }
    
    private void removeRoute(JLabel signal){
        new Thread(()->{
            try {
                if (self.is_segment_clickable.containsKey(signal)) self.is_segment_clickable.put(signal,false);
                Object block_of_signal = self.block_of_signal.get(signal);
                String direction = self.signal_direction.get(signal);
                Object current_segment = null;
                current_segment = block_of_signal;
                current_segment = direction.equals("left") ? self.segment_next_left.get(current_segment) : self.segment_next_right.get(current_segment);
                int count = 0;
                while(current_segment != null && self.segment_type.get(current_segment) != "block"){
                    self.is_segment_reserved.put(current_segment, false);
                    self.switcher.switchTrack(current_segment, "black");
                    self.audio.routePattern();
                    if (count == 2){
                        try {
                            Thread.sleep(400);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(MainControl.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        count = 0;
                    } else {
                        count++;
                    }
                    if (self.segment_type.get(current_segment).equals("junction") && self.junction_horizontal_heading.get(current_segment).equals(direction) && self.is_junction_turned.get(current_segment)){
                        current_segment = self.segment_next_turn.get(current_segment);
                    } else {
                        current_segment = direction.equals("left") ? self.segment_next_left.get(current_segment) : self.segment_next_right.get(current_segment);
                    }
                }
                Thread.sleep(1000);
                if (current_segment != null && self.segment_type.get(current_segment).equals("block")){
                    self.is_segment_reserved.put(current_segment,false);
                    JLabel signal_of_block = self.signal_of_block.get(current_segment);
                    if (self.is_segment_clickable.containsKey(signal)){
                        self.switcher.switchSignal(signal, "red");
                        Thread.sleep(100);
                        triggerRelativeSignal(signal);
                        self.entry_signal_adjacent.remove(signal_of_block);
                    }
                    self.relative_signal.put(signal_of_block, null);
                    self.switcher.switchSpeedSignal(signal, "off");
                    self.is_turn_route_created.put(block_of_signal, false);
                    /* --- Trigger next block to remove route pattern --- */
                    if (!self.is_segment_clickable.containsKey(signal_of_block)){
                        removeRoute(signal_of_block);
                    }
                    /* --- Alert if route has removed --- */
                    if (self.is_segment_clickable.containsKey(signal)){
                        self.audio.alertGeneral();
                    }
                    self.headPanel.increaseCounter(self.counter_removed_route);
                    String text = self.lang.text("route_removed");
                    self.information.notifyAndLog(text);
                }
                if (self.is_segment_clickable.containsKey(signal)) self.is_segment_clickable.put(signal,true);
            } catch (InterruptedException ex) {
                Logger.getLogger(MainControl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }
    
    public void removeSpecificPattern(Object point_from, Object point_to,String direction){
        System.out.println("Remove specific pattern from "+self.number_of_segment.get(point_from)+" to "+self.number_of_segment.get(point_to)+" direction "+direction);
        Object current_segment = point_from;
        int count = 0;
        while (current_segment != point_to){
            if (self.segment_type.get(current_segment).equals("block") == false && current_segment != point_from){
                self.is_train_on_segment.put(current_segment,false);
                self.is_segment_reserved.put(current_segment,false);
            }
            if (self.segment_type.get(current_segment).equals("track") || self.segment_type.get(current_segment).equals("junction")){
                self.is_train_on_segment.put(current_segment,false);
                self.is_segment_reserved.put(current_segment,false);
                self.switcher.switchTrack(current_segment, "black");
                count++;
            }
            if (count == 2){
                try {
                    Thread.sleep(400);
                    count = 0;
                } catch (InterruptedException ex) {
                    Logger.getLogger(MainControl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (self.segment_type.get(current_segment).equals("junction") && self.junction_horizontal_heading.get(current_segment).equals(direction) && self.is_junction_turned.get(current_segment)){
                current_segment = self.segment_next_turn.get(current_segment);
            } else {
                current_segment = direction.equals("left") ? self.segment_next_left.get(current_segment) : self.segment_next_right.get(current_segment);
            }
        }
        self.is_train_on_segment.put(current_segment,false);
        self.is_segment_reserved.put(current_segment,false);
        if (self.segment_type.get(current_segment).equals("track") || self.segment_type.get(current_segment).equals("junction")){
            self.switcher.switchTrack(current_segment, "black");
        }
    }
    
    public void removeSpecificPattern(ArrayList<Object> passed_segment){
        ListIterator<Object> segments = passed_segment.listIterator();
        int count = 0;
        Object current_segment = null;
        while (segments.hasNext()) {
            current_segment = segments.next();
            if (self.segment_type.get(current_segment).equals("block") == false && current_segment != passed_segment.get(0)){
                self.is_train_on_segment.put(current_segment,false);
                self.is_segment_reserved.put(current_segment,false);
            }
            if (self.segment_type.get(current_segment).equals("track") || self.segment_type.get(current_segment).equals("junction")){
                self.is_train_on_segment.put(current_segment,false);
                self.is_segment_reserved.put(current_segment,false);
                self.switcher.switchTrack(current_segment, "black");
                count++;
            }
            if (count == 2){
                try {
                    Thread.sleep(400);
                    count = 0;
                } catch (InterruptedException ex) {
                    Logger.getLogger(MainControl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        self.is_train_on_segment.put(current_segment,false);
        self.is_segment_reserved.put(current_segment,false);
        if (self.segment_type.get(current_segment).equals("track") || self.segment_type.get(current_segment).equals("junction")){
            self.switcher.switchTrack(current_segment, "black");
        }
    }
    
    /**
     * Arrival block is where the train enter the control panel
     * 1 : From block 101 (JNG Lower)
     * 2 : From block 203 (BKS)
     * 3 : From block 301 (JNG Upper)
     * @param arrival_block 
     */
    public void openRoute(int arrival_block){
        new Thread(()->{
            Object current_segment = self.arrival_block_segment.get(arrival_block);
            String direction = self.arrival_block_direction.get(arrival_block);
            String from_station = self.arrival_block_station.get(arrival_block);
            if (!self.is_segment_reserved.get(current_segment)){
                try {
                    self.headPanel.alertTrainEnter(from_station);
                    self.audio.alertArrival(from_station);
                    self.audio.routePattern();
                    while(!self.segment_type.get(current_segment).equals("block")){
                        self.is_segment_reserved.put(current_segment,true);
                        if (self.segment_type.get(current_segment).equals("track")){
                            self.switcher.switchTrack(current_segment, "yellow");
                        }
                        current_segment = direction.equals("left") ? self.segment_next_left.get(current_segment) : self.segment_next_right.get(current_segment);
                    }
                    Thread.sleep(3000);
                    if (self.segment_type.get(current_segment).equals("block")){
                        self.is_segment_reserved.put(current_segment, true);
                        JLabel signal_of_block = self.signal_of_block.get(current_segment);
                        createRoutePattern(signal_of_block);
                    }
                } catch (InterruptedException ex) {
                    Logger.getLogger(MainControl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }).start();
    }
    
    public void triggerRelativeSignal(JLabel signal){
        if (self.relative_signal.containsKey(signal) && self.relative_signal.get(signal) != null){
            JLabel relative_signal = self.relative_signal.get(signal);
            Object block_of_signal = self.block_of_signal.get(signal);
            if (!self.is_train_on_segment.get(block_of_signal)){
                /* --- Switch Signal --- */
                String state = self.signal_state.get(signal);
                if (state.equals("red")){
                    self.switcher.switchSignal(relative_signal, "yellow");
                } else {
                    self.switcher.switchSignal(relative_signal, "green");
                }
            }
        }
    }
    
    public void trainPassSignal(JLabel signal){
        self.switcher.switchSignal(signal, "red");
        if (isControlableSignal(signal)){
            JLabel adjacent_signal = self.entry_signal_adjacent.get(signal);
            self.relative_signal.remove(adjacent_signal);
            self.entry_signal_adjacent.remove(adjacent_signal);
            Object block = self.block_of_signal.get(signal);
            if (self.is_entry_signal_block.containsKey(block) && (self.is_turn_route_created.containsKey(block) && self.is_turn_route_created.get(block))){
                self.is_turn_route_created.put(block, false);
            }
        } else {
            JLabel relative_signal = self.relative_signal.get(signal);
            if (isControlableSignal(relative_signal)){
                self.relative_signal.remove(signal);
            }
        }
    }
    
    public boolean isControlableSignal(JLabel signal){
        return Arrays.asList(self.controlable_signal).contains(signal);
    }
    
    public void enableDisableDepartureButton(Object segment,boolean enabled){
        if (segment == self.block_010){
            self.btn_depart_platform_1.setEnabled(enabled);
        } else if (segment == self.block_020){
            self.btn_depart_platform_2.setEnabled(enabled);
        } else if (segment == self.block_030){
            self.btn_depart_platform_3.setEnabled(enabled);
        } else if (segment == self.block_040){
            self.btn_depart_platform_4.setEnabled(enabled);
        } else if (segment == self.block_050){
            self.btn_depart_platform_5.setEnabled(enabled);
        } else if (segment == self.block_060){
            self.btn_depart_platform_6.setEnabled(enabled);
        }
    }
    
    public void departingTrain(Object button){
        if (button == self.btn_depart_platform_1){
            self.platform_departure_approved.put(self.block_010,true);
            enableDisableDepartureButton(self.block_010,false);
        } else if (button == self.btn_depart_platform_2){
            self.platform_departure_approved.put(self.block_020,true);
            enableDisableDepartureButton(self.block_020,false);
        } else if (button == self.btn_depart_platform_3){
            self.platform_departure_approved.put(self.block_030,true);
            enableDisableDepartureButton(self.block_030,false);
        } else if (button == self.btn_depart_platform_4){
            self.platform_departure_approved.put(self.block_040,true);
            enableDisableDepartureButton(self.block_040,false);
        } else if (button == self.btn_depart_platform_5){
            self.platform_departure_approved.put(self.block_050,true);
            enableDisableDepartureButton(self.block_050,false);
        } else if (button == self.btn_depart_platform_6){
            self.platform_departure_approved.put(self.block_060,true);
            enableDisableDepartureButton(self.block_060,false);
        }
    }
    
    public void bellOnOff(){
        if (!self.is_bell_on){
            self.switcher.switchButtonTelephone(self.btn_bell, true);
            self.is_bell_on = true;
            self.audio.bell(true);
        } else {
            self.switcher.switchButtonTelephone(self.btn_bell, false);
            self.is_bell_on = false;
            self.audio.bell(false);
        }
    }

}
