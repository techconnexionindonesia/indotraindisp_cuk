package indotraindisp_cuk;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;

/**
 *
 * @author TCIPRO
 */
public class Failure {
    ControlPanelCUK self = null;
    
    public Failure(ControlPanelCUK parent){
        self = parent;
    }
    
    public void showObstacle(JLabel junction){
        new Thread(()->{
            try {
                self.is_segment_clickable.put(junction, false);
                self.is_segment_on_failure.put(junction, true);
                self.switcher.switchJunction(junction);
                self.audio.junctionClick();
                Thread.sleep(3000);
                self.switcher.switchJunction(junction);
                self.audio.junctionClick();
                JLabel label_of_junction = self.label_of_junction.get(junction);
                self.switcher.blinkBgColor(label_of_junction, "red", "dark_gray");
                String text = self.textParser.junctionHasObstacle(junction);
                self.information.alertFailure(text);
                self.headPanel.alertFailureJunction();
                self.is_segment_clickable.put(junction, true);
            } catch (InterruptedException ex) {
                Logger.getLogger(Failure.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }
    
    public void showNeedReposition(JLabel junction){
        new Thread(()->{
            try {
                self.is_segment_clickable.put(junction, false);
                self.is_segment_on_failure.put(junction,true);
                self.switcher.switchJunction(junction);
                self.audio.junctionClick();
                Thread.sleep(3000);
                self.switcher.blinkBgInvisible(junction);
                JLabel label_of_junction = self.label_of_junction.get(junction);
                self.switcher.blinkBgColor(label_of_junction, "red", "dark_gray");
                String text = self.textParser.junctionNeedReposition(junction);
                self.information.alertFailure(text);
                self.headPanel.alertFailureJunction();
                self.is_segment_clickable.put(junction, true);
            } catch (InterruptedException ex) {
                Logger.getLogger(Failure.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }
    
    public void fixJunctionHasObstacle(JLabel junction){
        new Thread(()->{
            try {
                JLabel label_of_junction = self.label_of_junction.get(junction);
                self.headPanel.removeObstacle();
                self.headPanel.increaseCounter(self.counter_remove_obstacle);
                self.switcher.blinkBgColor(label_of_junction, "green", "dark_gray");
                Thread.sleep(1000);
                self.switcher.switchJunction(junction);
                self.audio.junctionClick();
                Thread.sleep(3000);
                self.switcher.switchJunction(junction);
                self.audio.junctionClick();
                Thread.sleep(3000);
                self.switcher.switchJunction(junction);
                self.audio.junctionClick();
                Thread.sleep(3000);
                self.switcher.switchJunction(junction);
                self.audio.junctionClick();
                Thread.sleep(3000);
                if (self.is_junction_need_fixing.get(junction)){
                    
                } else {
                    self.switcher.stopBlinkBgColor(label_of_junction);
                    self.is_junction_has_obstacle.put(junction, false);
                    self.is_segment_on_failure.put(junction, false);
                    String text = self.textParser.segmentHasBeenFixed(junction);
                    self.information.notifyAndLog(text);
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(MainControl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }
    
    public void fixJunctionNeedReposition(JLabel junction){
        new Thread(()->{
            try {
                JLabel label_of_junction = self.label_of_junction.get(junction);
                self.headPanel.repositionJunction();
                self.headPanel.increaseCounter(self.counter_reposition_junction);
                self.switcher.blinkBgColor(label_of_junction, "green", "dark_gray");
                Thread.sleep(5000);
                if (self.is_junction_need_fixing.get(junction)){
                    
                } else {
                    self.switcher.stopBlinkBgColor(label_of_junction);
                    self.switcher.stopBlinkBgColor(junction);
                    self.is_junction_need_reposition.put(junction, false);
                    self.is_segment_on_failure.put(junction, false);
                    String text = self.textParser.segmentHasBeenFixed(junction);
                    self.information.notifyAndLog(text);
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(Failure.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }
}
