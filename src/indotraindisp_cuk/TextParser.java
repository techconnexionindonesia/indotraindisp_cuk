/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indotraindisp_cuk;

import javax.swing.JLabel;

/**
 *
 * @author TCIPRO
 */
public class TextParser {
    ControlPanelCUK self = null;
    
    public TextParser(ControlPanelCUK parent){
        self = parent;
    }
    
    public String textJunctionChanged(JLabel junction){
        String junction_name = self.lang.text("junction")+" "+self.number_of_segment.get(junction);
        String text = self.lang.text("has_changed",junction_name);
        return text;
    }
    
    public String textJunctionHasBeenLocked(JLabel junction){
        String junction_name = self.lang.text("junction")+" "+self.number_of_segment.get(junction);
        String text = self.lang.text("has_been_locked",junction_name);
        return text;
    }
    
    public String textJunctionHasBeenUnlocked(JLabel junction){
        String junction_name = self.lang.text("junction")+" "+self.number_of_segment.get(junction);
        String text = self.lang.text("has_been_unlocked",junction_name);
        return text;
    }
    
    public String junctionHasObstacle(JLabel junction){
        String junction_name = self.lang.text("junction")+" "+self.number_of_segment.get(junction);
        String text = self.lang.text("junction_has_obstacle",junction_name);
        return text;
    }
    
    public String junctionNeedReposition(JLabel junction){
        String junction_name = self.lang.text("junction")+" "+self.number_of_segment.get(junction);
        String text = self.lang.text("junction_need_reposition",junction_name);
        return text;
    }
    
    public String segmentHasBeenFixed(Object segment){
        String type = self.segment_type.get(segment);
        String segment_name = self.lang.text(type)+" "+self.number_of_segment.get(segment);
        String text = self.lang.text("has_been_fixed",segment_name);
        return text;
    }
    
    public String routeCreated(Object start_block, Object end_block){
        String start_block_name = self.lang.text("block")+" "+self.number_of_segment.get(start_block);
        String end_block_name = self.lang.text("block")+" "+self.number_of_segment.get(end_block);
        String text = self.lang.text("route_created",start_block_name,end_block_name);
        return text;
    }
    
    public String trainEnter(String train_number){
        String from = self.arrival_block_station.get(self.train_arrival_block.get(train_number));
        String text = self.lang.text("train_enter",train_number,from);
        return text;
    }
    
    public String trainStopDepart(String train_number,Object segment,String sequence){
        String platform = self.platform_number.get(segment);
        String text = self.lang.text("train_stop_at_platform",train_number,platform);
        if (sequence.equals("depart")){
            text = self.lang.text("train_depart_from_platform",train_number,platform);
        }
        return text;
    }
    
}
