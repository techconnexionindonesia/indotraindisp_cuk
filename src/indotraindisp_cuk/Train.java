package indotraindisp_cuk;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author Tech Connexion Indonesia
 */
public class Train {
    ControlPanelCUK self = null;
    
    public Train(ControlPanelCUK parent){
        self = parent;
    }
    
    public void checkEntryTrain(){
        new Thread(()->{
            while(self.is_simulation_running){
                try {
                    ListIterator<String> iterator = self.active_trains.listIterator();
                    while (iterator.hasNext()) {
                        String train_number = iterator.next();
                        if (self.is_train_entered_zone.get(train_number) == false){
                            boolean terminus_departure = false;
                            String train_eta_text = self.train_eta.get(train_number);
                            if (train_eta_text.equals("-")){
                                terminus_departure = true;
                                train_eta_text = self.train_etd.get(train_number);
                            }
                            int delay = self.train_delay.get(train_number);
                            DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy/MM/dd HH:mm:ss");
                            DateTime time_eta = formatter.parseDateTime(self.date_play+" "+train_eta_text+":00");
                            time_eta = terminus_departure ? time_eta : time_eta.plusMinutes(Integer.valueOf(delay));
                            
                            /* --- Check Notification to sent from previous station --- */
                            if (time_eta.isBefore(self.time_play.plusSeconds(960)) 
                                    && time_eta.isAfter(self.time_play.plusSeconds(955)) 
                                    && self.train_type.get(train_number).equals("krl") 
                                    && self.train_notification_previous_station_sent.get(train_number) == false
                                    && (self.train_arrival_block.get(train_number) == 1 || self.train_arrival_block.get(train_number) == 3)){
                                self.train_notification_previous_station_sent.put(train_number, true);
                                self.headPanel.notificationArrival("JNG");
                                self.announcement.trainDepAdjacentStation(train_number,"JNG");
                            }
                            else if (time_eta.isBefore(self.time_play.plusSeconds(660)) 
                                    && time_eta.isAfter(self.time_play.plusSeconds(655)) 
                                    && self.train_type.get(train_number).equals("krl") == false 
                                    && self.train_notification_previous_station_sent.get(train_number) == false
                                    && (self.train_arrival_block.get(train_number) == 1 || self.train_arrival_block.get(train_number) == 3)){
                                self.train_notification_previous_station_sent.put(train_number, true);
                                self.headPanel.notificationArrival("JNG");
                            }
                            else if (time_eta.isBefore(self.time_play.plusSeconds(480)) 
                                    && time_eta.isAfter(self.time_play.plusSeconds(475)) 
                                    && self.train_type.get(train_number).equals("krl")
                                    && self.train_notification_previous_station_sent.get(train_number) == false
                                    && self.train_arrival_block.get(train_number) == 2){
                                self.train_notification_previous_station_sent.put(train_number, true);
                                self.headPanel.notificationArrival("BKS");
                                self.announcement.trainDepAdjacentStation(train_number,"BKS");
                            }
                            else if (time_eta.isBefore(self.time_play.plusSeconds(240)) 
                                    && time_eta.isAfter(self.time_play.plusSeconds(235)) 
                                    && self.train_type.get(train_number).equals("krl") == false
                                    && self.train_notification_previous_station_sent.get(train_number) == false
                                    && self.train_arrival_block.get(train_number) == 2){
                                self.train_notification_previous_station_sent.put(train_number, true);
                                self.headPanel.notificationArrival("BKS");
                            }
                            
                            /* --- Check train entering --- */
                            boolean train_enter = false;
                            if (time_eta.isBefore(self.time_play.plusSeconds(260)) 
                                    && time_eta.isAfter(self.time_play.plusSeconds(230)) 
                                    && self.train_type.get(train_number).equals("krl") 
                                    && self.train_arrival_block.get(train_number) == 1
                                    && self.train_platform.get(train_number).equals("2") == false
                                    && self.is_train_entered_zone.get(train_number) == false 
                                    && self.entry_waiting.contains(train_number) == false){
                                train_enter = true;
                            } else if (time_eta.isBefore(self.time_play.plusSeconds(225)) 
                                    && time_eta.isAfter(self.time_play.plusSeconds(195)) 
                                    && self.train_type.get(train_number).equals("krl") 
                                    && self.train_arrival_block.get(train_number) == 1
                                    && self.train_platform.get(train_number).equals("2")
                                    && self.is_train_entered_zone.get(train_number) == false 
                                    && self.entry_waiting.contains(train_number) == false){
                                train_enter = true;
                            } else if (time_eta.isBefore(self.time_play.plusSeconds(270)) 
                                    && time_eta.isAfter(self.time_play.plusSeconds(240)) 
                                    && self.train_type.get(train_number).equals("krl") 
                                    && self.train_arrival_block.get(train_number) == 2
                                    && self.train_platform.get(train_number).equals("3") == false
                                    && self.is_train_entered_zone.get(train_number) == false 
                                    && self.entry_waiting.contains(train_number) == false){
                                train_enter = true;
                            } else if (time_eta.isBefore(self.time_play.plusSeconds(195)) 
                                    && time_eta.isAfter(self.time_play.plusSeconds(165)) 
                                    && self.train_type.get(train_number).equals("krl") 
                                    && self.train_arrival_block.get(train_number) == 2
                                    && self.train_platform.get(train_number).equals("3")
                                    && self.is_train_entered_zone.get(train_number) == false 
                                    && self.entry_waiting.contains(train_number) == false){
                                train_enter = true;
                            } else if (time_eta.isBefore(self.time_play.plusSeconds(143)) 
                                    && time_eta.isAfter(self.time_play.plusSeconds(113)) 
                                    && self.train_type.get(train_number).equals("krl") == false
                                    && self.train_arrival_block.get(train_number) == 1
                                    && self.is_train_entered_zone.get(train_number) == false 
                                    && self.entry_waiting.contains(train_number) == false) {
                                train_enter = true;
                            } else if (time_eta.isBefore(self.time_play.plusSeconds(153)) 
                                    && time_eta.isAfter(self.time_play.plusSeconds(123)) 
                                    && self.train_type.get(train_number).equals("krl") == false
                                    && self.train_arrival_block.get(train_number) == 2
                                    && self.train_platform.get(train_number).equals("6") == false
                                    && self.is_train_entered_zone.get(train_number) == false 
                                    && self.entry_waiting.contains(train_number) == false) {
                                train_enter = true;
                            } else if (time_eta.isBefore(self.time_play.plusSeconds(273)) 
                                    && time_eta.isAfter(self.time_play.plusSeconds(243)) 
                                    && self.train_type.get(train_number).equals("krl") == false
                                    && self.train_arrival_block.get(train_number) == 2
                                    && self.train_platform.get(train_number).equals("6") == true
                                    && self.is_train_entered_zone.get(train_number) == false 
                                    && self.entry_waiting.contains(train_number) == false) {
                                train_enter = true;
                            } else if (time_eta.isBefore(self.time_play.plusSeconds(195)) 
                                    && time_eta.isAfter(self.time_play.plusSeconds(165)) 
                                    && self.train_type.get(train_number).equals("krl") == false
                                    && self.train_arrival_block.get(train_number) == 3
                                    && self.is_train_entered_zone.get(train_number) == false 
                                    && self.entry_waiting.contains(train_number) == false) {
                                train_enter = true;
                            }
                            if (train_enter && checkIfEntryAvailable(train_number)){
                                insertTrainIntoZone(train_number);
                            } else if (train_enter){
                                self.entry_waiting.add(train_number);
                            }
                        }
                    }
                    checkEntryWaiting();
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Train.class.getName()).log(Level.SEVERE, null, ex);
                } 
           }
        }).start();
    }
    
    private void checkEntryWaiting(){
        ArrayList<String> modified_train_entry_list = self.entry_waiting;
        for (int i = 0; i < modified_train_entry_list.size(); i++) {
            String train_number = self.entry_waiting.get(i);
            if (checkIfEntryAvailable(train_number)){
                System.out.println("Insert train "+train_number+" into waiting entry list");
                insertTrainIntoZone(train_number);
                self.entry_waiting.remove(train_number);
            }
        }
    }
    
    private void insertTrainIntoZone(String train_number){
        new Thread(()->{
            try {
                int random_enter_delay = self.utility.getRandomNumber(1, 30);
                Thread.sleep(random_enter_delay);
                self.is_train_entered_zone.put(train_number,true);
                self.running_train.add(train_number);
                self.mainControl.openRoute(self.train_arrival_block.get(train_number));
                startTrainInstance(train_number);
            } catch (InterruptedException ex) {
                Logger.getLogger(Train.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }
    
    public void startTrainInstance(String train_number){
        new Thread(()->{
            /*--- initial run ---*/
            Object[] initial_run = initialRun(train_number);
            Object segment = initial_run[0];
            String direction = (String) initial_run[1];
            /*--- looping run ---*/
            boolean finish = false;
            while (finish == false && self.is_simulation_running){
                try {
                    System.out.println();
                    JLabel signal_of_block = self.signal_of_block.get(segment);
                    String signal_state = self.signal_state.get(signal_of_block);
                    System.out.println("Train "+train_number+" facing signal "+signal_state);
                    boolean stopped = checkIfStopAtStation(segment,train_number);
                    int restricted_speed = 0;
                    
                    /* --- Wait if getting red signal --- */
                    while (signal_state.equals("red")){
                        try {
                            stopped = true;
                            updateTrainMovement(train_number,false);
                            Thread.sleep(1000); // Wait if signal still red
                            signal_state = self.signal_state.get(signal_of_block);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(Train.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    
                    /* --- Check restricted speed by signal --- */
                    if ((self.is_speed_signal_on.containsKey(signal_of_block) && self.is_speed_signal_on.get(signal_of_block))){
                        restricted_speed = 30;
                    } else if (signal_state.equals("yellow")) {
                        restricted_speed = 45;
                    }
                    
                    /* --- Check if got red signal --- */
                    updateTrainMovement(train_number,true);
                    if (stopped){
                        if (self.platform_number.containsKey(segment)){
                            String log = self.textParser.trainStopDepart(train_number, segment, "depart");
                            self.information.notifyAndLog(log);
                            self.audio.checkTrainDeparting(train_number,segment);
                            self.scoring.checkDepartureTime(train_number);
                        }
                        int random_respond_time = self.utility.getRandomNumber(10, 17);
                        Thread.sleep(random_respond_time*1000);
                    }
                    
                    /* --- Get Next Block --- */
                    Object previous_segment = segment;
                    segment = getNextBlock(segment,direction);
                    boolean exiting = false;
                    if (self.exit_block_segment.containsKey(segment)){
                        exiting = true;
                    }
                    if (self.block_notification_trigger.containsKey(segment)){
                        self.headPanel.makeNotifyAvailable(self.block_notification_trigger.get(segment));
                    } else {
                        checkNotifyAvailable();
                    }
                    
                    /* --- Announcement --- */
                    if (self.platform_number.containsKey(segment) || self.is_entry_signal_block.containsKey(segment)){
                        self.announcement.trainEnterPlatform(train_number);
                    }
                    
                    /* --- change train number position --- */
                    System.out.println("Getting train moved from block "+self.number_of_segment.get(previous_segment)+" to block "+(exiting ? "exiting" : self.number_of_segment.get(segment)));
                    JTextField previous_block = (JTextField) previous_segment;
                    JLabel signal_of_previous_block = self.signal_of_block.get(previous_block);
                    Object present_checking_segment = segment;
                    new Thread(()->{
                        previous_block.setText("");
                        if (self.segment_type.get(present_checking_segment).equals("block")){
                            JTextField next_block = (JTextField) present_checking_segment;
                            next_block.setText(train_number);
                        }
                    }).start();
                    self.mainControl.trainPassSignal(signal_of_previous_block);
                    checkIfTurnRouteCreated(segment);
                    
                    /* --- Waiting train's travel time --- */
                    int travel_time = 0;
                    if (!exiting){
                        updateTrainPosition(train_number,segment,false);
                        travel_time = restricted_speed != 0 ? getTravelTime(previous_segment,segment,restricted_speed) : getTravelTime(previous_segment,segment,train_number);
                    } else {
                        updateTrainPosition(train_number,segment,true);
                        travel_time = restricted_speed != 0 ? getTravelTime(self.exit_block_segment.get(segment),restricted_speed) : getTravelTime(self.exit_block_segment.get(segment),train_number);
                    }
                    clearPreviousSegment(previous_segment,segment,direction,train_number,restricted_speed);
                    if (self.platform_number.containsKey(segment)){
                        self.scoring.checkEnteringPlatform(train_number,segment);
                        self.audio.trainToStation(train_number,segment,travel_time,restricted_speed);
                    }
                    Thread.sleep(travel_time*1000);
                    if (exiting){
                        self.scoring.checkNotificationDeparture(train_number);
                        trainExitingZone(train_number,segment,previous_segment);
                        finish = true;
                    }
                } catch (InterruptedException ex) {
                    Logger.getLogger(Train.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }).start();
    }
    
    private Object[] initialRun(String train_number){
        Object block_segment = null;
        String direction = "";
        try {
            System.out.println("Train "+train_number+" entering control zone");
            String notification = self.textParser.trainEnter(train_number);
            self.information.notifyAndLog(notification);
            int arrival_block = self.train_arrival_block.get(train_number);
            Object initial_segment = self.arrival_block_segment.get(arrival_block);
            direction = self.arrival_block_direction.get(arrival_block);
            /* --- Enter train initially into block ---*/
            String entry_block_name = self.arrival_block_name.get(arrival_block);
            int initial_travel_time = getTravelTime(entry_block_name,train_number); // second
            Thread.sleep(initial_travel_time*1000);
            Object get_next_block = getNextBlock(initial_segment,direction);
            block_segment = get_next_block;
            System.out.println("Get initial block for train : "+train_number+" is : Block "+self.number_of_segment.get(get_next_block));
            JTextField current_block = (JTextField) get_next_block;
            current_block.setText(train_number);
            updateTrainPosition(train_number,current_block,false);
            Thread.sleep(30*1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Train.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new Object[]{block_segment,direction};
    }
    
    private Object getNextBlock(Object segment, String direction){
        boolean initial_run = true;
        if (self.arrival_block_segment.containsValue(segment) == false){
            self.is_segment_reserved.put(segment,false);
            self.is_train_on_segment.put(segment,false);
        } else {
            self.is_train_on_segment.put(segment, true);
        }
        while ((segment != null && self.segment_type.get(segment).equals("block") == false && self.exit_block_segment.containsKey(segment) == false) || initial_run == true){
            self.is_train_on_segment.put(segment, true);
            if (self.segment_type.get(segment).equals("block") == false){
                JLabel track = (JLabel) segment;
                new Thread(()->{self.switcher.switchTrack(track, "red");}).start();
            }
            if (self.segment_type.get(segment).equals("junction") && self.junction_horizontal_heading.get(segment).equals(direction) && self.is_junction_turned.get(segment)){
                segment = self.segment_next_turn.get(segment);
            } else {
                segment = direction.equals("left") ? self.segment_next_left.get(segment) : self.segment_next_right.get(segment);
            }
            initial_run = false;
        }
        self.is_train_on_segment.put(segment, true);
        if (self.segment_type.get(segment).equals("track")){
            self.switcher.switchTrack(segment, "red");
        }
        return segment;
    }
    
    private void updateTrainPosition(String train_number, Object position, Boolean exiting){
        String position_name = self.lang.text("exiting");
        if (!exiting){
            position_name = self.number_of_segment.get(position);
            self.train_position.put(train_number,self.lang.text("block")+" "+position_name);
        }
    }
    
    private void updateTrainMovement(String train_number, boolean is_running){
        String movement = is_running ? self.lang.text("running") : self.lang.text("stopped");
        self.train_movement.put(train_number,movement);
    }
    
    private void clearPreviousSegment(Object previous_segment,Object next_segment,String direction,String train_number,int speed){
        new Thread(()->{
            try {
                System.out.println("Clear previous segment from block "+self.number_of_segment.get(previous_segment)+" to block "+self.number_of_segment.get(next_segment)+" for train "+train_number+" with speed "+(speed != 0?speed:"normal"));
                Thread.sleep(2*1000);
                Object current_segment = previous_segment;
                boolean initial_run = true;
                boolean is_from_entry = false;
                boolean is_from_partial_segment = false;
                Object adjacent_segment = null;
                while ((is_from_entry == false && is_from_partial_segment == false && current_segment != null && self.segment_type.get(current_segment).equals("block") == false) || initial_run == true){
                    if (self.segment_type.get(current_segment).equals("junction") && self.junction_horizontal_heading.get(current_segment).equals(direction) == false && self.is_junction_turned.get(current_segment)){
                        current_segment = self.segment_next_turn.get(current_segment);
                    } else {
                        current_segment = direction.equals("left") ? self.segment_next_right.get(current_segment) : self.segment_next_left.get(current_segment);
                    }
                    if (self.arrival_block_segment.containsValue(current_segment)){
                        System.out.println("Clear previous from entry");
                        is_from_entry = true;
                    } else {
                        if (self.segment_type.get(current_segment).equals("junction") && self.junction_horizontal_heading.get(current_segment).equals(direction) == false && self.is_junction_turned.get(current_segment)){
                            adjacent_segment = self.segment_next_turn.get(current_segment);
                        } else {
                            adjacent_segment = direction.equals("left") ? self.segment_next_right.get(current_segment) : self.segment_next_left.get(current_segment);
                        }
                        if (self.is_train_on_segment.get(adjacent_segment) == false && self.segment_type.get(adjacent_segment).equals("block") == false){
                            is_from_partial_segment = true;
                        }
                    }
                    initial_run = false;
                }
                if (!is_from_entry){
                    Object point_from = current_segment;
                    Object point_to = null;
                    ArrayList<Object> passed_segment = new ArrayList<Object>();
                    passed_segment.add(point_from);
                    while (current_segment != next_segment){
                        if (self.segment_type.get(current_segment).equals("junction") && self.junction_horizontal_heading.get(current_segment).equals(direction) && self.is_junction_turned.get(current_segment)){
                            current_segment = self.segment_next_turn.get(current_segment);
                        } else {
                            current_segment = direction.equals("left") ? self.segment_next_left.get(current_segment) : self.segment_next_right.get(current_segment);
                        }
                        passed_segment.add(current_segment);
                        if (self.segment_type.get(current_segment).equals("junction")){
                            try {
                                point_to = current_segment;
                                int interval = speed != 0 ? getTravelTime(point_from,point_to,speed) : getTravelTime(point_from,point_to,train_number);
                                Thread.sleep(interval*1000);
                                self.mainControl.removeSpecificPattern(passed_segment);
                                point_from = current_segment;
                                passed_segment = new ArrayList<Object>();
                                passed_segment.add(point_from);
                            } catch (InterruptedException ex) {
                                Logger.getLogger(Train.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        } else if (self.segment_type.get(current_segment).equals("block") && current_segment != next_segment){
                            point_to = current_segment;
                            int interval = 7;
                            Thread.sleep(interval*1000);
                            self.mainControl.removeSpecificPattern(passed_segment);
                            point_from = current_segment;
                            passed_segment = new ArrayList<Object>();
                            passed_segment.add(point_from);
                            JLabel signal_of_block = self.signal_of_block.get(previous_segment);
                            self.mainControl.triggerRelativeSignal(signal_of_block);
                            checkPreviousBlockForAnotherTrain(previous_segment);
                        }
                        initial_run = false;
                    }
                } else {
                    self.mainControl.removeSpecificPattern(current_segment,previous_segment,direction);
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(Train.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }
    
    private int getTravelTime(Object from, Object to, String train_number){
        System.out.println("Get travel time from "+self.segment_type.get(from)+" "+self.number_of_segment.get(from)+" to "+self.segment_type.get(to)+" "+self.number_of_segment.get(to));
        float get_distance = self.segment_distance.get(from).get(to);
        String train_type = self.train_type.get(train_number);
        String[] fast_train = {"Argo Bromo Anggrek"};
        int speed = 75; //Kmh
        if (train_type.equals("krl") || train_type.equals("krlf") || train_type.equals("barang") || train_type.equals("barangf")){
            speed = 75;
        } else if (Arrays.asList(fast_train).contains(self.train_name.get(train_number))){
            speed = 120;
        } else {
            speed = 100;
        }
        double travel_time_math = ((get_distance/1000) / speed) * 60 * 60;
        int travel_time = (int) travel_time_math;
        System.out.println("Train speed is : "+String.valueOf(speed)+" , travel time is "+String.valueOf(travel_time)+" second");
        return travel_time;
    }
    
    private int getTravelTime(String entry_exit_name, String train_number){
        System.out.println("Get travel time from entry/exit : "+entry_exit_name);
        float get_distance = self.entry_exit_distance.get(entry_exit_name);
        String train_type = self.train_type.get(train_number);
        String[] fast_train = {"Argo Bromo Anggrek"};
        int speed = 75; //Kmh
        if (train_type.equals("krl") || train_type.equals("krlf") || train_type.equals("barang") || train_type.equals("barangf")){
            speed = 75;
        } else if (Arrays.asList(fast_train).contains(self.train_name.get(train_number))){
            speed = 120;
        } else {
            speed = 100;
        }
        double travel_time_math = ((get_distance/1000) / speed) * 60 * 60;
        int travel_time = (int) travel_time_math;
        System.out.println("Train speed is : "+String.valueOf(speed)+" , travel time is "+String.valueOf(travel_time)+" second");
        return travel_time;
    }
    
    private int getTravelTime(Object from, Object to, int speed){
        System.out.println("Get travel time from "+self.segment_type.get(from)+" "+self.number_of_segment.get(from)+" to "+self.segment_type.get(to)+" "+self.number_of_segment.get(to));
        float get_distance = self.segment_distance.get(from).get(to);
        double travel_time_math = ((get_distance/1000) / speed) * 60 * 60;
        int travel_time = (int) travel_time_math;
        System.out.println("Train speed is : "+String.valueOf(speed)+" , travel time is "+String.valueOf(travel_time)+" second");
        return travel_time;
    }
    
    private int getTravelTime(String entry_exit_name, int speed){
        System.out.println("Get travel time from entry/exit : "+entry_exit_name);
        float get_distance = self.entry_exit_distance.get(entry_exit_name);
        double travel_time_math = ((get_distance/1000) / speed) * 60 * 60;
        int travel_time = (int) travel_time_math;
        System.out.println("Train speed is : "+String.valueOf(speed)+" , travel time is "+String.valueOf(travel_time)+" second");
        return travel_time;
    }
    
    private boolean checkIfStopAtStation(Object segment,String train_number){
        boolean stopped = false;
        try{
            String train_type = self.train_type.get(train_number);
            if (self.is_adjacent_small_station.containsKey(segment) && (train_type.equals("krl") || train_type.equals("krlf"))){
                stopped = true;
                updateTrainMovement(train_number,false);
                Thread.sleep(30*1000); // Wait 30 second
            }
            String train_etd = self.train_etd.get(train_number);
            if (self.platform_number.containsKey(segment)){
                self.scoring.checkArrivalTime(train_number);
                if (train_etd.equals("Ls") != true){
                    stopped = true;
                    String log = self.textParser.trainStopDepart(train_number, segment, "stop");
                    self.information.notifyAndLog(log);
                    updateTrainMovement(train_number,false);
                    if (self.train_type.get(train_number).equals("krl") || self.train_type.get(train_number).equals("krlf")){
                        self.audio.openDoor(train_number);
                        self.announcement.welcomingPassanger();
                    }
                    Thread.sleep(10*1000);
                    self.mainControl.enableDisableDepartureButton(segment,true);
                    while (self.platform_departure_approved.get(segment) == false){
                        Thread.sleep(1*1000);
                    }
                    self.platform_departure_approved.put(segment,false);
                    self.audio.closeDoor(train_number);
                }
            }
            updateTrainMovement(train_number,true);
            int random_respond_time = self.utility.getRandomNumber(3, 10);
            Thread.sleep(random_respond_time*1000);
        }   catch (InterruptedException ex) {
            Logger.getLogger(Train.class.getName()).log(Level.SEVERE, null, ex);
        }
        return stopped;
    }
    
    private void checkIfTurnRouteCreated(Object block){
        new Thread(()->{
            if (self.is_entry_signal_block.containsKey(block) && (self.is_turn_route_created.containsKey(block) && self.is_turn_route_created.get(block))){
                try {
                    Thread.sleep(1000);
                    JLabel signal_of_block = self.signal_of_block.get(block);
                    self.mainControl.switchEntrySignal(signal_of_block,self.is_turn_route_created.get(block));
                } catch (InterruptedException ex) {
                    Logger.getLogger(Train.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }).start();
    }
    
    private void checkPreviousBlockForAnotherTrain(Object block){
        JLabel signal_of_block = self.signal_of_block.get(block);
        if (self.relative_signal.containsKey(signal_of_block)){
            JLabel relative_signal = self.relative_signal.get(signal_of_block);
            Object relative_block = self.block_of_signal.get(relative_signal);
            if (self.is_segment_reserved.get(relative_block)){
                new Thread(()->{
                    try {
                        Thread.sleep(2000);
                        self.mainControl.createRoutePattern(relative_signal);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Train.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }).start();
            }
        }
    }
    
    private void removeTrainFromActiveRunningList(String train_number){
        self.active_trains.remove(train_number);
        self.running_train.remove(train_number);
    }
    
    private boolean checkIfEntryAvailable(String train_number){
        int arrival_block = self.train_arrival_block.get(train_number);
        Object arrival_segment = self.arrival_block_segment.get(arrival_block);
        return self.is_segment_reserved.get(arrival_segment) == false;
    }
    
    private void trainExitingZone(String train_number, Object segment,Object previous_segment){
        self.exiting_list.add(train_number);
        self.train_exiting_segment.put(train_number, self.exit_block_segment.get(segment));
        if (self.segment_type.get(segment).equals("track") || self.segment_type.get(segment).equals("junction")){
            try {
                Thread.sleep(7*1000);
                self.is_segment_reserved.put(segment,false);
                self.is_train_on_segment.put(segment,false);
                self.switcher.switchTrack(segment, "black");
                removeTrainFromActiveRunningList(train_number);
                self.switcher.switchSignal(self.signal_of_block.get(previous_segment), "yellow");
                Thread.sleep(1000);
                self.mainControl.triggerRelativeSignal(self.signal_of_block.get(previous_segment));
                checkPreviousBlockForAnotherTrain(previous_segment);
                Thread.sleep(30*1000);
                ListIterator<String> exiting_trains = self.exiting_list.listIterator();
                Boolean no_adjacent_train = true;
                while (exiting_trains.hasNext()) {
                    String exiting_train = exiting_trains.next();
                    if (exiting_train != train_number && self.train_exiting_segment.get(exiting_train).equals(self.exit_block_segment.get(segment))){
                        System.out.println("get exiting train "+exiting_train);
                        no_adjacent_train = false;
                    }
                }
                if (no_adjacent_train){
                    self.switcher.switchSignal(self.signal_of_block.get(previous_segment), "green");
                    Thread.sleep(1000);
                    self.mainControl.triggerRelativeSignal(self.signal_of_block.get(previous_segment));
                    checkPreviousBlockForAnotherTrain(previous_segment);
                }
                self.exiting_list.remove(train_number);
            } catch (InterruptedException ex) {
                Logger.getLogger(Train.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private void checkNotifyAvailable(){
        if (self.block_103.getText().equals("")){
            self.headPanel.makeNotifyUnavailable("BKS");
        }
        if (self.block_202.getText().equals("") && self.block_401.getText().equals("")){
            self.headPanel.makeNotifyUnavailable("JNG");
        }
    }
}
